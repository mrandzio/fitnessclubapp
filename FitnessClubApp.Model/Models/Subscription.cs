﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClubApp.Model
{
    public class Subscription
    {
        public int SubscriptionID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int PercentPromotion { get; set; }
        public int Days { get; set; }
    }
}
