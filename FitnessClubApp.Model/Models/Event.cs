﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Model
{
    public class Event
    {
        public int EventID { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LimitOfPlaces { get; set; }

        public int RoomID { get; set; }
        public Room Room { get; set; }

        public int WorkShiftID { get; set; }
        public WorkShift WorkShift { get; set; }

        public virtual List<User> UsersOnEvent { get; set; }
    }
}
