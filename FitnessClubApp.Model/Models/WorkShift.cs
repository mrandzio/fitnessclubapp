﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClubApp.Model
{
    public class WorkShift
    {
        public int WorkShiftID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int UserID { get; set; }
        public User User { get; set; }

        public virtual List<Event> Events { get; set; }
    }
}
