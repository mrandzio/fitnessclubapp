﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FitnessClubApp.Model
{
    public class User
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Password { get; set; }
        public bool IsEmailVerified { get; set; }
        public Guid? UniqueCode { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Male { get; set; }

        public int UserRoleID { get; set; }
        public UserRole UserRole { get; set; }

        public virtual DescriptionOfUser DescriptionOfUser { get; set; }
        public virtual List<PurchasedSubscription> PurchasedSubscriptions { get; set; }
        public virtual List<WorkShift> WorkShifts { get; set; }
        public virtual List<Event> UserOnEvents { get; set; }
        public virtual List<Entry> Entries { get; set; }
    }
}
