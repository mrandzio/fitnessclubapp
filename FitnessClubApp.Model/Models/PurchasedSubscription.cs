﻿using System;

namespace FitnessClubApp.Model
{
    public class PurchasedSubscription
    {
        public int PurchasedSubscriptionID { get; set; }

        public DateTime OrderDate { get; set; }

        public string SubscriptionName { get; set; }

        public decimal Price { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string StreetAddress { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public bool Paid { get; set; }

        public bool Complete { get; set; }

        public bool Shipment { get; set; }


        public int UserID { get; set; }

        public User User { get; set; }

    }
}
