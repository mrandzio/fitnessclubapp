﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClubApp.Model
{
    public class UserRole
    {
        public int UserRoleId { get; set; }

        public string Name { get; set; }

        public string PolishName { get; set; }

        public virtual List<User> Users { get; set; }
    }
}
