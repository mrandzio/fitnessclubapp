﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClubApp.Model
{
    public class Room
    {
        public int RoomID { get; set; }

        public string Name { get; set; }

        public virtual List<Event> Events { get; set; }
    }
}
