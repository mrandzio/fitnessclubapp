﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClubApp.Model
{
    public class DescriptionOfUser
    {
        public int DescriptionOfUserID { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }

        public User User { get; set; }
    }
}
