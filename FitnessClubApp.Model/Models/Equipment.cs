﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClubApp.Model
{
    public class Equipment
    {
        public int EquipmentID { get; set; }
        public string Brand { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}
