﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using FitnessClubApp.Web.ViewModels;

namespace FitnessClubApp.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;
        private readonly IResourceService resourceService;

        public HomeController(IPurchasedSubscriptionService purchasedSubscriptionService, IResourceService resourceService)
        {
            this.purchasedSubscriptionService = purchasedSubscriptionService;
            this.resourceService = resourceService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Trainings()
        {
            var trainings = resourceService.GetTrainings().OrderBy(t => t.Name);
            return View(trainings);
        }

        public ActionResult Trainers()
        {
            var trainers = resourceService.GetDescriptionsOfUsers().OrderBy(t => $"{t.User.FirstName} {t.User.LastName}");
            return View(trainers);
        }

        public ActionResult Equipments()
        {
            var equipments = resourceService.GetEquipments().OrderBy(e => e.Name).ThenBy(e => e.Brand);
            return View(equipments);
        }

        public JsonResult GetEquipments(int? page, int? limit, string sortBy, string direction, string brand, string name)
        {
            List<DisplayEquipmentViewModel> records;
            int total;

            var equipments = resourceService.GetEquipments();
            var query = Mapper.Map<IEnumerable<Equipment>, IEnumerable<DisplayEquipmentViewModel>>(equipments);

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(q => q.Name.ToLower().Trim().Contains(name.ToLower().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(brand))
            {
                query = query.Where(q => q.Brand.ToLower().Trim().Contains(brand.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    switch (sortBy)
                    {
                        case "Brand":
                            query = query.OrderBy(q => q.Brand);
                            break;
                        case "Name":
                            query = query.OrderBy(q => q.Name);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "Brand":
                            query = query.OrderByDescending(q => q.Brand);
                            break;
                        case "Name":
                            query = query.OrderByDescending(q => q.Name);
                            break;
                    }
                }
            }
            else
            {
                query = query.OrderBy(q => q.Name);
            }

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "User")]
        public ActionResult GetSubscriptionStatus()
        {
            var activeSubscription = purchasedSubscriptionService.UserIsActiveSubscriber(User.Identity.Name);
            return PartialView("_SubscriptionStatus", activeSubscription);
        }

        [Authorize]
        public ActionResult GetNavbarItems()
        {
            var activeSubscription = purchasedSubscriptionService.UserIsActiveSubscriber(User.Identity.Name);
            return PartialView("_NavbarItems", activeSubscription);
        }
    }
}