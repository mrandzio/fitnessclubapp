﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using FitnessClubApp.Data;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Extensions.Alerts;
using FitnessClubApp.Web.ViewModels;

namespace FitnessClubApp.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IEmailService emailService;
        private readonly IUserService userService;

        public UserController(IEmailService emailService, IUserService userService)
        {
            this.emailService = emailService;
            this.userService = userService;
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(RegisterUserFormViewModel userFromViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(userFromViewModel);
            }

            #region Check if email already exists

            var userExists = userService.UserExists(userFromViewModel.Email);
            if (userExists)
            {
                ModelState.AddModelError("Email", "Istnieje już konto o podanym adresie e-mail");
                return View(userFromViewModel);
            }

            #endregion

            #region Save to database

            var userAfterMapping = Mapper.Map<RegisterUserFormViewModel, User>(userFromViewModel);

            userService.CreateUser(userAfterMapping);
            userService.SaveUser();

            #endregion

            #region Send email with verification link

            if (!userAfterMapping.UniqueCode.HasValue)
                return RedirectToAction("Index", "Home").WithWarning("Rejestracja nie przebiegła pomyślnie.", "Brak kodu aktywującego.");

            var generatedLink = GenerateLink("VerifyAccount", userAfterMapping.UniqueCode.Value);

            if (!string.IsNullOrEmpty(generatedLink))
                emailService.SendVerificationLinkEmail(userAfterMapping, generatedLink);

            #endregion

            return RedirectToAction("Index", "Home").WithSuccess("Rejestracja przebiegła pomyślnie.", $"Aktywuj konto za pomocą linku wysłanego na podany adres email ({ userAfterMapping.Email }).");
        }

        public ActionResult SendVerificationLink(string email)
        {
            #region Get user by email

            var user = userService.GetUser(email);

            if (user == null)
                return RedirectToAction("Index", "Home").WithDanger("Nie udało się wysłać linku aktywującego.",
                    $"Adres email ({email}) nie istnieje w naszej bazie.");

            #endregion

            #region Generate new unique code

            userService.GenerateNewUniqueCode(user);
            userService.SaveUser();

            #endregion

            #region Send email with verification link

            if (!user.UniqueCode.HasValue)
                return RedirectToAction("Index", "Home").WithDanger("Nie udało się wysłać linku aktywującego.",
                    "Brak kodu aktywującego.");

            var generatedLink = GenerateLink("VerifyAccount", user.UniqueCode.Value);

            if (string.IsNullOrEmpty(generatedLink))
                return RedirectToAction("Index", "Home").WithDanger("Nie udało się wysłać linku aktywującego.",
                    "Problem z wygenerowaniem linku.");

            emailService.SendVerificationLinkEmail(user, generatedLink);

            #endregion

            return RedirectToAction("Index", "Home").WithSuccess("Link aktywacyjny został ponownie wysłany.",
                $"Aktywuj konto za pomocą linku wysłanego na podany adres email ({ user.Email }).");
        }

        [NonAction]
        private string GenerateLink(string actionName, Guid activationCode)
        {
            var urlToVerifyAccount = Url.Action(actionName, new { id = activationCode });

            return Request.Url == null ? string.Empty : Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, urlToVerifyAccount);
        }

        [HttpGet]
        public ActionResult VerifyAccount(Guid id)
        {
            var user = userService.GetUser(id);
            if (user == null)
                return RedirectToAction("Index", "Home").WithDanger("Błąd!",
                    "Nie udało się aktywować konta. Prawdopodobnie błędny link aktywacyjny.");

            if (user.IsEmailVerified)
                return RedirectToAction("Index", "Home").WithInfo("Konto jest już aktywne!",
                    $"Możesz zalogować się wpisując jako login swój adres e-mail ({ user.Email }).");

            userService.ActivateAccount(user);
            userService.RemoveUniqueCode(user);
            userService.SaveUser();

            return RedirectToAction("Index", "Home").WithSuccess("Aktywowałeś konto!",
                $"Możesz teraz zalogować się wpisując jako login swój adres e-mail ({ user.Email }).");
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            if (!ModelState.IsValid)
                return View();

            #region Get user by email

            var user = userService.GetUser(email);

            if (user == null)
                return RedirectToAction("Index", "Home").WithDanger("Nie udało się wysłać linku do zmiany hasła.",
                    $"Adres email ({email}) nie istnieje w naszej bazie.");

            #endregion

            #region Generate new unique code

            userService.GenerateNewUniqueCode(user);
            userService.SaveUser();

            #endregion

            #region Send email with reset password link

            if (!user.UniqueCode.HasValue)
                return RedirectToAction("Index", "Home").WithDanger("Nie udało się wysłać linku do zmiany hasła.",
                    "Brak kodu do zmiany hasła.");

            var generatedLink = GenerateLink("ResetPassword", user.UniqueCode.Value);

            if (string.IsNullOrEmpty(generatedLink))
                return RedirectToAction("Index", "Home").WithDanger("Nie udało się wysłać linku do zmiany hasła.",
                    "Problem z wygenerowaniem linku.");

            emailService.SendResetPasswordLinkEmail(user, generatedLink);

            #endregion

            return RedirectToAction("Index", "Home").WithSuccess("Link do zmiany hasła został wysłany.",
                $"Wejdź na podaną skrzynkę pocztową ({ user.Email }) i odczytaj przesłaną wiadomość.");
        }

        [HttpGet]
        public ActionResult ResetPassword(Guid id)
        {
            var user = userService.GetUser(id);
            if (user == null)
                return RedirectToAction("Index", "Home").WithDanger("Błąd!",
                    "Nie udało się zweryfikować kodu. Prawdopodobnie błędny link.");

            var resetPasswordFormViewModel = Mapper.Map<User, ResetPasswordFormViewModel>(user);

            return View(resetPasswordFormViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordFormViewModel resetPasswordFormViewModel)
        {
            if (!ModelState.IsValid)
                return View();

            var user = userService.GetUser(resetPasswordFormViewModel.ResetPasswordCode);
            if (user == null)
                return RedirectToAction("Index", "Home").WithDanger("Błąd!",
                    "Nie udało się zweryfikować kodu. Spróbuj jeszcze raz.");

            userService.ChangePassword(user, resetPasswordFormViewModel.NewPassword);
            userService.RemoveUniqueCode(user);
            userService.SaveUser();

            return RedirectToAction("Login").WithSuccess("Udało się zmienić hasło na Twoim koncie!", "Możesz teraz zalogować się.");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginFormViewModel loginViewModel, string ReturnUrl = "")
        {
            if (!ModelState.IsValid)
                return View();

            var user = userService.GetUser(loginViewModel.Email);

            if (user == null || !Crypto.VerifyHashedPassword(user.Password, loginViewModel.Password))
                return View().WithDanger("Logowanie zakończyło się niepowodzeniem.", "Login (e-mail) lub hasło są nieprawidłowe.");

            if (!user.IsEmailVerified)
                return View().WithWarning("Logowanie zakończyło się niepowodzeniem.", 
                    $"Konto nie jest jeszcze aktywne. Sprawdź skrzynkę e-mail (w tym spam). Ewentualnie <a href=\'{ Url.Action("SendVerificationLink", new { email = user.Email }) }\'>wyślij ponownie link aktywacyjny</a>.");

            var timeout = user.UserRole.Name != "User" || loginViewModel.RememberMe ? 525600 : 20; // 525600 min = 1 year

            if (user.UserRole == null)
                return View().WithDanger("Logowanie zakończyło się niepowodzeniem.", "Użytkownik nie ma przypisanych uprawnień.");

            var ticket = new FormsAuthenticationTicket(
                1,
                user.Email, //user id
                DateTime.Now,
                DateTime.Now.AddMinutes(timeout), // expiry
                user.UserRole.Name != "User" || loginViewModel.RememberMe, //do not remember
                user.UserRole.Name,
                FormsAuthentication.FormsCookiePath);

            var encrypted = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted)
            {
                Expires = DateTime.Now.AddMinutes(timeout),
                HttpOnly = true,
            };
            Response.Cookies.Add(cookie);

            return Url.IsLocalUrl(ReturnUrl)
                ? (ActionResult) Redirect(FormsAuthentication.GetRedirectUrl(user.Email, user.UserRole.Name != "User" || loginViewModel.RememberMe))
                : RedirectToAction("Index", "Home");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }

        [Authorize]
        [HttpGet]
        public new ActionResult Profile()
        {
            var user = userService.GetUser(User.Identity.Name);
            return View(user);
        }

        [Authorize]
        [HttpGet]
        public ActionResult ChangePassword()
        {
            var user = userService.GetUser(User.Identity.Name);

            var changePasswordFormViewModel = new ChangePasswordFormViewModel { UserID = user.UserID };

            return View(changePasswordFormViewModel);
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordFormViewModel changePasswordFormViewModel)
        {
            if (!ModelState.IsValid)
                return View(changePasswordFormViewModel);

            var user = userService.GetUser(User.Identity.Name);

            if (changePasswordFormViewModel.UserID != user.UserID)
                return RedirectToAction("Profile")
                    .WithDanger("Problemy z uwierzytelnieniem!", "Spróbuj zalogować się jeszcze raz.");

            if (!Crypto.VerifyHashedPassword(user.Password, changePasswordFormViewModel.CurrentPassword))
            {
                ModelState.AddModelError("CurrentPassword", "Podaj prawidłowe obecne hasło");
                return View(changePasswordFormViewModel);
            }

            userService.ChangePassword(user, changePasswordFormViewModel.NewPassword);
            userService.SaveUser();

            return RedirectToAction("Profile").WithSuccess("Udało się zmienić hasło na Twoim koncie!", "Przy ponownym logowaniu podaj nowe hasło.");
        }
    }
}