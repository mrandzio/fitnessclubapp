﻿using System.Web.Mvc;

namespace FitnessClubApp.Web.Areas.Membership
{
    public class MembershipAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Membership";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Membership_default",
                "Membership/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}