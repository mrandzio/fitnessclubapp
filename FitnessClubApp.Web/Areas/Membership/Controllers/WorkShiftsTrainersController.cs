﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using FitnessClubApp.Web.Extensions.Alerts;
using WebGrease.Css.Extensions;

namespace FitnessClubApp.Web.Areas.Membership.Controllers
{
    [Authorize(Roles = "User")]
    public class WorkShiftsTrainersController : Controller
    {
        private readonly IWorkScheduleService workScheduleService;
        private readonly IUserService userService;
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;

        public WorkShiftsTrainersController(IWorkScheduleService workScheduleService, IUserService userService, IPurchasedSubscriptionService purchasedSubscriptionService)
        {
            this.workScheduleService = workScheduleService;
            this.userService = userService;
            this.purchasedSubscriptionService = purchasedSubscriptionService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetWorkShiftsTrainers(DateTime start, DateTime end)
        {
            var workShifts = workScheduleService.GetWorkShifts(start, end).Where(ws => ws.User.UserRole.Name == "Trainer");
            var eventsInSchedule = Mapper.Map<IEnumerable<WorkShift>, IEnumerable<DisplayWorkShiftsTrainersScheduleEventViewModel>>(workShifts);

            return Json(eventsInSchedule, JsonRequestBehavior.AllowGet);
        }
    }
}