﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using FitnessClubApp.Web.Extensions.Alerts;
using Microsoft.Ajax.Utilities;

namespace FitnessClubApp.Web.Areas.Membership.Controllers
{
    [Authorize(Roles = "User")]
    public class SeatReservationController : Controller
    {
        private readonly IResourceService resourceService;
        private readonly IWorkScheduleService workScheduleService;
        private readonly IUserService userService;
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;

        public SeatReservationController(IResourceService resourceService, IWorkScheduleService workScheduleService, 
            IUserService userService, IPurchasedSubscriptionService purchasedSubscriptionService)
        {
            this.resourceService = resourceService;
            this.workScheduleService = workScheduleService;
            this.userService = userService;
            this.purchasedSubscriptionService = purchasedSubscriptionService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetSeatReservationEvents(DateTime start, DateTime end)
        {
            var user = userService.GetUser(User.Identity.Name);
            var events = workScheduleService.GetEvents(start, end);
            var eventsInSchedule = Mapper.Map<IEnumerable<Event>, IEnumerable<DisplaySeatReservationScheduleEventViewModel>>(events);

            events.ForEach(e =>
            {
                if (e.UsersOnEvent.Any(u => u.UserID == user.UserID))
                {
                    eventsInSchedule.First(eis => eis.id == e.EventID).color = "rgb(58, 135, 173)";
                }
            });

            return Json(eventsInSchedule, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowSavingOptions(int eventID)
        {
            var @event = workScheduleService.GetEvent(eventID);
            if (@event == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (@event.StartDate.AddHours(-1) < DateTime.Now)
                return PartialView("_ErrorSigningUpForEvent", @event);

            var user = userService.GetUser(User.Identity.Name);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var registeredUser = @event.UsersOnEvent.Any(u => u.UserID == user.UserID);
            if (registeredUser)
                return PartialView("_RemoveUserFromEvent", @event);

            var userInActiveSubscription = purchasedSubscriptionService.UserInActiveSubscription(user.Email, @event.StartDate);
            if (!userInActiveSubscription)
                return PartialView("_ErrorActiveSubscriber", @event);

            if (@event.UsersOnEvent.Count >= @event.LimitOfPlaces)
                return PartialView("_ErrorLimitOfPlaces", @event);

            return PartialView("_AddUserToEvent", @event);
        }

        public ActionResult AddUserToEvent(int eventID)
        {
            var @event = workScheduleService.GetEvent(eventID);
            if (@event == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (@event.StartDate.AddHours(-1) < DateTime.Now)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = userService.GetUser(User.Identity.Name);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            @event.UsersOnEvent.Add(user);
            workScheduleService.SaveEvent();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult RemoveUserFromEvent(int eventID)
        {
            var @event = workScheduleService.GetEvent(eventID);
            if (@event == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (@event.StartDate.AddHours(-1) < DateTime.Now)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = userService.GetUser(User.Identity.Name);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            @event.UsersOnEvent.Remove(user);
            workScheduleService.SaveEvent();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}