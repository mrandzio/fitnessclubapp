﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using FitnessClubApp.Web.Extensions.Alerts;

namespace FitnessClubApp.Web.Areas.Membership.Controllers
{
    [Authorize(Roles = "User")]
    public class PurchasedSubscriptionController : Controller
    {
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;
        private readonly IUserService userService;

        public PurchasedSubscriptionController(IPurchasedSubscriptionService purchasedSubscriptionService, IUserService userService)
        {
            this.purchasedSubscriptionService = purchasedSubscriptionService;
            this.userService = userService;
        }

        public ActionResult History()
        {
            var activePurchasedSubscription = purchasedSubscriptionService.GetActiveUserSubscription(User.Identity.Name);

            return View(activePurchasedSubscription);
        }

        public JsonResult GetTransactionHistory(int? page, int? limit)
        {
            List<DisplayTransactionViewModel> records;
            int total;

            var user = userService.GetUser(User.Identity.Name);

            var purchasedSubscriptions = purchasedSubscriptionService.GetUserPurchasedSubscriptions(user);

            var query = Mapper.Map<IEnumerable<PurchasedSubscription>, IEnumerable<DisplayTransactionViewModel>>(purchasedSubscriptions);

            query = query.OrderByDescending(q => q.OrderDate);

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
    }
}