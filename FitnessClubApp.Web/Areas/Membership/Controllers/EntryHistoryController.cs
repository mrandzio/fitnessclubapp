﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Membership.ViewModels;
using FitnessClubApp.Web.Extensions.Alerts;

namespace FitnessClubApp.Web.Areas.Membership.Controllers
{
    [Authorize(Roles = "User")]
    public class EntryHistoryController : Controller
    {
        private readonly IEntryRegisterService entryRegisterService;
        private readonly IUserService userService;
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;

        public EntryHistoryController(IEntryRegisterService entryRegisterService, IUserService userService, IPurchasedSubscriptionService purchasedSubscriptionService)
        {
            this.entryRegisterService = entryRegisterService;
            this.userService = userService;
            this.purchasedSubscriptionService = purchasedSubscriptionService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEntryHistory(int? page, int? limit)
        {
            var currentDate = DateTime.Now.Date.AddDays(-30);
            var user = userService.GetUser(User.Identity.Name);    

            List<DisplayEntryViewModel> records;
            int total;

            var entries = entryRegisterService.GetUserEntries(user.UserID, currentDate);

            var query = Mapper.Map<IEnumerable<Entry>, IEnumerable<DisplayEntryViewModel>>(entries);

            query = query.OrderByDescending(q => q.StartDate);

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
    }
}