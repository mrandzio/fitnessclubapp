﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Membership.ViewModels;
using FitnessClubApp.Web.Extensions.Alerts;
using Microsoft.AspNet.Identity;

namespace FitnessClubApp.Web.Areas.Membership.Controllers
{
    [Authorize(Roles = "User")]
    public class StoreController : Controller
    {
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;
        private readonly ISubscriptionService subscriptionService;
        private readonly IUserService userService;
        private readonly IEmailService emailService;

        public StoreController(ISubscriptionService subscriptionService, IUserService userService, IPurchasedSubscriptionService purchasedSubscriptionService, IEmailService emailService)
        {
            this.purchasedSubscriptionService = purchasedSubscriptionService;
            this.subscriptionService = subscriptionService;
            this.userService = userService;
            this.emailService = emailService;
        } 

        [AllowAnonymous]
        public ActionResult Choose()
        {
            var subscriptions = subscriptionService.GetSortedSubscriptions();
            return View(subscriptions);
        }
        
        [HttpGet]
        public ActionResult Checkout(int subscriptionID)
        {
            var user = userService.GetUser(User.Identity.GetUserName());
            if (user == null)
                return RedirectToAction("Choose")
                    .WithDanger("Nie możesz przejść do kasy.", "Musisz być zalogowanym do konta.");

            var subscription = subscriptionService.GetSubscription(subscriptionID);
            if (subscription == null)
                return RedirectToAction("Choose")
                    .WithDanger("Nie możesz przejść do kasy.", "Wybrałeś nieistniejącą subskrypcję.");

            var subscriptionOrderFormViewModel = new SubscriptionOrderFormViewModel
            {
                User = user,
                UserID = user.UserID,
                Subscription = subscription,
                SubscriptionID = subscription.SubscriptionID
            };

            var lastPurchasedSubscription = purchasedSubscriptionService.GetLastUserPurchasedSubscription(user.UserID);
            if (lastPurchasedSubscription != null)
            {
                subscriptionOrderFormViewModel = Mapper.Map(lastPurchasedSubscription, subscriptionOrderFormViewModel);
            }

            return View(subscriptionOrderFormViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Checkout(SubscriptionOrderFormViewModel subscriptionOrderFormView)
        {
            var user = userService.GetUser(User.Identity.GetUserName());
            if (user == null)
                return RedirectToAction("Choose")
                    .WithDanger("Nie możesz przejść do podsumowania.", "Musisz być zalogowanym do konta.");

            var subscription = subscriptionService.GetSubscription(subscriptionOrderFormView.SubscriptionID);
            if (subscription == null)
                return RedirectToAction("Choose")
                    .WithDanger("Nie możesz przejść do podsumowania.", "Wybrałeś nieistniejącą subskrypcję.");

            if (user.UserID != subscriptionOrderFormView.UserID)
                return RedirectToAction("Choose")
                    .WithDanger("Nie możesz przejść do podsumowania.", "Chcesz przypisać subskrypcję do użytkownika, na którym nie jesteś obecnie zalogowanym.");

            subscriptionOrderFormView.Subscription = subscription;
            subscriptionOrderFormView.User = user;

            if (!ModelState.IsValid)
                return View(subscriptionOrderFormView);

            var purchasedSubscriptionExists =
                purchasedSubscriptionService.PurchasedSubscriptionExists(user.UserID,
                    subscriptionOrderFormView.StartDate, subscription.Days);
            if (purchasedSubscriptionExists)
            {
                ModelState.AddModelError("StartDate", "Kupiłeś już subskrypcję w wybranych datach");
                return View(subscriptionOrderFormView);
            }

            Session["subscriptionOrdered"] = subscriptionOrderFormView;

            return RedirectToAction("Summary");
        }

        [HttpGet]
        public ActionResult Summary()
        {
            var subscriptionOrdered = Session["subscriptionOrdered"];

            if (subscriptionOrdered == null || subscriptionOrdered.GetType() != typeof(SubscriptionOrderFormViewModel))
                return RedirectToAction("Choose")
                    .WithDanger("Nie możesz przejść do podsumowania.", "Nie złożyłeś dyspozycji zakupu subskrypcji."); 

            return View(subscriptionOrdered);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Confirm()
        {
            var subscriptionOrderedViewModel = Session["subscriptionOrdered"];

            if (subscriptionOrderedViewModel == null || subscriptionOrderedViewModel.GetType() != typeof(SubscriptionOrderFormViewModel))
                return RedirectToAction("Choose")
                    .WithDanger("Nie możesz przejść do podsumowania.", "Nie złożyłeś dyspozycji zakupu subskrypcji.");

            var purchasedSubscription =
                Mapper.Map<SubscriptionOrderFormViewModel, PurchasedSubscription>(subscriptionOrderedViewModel as SubscriptionOrderFormViewModel);

            purchasedSubscription.User = null;
            purchasedSubscriptionService.CreatePurchasedSubscription(purchasedSubscription);
            purchasedSubscriptionService.SavePurchasedSubscription();

            var user = (subscriptionOrderedViewModel as SubscriptionOrderFormViewModel)?.User;

            Session["subscriptionOrdered"] = null;

            emailService.SendConfirmationPurchaseSubscription(user, purchasedSubscription);

            return RedirectToAction("Confirmation", new { purchasedSubscriptionID = purchasedSubscription.PurchasedSubscriptionID });
        }

        [HttpGet]
        public ActionResult Confirmation(int purchasedSubscriptionID)
        {
            var purchasedSubscription = purchasedSubscriptionService.GetPurchasedSubscription(purchasedSubscriptionID);

            if (purchasedSubscription == null)
                return RedirectToAction("Index", "Home").WithDanger("Potwierdzenie nie zostanie wygenerowane.", "Zły parametr do utworzenia potwierdzenia.");

            var user = userService.GetUser(purchasedSubscription.UserID);

            if (user.Email != User.Identity.GetUserName())
                return RedirectToAction("Index", "Home").WithDanger("Potwierdzenie nie zostanie wygenerowane.", "Zalogowany użytkownik nie ma takich uprawnień.");

            return View(purchasedSubscription);
        }
    }
}