﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Model;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Membership.ViewModels
{
    public class SubscriptionOrderFormViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę rozpoczęcia")]
        [SubscriptionStartDateValidation(MaximumLateness = 30)]
        public string StartDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj adres")]
        public string StreetAddress { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj imię")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj nazwisko")]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj kod pocztowy")]
        [RegularExpression(@"^\d{2}[-]\d{3}$", ErrorMessage = "Podaj prawidłowy kod pocztowy (XX-XXX)")]
        public string ZipCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj miejscowość")]
        public string City { get; set; }

        public bool Shipment { get; set; }

        [CheckboxIsChecked(ErrorMessage = "Musisz zaakceptować regulaminy")]
        public bool IsAcceptedRules { get; set; }

        [Required]
        public int UserID { get; set; }

        public User User { get; set; }

        [Required]
        public int SubscriptionID { get; set; }

        public Subscription Subscription { get; set; }

    }
}