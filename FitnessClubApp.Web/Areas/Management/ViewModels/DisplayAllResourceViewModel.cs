﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayAllResourceViewModel
    {
        public string id { get; set; }

        public string title { get; set; }

        public string role { get; set; }

        public IEnumerable<Room> children { get; set; }

        public class Room
        {
            public string id { get; set; }

            public string title { get; set; }
        }
    }
}