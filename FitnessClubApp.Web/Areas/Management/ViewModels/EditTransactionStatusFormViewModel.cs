﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class EditTransactionStatusFormViewModel
    {
        public int PurchasedSubscriptionID { get; set; }

        public bool Paid { get; set; }

        public bool Complete { get; set; }
    }
}