﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayUnfinishedEntryViewModel
    {
        public int EntryID { get; set; }
        public int UserID { get; set; }
        public DateTime StartDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}