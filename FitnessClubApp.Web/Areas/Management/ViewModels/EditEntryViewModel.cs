﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class EditEntryViewModel
    {
        public int EntryID { get; set; }

        [DateTimeValidation(ErrorMessage = "Podaj prawidłową datę wejścia")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę wejścia")]
        public string StartDate { get; set; }

        [OptionalDateTimeValidation(ErrorMessage = "Podaj prawidłową datę wyjścia")]
        public string EndDate { get; set; }
    }
}