﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class RoomFormViewModel
    {
        public int RoomID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj nazwę")]
        public string Name { get; set; }
    }
}