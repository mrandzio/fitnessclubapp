﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class MonthlyAmountOfSalesFormViewModel
    {
        [MonthValidation(ErrorMessage = "Podaj prawidłowy miesiąc początkowy")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj miesiąc początkowy")]
        public string StartDate { get; set; }

        [MonthValidation(ErrorMessage = "Podaj prawidłowy miesiąc końcowy")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj miesiąc końcowy")]
        public string EndDate { get; set; }

        public string TypeOfGraph { get; set; }

        public IEnumerable<SelectListItem> GraphTypes { get; set; }
    }
}