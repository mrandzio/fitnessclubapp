﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FitnessClubApp.Model;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayTransactionViewModel
    {
        public int PurchasedSubscriptionID { get; set; }

        public DateTime OrderDate { get; set; }

        public string SubscriptionName { get; set; }

        public string Price { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PurcharserFirstName { get; set; }

        public string PurcharserLastName { get; set; }

        public string PurcharserPersonalData { get; set; }

        public string StreetAddress { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public bool Paid { get; set; }

        public bool Complete { get; set; }

        public bool Shipment { get; set; }

        public string SubscriberFirstName { get; set; }

        public string SubscriberLastName { get; set; }

        public string SubscriberPersonalData { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}