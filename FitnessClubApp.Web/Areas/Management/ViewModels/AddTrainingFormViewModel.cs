﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class AddTrainingFormViewModel
    {
        public int TrainingID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj nazwę")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj opis")]
        public string Description { get; set; }

        [ValidateFile(ErrorMessage = "Dołącz obraz w formacie JPEG lub PNG i rozmiarze poniżej 1MB")]
        public HttpPostedFileBase Image { get; set; }
    }
}