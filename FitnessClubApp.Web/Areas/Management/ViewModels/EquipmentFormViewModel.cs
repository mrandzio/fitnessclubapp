﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FitnessClubApp.Model;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class EquipmentFormViewModel
    {
        public int EquipmentID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj markę")]
        public string Brand { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj nazwę")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj liczbę sztuk")]
        [Range(1, 100, ErrorMessage = "Podaj prawidłową liczbę sztuk")]
        public int Amount { get; set; }
    }
}