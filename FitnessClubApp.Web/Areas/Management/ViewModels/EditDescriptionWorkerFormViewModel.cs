﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class EditDescriptionWorkerFormViewModel
    {
        public int DescriptionOfUserID { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj opis")]
        public string Description { get; set; }
    }
}