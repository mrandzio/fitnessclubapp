﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DailyAmountOfSalesFormViewModel
    {
        [DateValidation(ErrorMessage = "Podaj prawidłową datę początkową")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę początkową")]
        public string StartDate { get; set; }

        [DateValidation(ErrorMessage = "Podaj prawidłową datę końcową")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę końcową")]
        public string EndDate { get; set; }

        public string TypeOfGraph { get; set; }

        public IEnumerable<SelectListItem> GraphTypes { get; set; }
    }
}