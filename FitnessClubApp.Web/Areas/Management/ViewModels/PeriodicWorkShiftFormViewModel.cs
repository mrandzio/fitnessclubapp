﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Model;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class PeriodicWorkShiftFormViewModel
    {
        [DateValidation(ErrorMessage = "Podaj prawidłową datę rozpoczęcia cyklu")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę rozpoczęcia cyklu")]
        public string StartDate { get; set; }

        [DateValidation(ErrorMessage = "Podaj prawidłową datę zakończenia cyklu")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę zakończenia cyklu")]
        public string EndDate { get; set; }

        [TimeValidation(ErrorMessage = "Podaj prawidłową godzinę rozpoczęcia dyżuru")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj godzinę rozpoczęcia dyżuru")]
        public string StartTime { get; set; }

        [TimeValidation(ErrorMessage = "Podaj prawidłową godzinę zakończenia dyżuru")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj godzinę zakończenia dyżuru")]
        public string EndTime { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Wybierz pracownika")]
        public int WorkerID { get; set; }

        public IEnumerable<SelectListItem> Workers { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj dzień tygodnia dyżuru")]
        public int DayOfWeek { get; set; }

        public IEnumerable<SelectListItem> DaysOfWeek { get; set; }
    }
}