﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class EditUserRoleFormViewModel
    {
        public int UserID { get; set; }

        public int UserRoleID { get; set; }
    }
}