﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class SubscriptionFormViewModel
    {
        public int SubscriptionID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj nazwę")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Podaj cenę")]
        [Range(0.0, double.MaxValue, ErrorMessage = "Podaj prawidłową cenę")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Podaj procent promocji")]
        [Range(0, 100, ErrorMessage = "Podaj prawidłowy procent promocji")]
        public int PercentPromotion { get; set; }

        [Required(ErrorMessage = "Podaj liczbę dni subskrypcji")]
        [Range(1, int.MaxValue, ErrorMessage = "Podaj prawidłową liczbę dni subskrypcji")]
        public int Days { get; set; }
    }
}