﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayPieGraphViewModel
    {
        public IEnumerable<string> labels { get; set; }
        public List<GraphData> datasets { get; set; } = new List<GraphData>();

        public class GraphData
        {
            public IEnumerable<int> data { get; set; }
            public IEnumerable<string> backgroundColor { get; set; }
        }
    }
}