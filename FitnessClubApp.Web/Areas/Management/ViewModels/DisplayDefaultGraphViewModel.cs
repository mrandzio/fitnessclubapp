﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayDefaultGraphViewModel
    {
        public string type { get; set; }
        public GraphContent data { get; set; }
        public class GraphContent
        {
            public IEnumerable<string> labels { get; set; }
            public List<GraphData> datasets { get; set; } = new List<GraphData>();

            public class GraphData
            {
                public string label { get; set; }
                public IEnumerable<int> data { get; set; }
                public string backgroundColor { get; set; }
                public string borderColor { get; set; }
                public int borderWidth { get; set; }
                public bool fill { get; set; }
            }
        }
    }
}