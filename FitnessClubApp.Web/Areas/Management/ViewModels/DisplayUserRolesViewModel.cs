﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayUserRolesViewModel
    {
        public int UserRoleID { get; set; }

        public string Name { get; set; }
    }
}