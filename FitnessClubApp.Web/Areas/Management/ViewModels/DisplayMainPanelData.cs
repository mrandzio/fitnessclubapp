﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayMainPanelData
    {
        public int DailyRegisteredAccounts { get; set; }
        public int MonthlyRegisteredAccounts { get; set; }
        public int AnnualRegisteredAccounts { get; set; }
        public int AllRegisteredAccounts { get; set; }
        public int DailyRegisteredEntries { get; set; }
        public int MonthlyRegisteredEntries { get; set; }
        public int AnnualRegisteredEntries { get; set; }
        public int AllRegisteredEntries { get; set; }
        public int DailyTransactions { get; set; }
        public int MonthlyTransactions { get; set; }
        public int AnnualTransactions { get; set; }
        public int AllTransactions { get; set; }
        public int DailyUsersInEvents { get; set; }
        public int MonthlyUsersInEvents { get; set; }
        public int AnnualUsersInEvents { get; set; }
        public int AllUsersInEvents { get; set; }
        public int AmountOfReceptionists { get; set; }
        public int AmountOfTrainers { get; set; }
        public int AmountOfManagers { get; set; }
        public int AmountOfEquipments { get; set; }
        public int AmountOfRooms { get; set; }
        public int AmountOfTrainings { get; set; }
        public int AmountOfSubscriptions { get; set; }
    }
}