﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Model;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisposableEventFormViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Wybierz nazwę zajęć")]
        public string Name { get; set; }

        [DateTimeValidation(ErrorMessage = "Podaj prawidłową datę rozpoczęcia zajęć")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę zakończenia zajęć")]
        public string StartDate { get; set; }

        [DateTimeValidation(ErrorMessage = "Podaj prawidłową datę zakończenia zajęć")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę zakończenia zajęć")]
        public string EndDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj liczbę miejsc")]
        [Range(1, 100, ErrorMessage = "Podaj prawidłową liczbę miejsc")]
        public int LimitOfPlaces { get; set; } = 10;

        [Required(AllowEmptyStrings = false, ErrorMessage = "Wybierz salę")]
        public int RoomID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Wybierz pracownika")]
        public int WorkerID { get; set; }

        public IEnumerable<SelectListItem> Workers { get; set; }
        public IEnumerable<SelectListItem> Rooms { get; set; }
        public IEnumerable<SelectListItem> Trainings { get; set; }
    }
}