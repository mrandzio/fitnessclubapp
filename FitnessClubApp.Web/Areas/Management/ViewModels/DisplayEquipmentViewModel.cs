﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FitnessClubApp.Model;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayEquipmentViewModel
    {
        public int EquipmentID { get; set; }

        public string Brand { get; set; }

        public string Name { get; set; }

        public int Amount { get; set; }
    }
}