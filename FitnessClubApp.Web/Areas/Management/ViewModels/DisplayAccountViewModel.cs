﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FitnessClubApp.Model;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplayAccountViewModel
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool Male { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserRoleID { get; set; }
        public string UserRoleName { get; set; }
    }
}