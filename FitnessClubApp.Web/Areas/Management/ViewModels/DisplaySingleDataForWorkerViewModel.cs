﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class DisplaySingleDataForWorkerViewModel
    {
        public int id { get; set; }

        public string description { get; set; }

        public DateTime start { get; set; }

        public DateTime end { get; set; }

        public string title { get; set; }
    }
}