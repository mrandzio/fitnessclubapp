﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Model;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.Areas.Management.ViewModels
{
    public class EditWorkShiftFormViewModel
    {
        public int WorkShiftID { get; set; }

        [DateTimeValidation(ErrorMessage = "Podaj prawidłową datę rozpoczęcia dyżuru")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę rozpoczęcia dyżuru")]
        public string StartDate { get; set; }

        [DateTimeValidation(ErrorMessage = "Podaj prawidłową datę zakończenia dyżuru")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę zakończenia dyżuru")]
        public string EndDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Wybierz pracownika")]
        public int WorkerID { get; set; }

        public IEnumerable<SelectListItem> Workers { get; set; }
    }
}