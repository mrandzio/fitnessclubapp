﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class SubscriptionController : Controller
    {
        private readonly ISubscriptionService subscriptionService;

        public SubscriptionController(ISubscriptionService subscriptionService)
        {
            this.subscriptionService = subscriptionService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllSubscriptions()
        {
            var subscriptions = subscriptionService.GetSortedSubscriptions();
            return PartialView("_AllSubscriptions", subscriptions);
        }

        [HttpGet]
        public ActionResult AddSubscription()
        {
            return PartialView("_SubscriptionForm");
        }

        [HttpGet]
        public ActionResult EditSubscription(int subscriptionID)
        {
            var subscription = subscriptionService.GetSubscription(subscriptionID);

            if (subscription == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var subscriptionViewModel = Mapper.Map<Subscription, SubscriptionFormViewModel>(subscription);

            return PartialView("_SubscriptionForm", subscriptionViewModel);
        }

        [HttpGet]
        public ActionResult RemoveSubscription(int subscriptionID)
        {
            var subscription = subscriptionService.GetSubscription(subscriptionID);

            if (subscription == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("_ConfirmationRemoveSubscription", subscription);
        }

        [HttpPost]
        public ActionResult AddSubscription(SubscriptionFormViewModel subscriptionFormViewModel)
        {
            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var subscriptionAfterMapping = Mapper.Map<SubscriptionFormViewModel, Subscription>(subscriptionFormViewModel);

            subscriptionService.CreateSubscription(subscriptionAfterMapping);
            subscriptionService.SaveSubscription();

            return GetAllSubscriptions();
        }

        [HttpPost]
        public ActionResult EditSubscription(SubscriptionFormViewModel subscriptionFormViewModel)
        {
            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var subscriptionAfterMapping = Mapper.Map<SubscriptionFormViewModel, Subscription>(subscriptionFormViewModel);

            subscriptionService.EditSubscription(subscriptionAfterMapping);
            subscriptionService.SaveSubscription();

            return GetAllSubscriptions();
        }

        [HttpPost]
        public ActionResult ConfirmationRemoveSubscription(int subscriptionID)
        {
            var subscription = subscriptionService.GetSubscription(subscriptionID);

            if (subscription == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            subscriptionService.RemoveSubscription(subscription);
            subscriptionService.SaveSubscription();

            return GetAllSubscriptions();
        }

    }
}