﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Trainer,Receptionist,Manager,Admin")]
    public class TransactionController : Controller
    {
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;

        public TransactionController(IPurchasedSubscriptionService purchasedSubscriptionService)
        {
            this.purchasedSubscriptionService = purchasedSubscriptionService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetTransactions(int? page, int? limit, string sortBy, string direction, string purcharser, string ID, bool? paid, bool? complete)
        {
            List<DisplayTransactionViewModel> records;
            int total;

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptionsWithUser();
            var query = Mapper.Map<IEnumerable<PurchasedSubscription>, IEnumerable<DisplayTransactionViewModel>>(purchasedSubscriptions);

            if (!string.IsNullOrEmpty(ID))
            {
                List<DisplayTransactionViewModel> query2 = query.ToList();
                int integerID;
                if (int.TryParse(ID, out integerID))
                    query2 = query2.Where(q => q.PurchasedSubscriptionID == integerID).ToList();

                if (query2.Any())
                {
                    total = query2.Count;
                    return Json(new { records = query2, total }, JsonRequestBehavior.AllowGet);
                }                   
            }          

            if (paid.HasValue)
            {
                query = query.Where(q => q.Paid == paid.Value);
            }

            if (complete.HasValue)
            {
                query = query.Where(q => q.Complete == complete.Value);
            }

            if (!string.IsNullOrWhiteSpace(purcharser))
            {
                query = query.Where(q => $"{q.PurcharserFirstName} {q.PurcharserLastName}".ToLower().Trim().Contains(purcharser.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    switch (sortBy)
                    {
                        case "PurchasedSubscriptionID":
                            query = query.OrderBy(q => q.PurchasedSubscriptionID);
                            break;
                        case "OrderDate":
                            query = query.OrderBy(q => q.OrderDate);
                            break;
                        case "PurcharserPersonalData":
                            query = query.OrderBy(q => q.PurcharserPersonalData);
                            break;
                        case "SubscriptionName":
                            query = query.OrderBy(q => q.SubscriptionName);
                            break;
                        case "Price":
                            query = query.OrderBy(q => q.Price);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "PurchasedSubscriptionID":
                            query = query.OrderByDescending(q => q.PurchasedSubscriptionID);
                            break;
                        case "OrderDate":
                            query = query.OrderByDescending(q => q.OrderDate);
                            break;
                        case "PurcharserPersonalData":
                            query = query.OrderByDescending(q => q.PurcharserPersonalData);
                            break;
                        case "SubscriptionName":
                            query = query.OrderByDescending(q => q.SubscriptionName);
                            break;
                        case "Price":
                            query = query.OrderByDescending(q => q.Price);
                            break;
                    }
                }
            }
            else
            {
                query = query.OrderByDescending(q => q.OrderDate);
            }

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditTransactionStatus(EditTransactionStatusFormViewModel transactionStatusViewModel)
        {
            var purchasedSubscription = purchasedSubscriptionService.GetPurchasedSubscription(transactionStatusViewModel.PurchasedSubscriptionID);

            purchasedSubscription = Mapper.Map(transactionStatusViewModel, purchasedSubscription);

            purchasedSubscriptionService.EditPurchasedSubscription(purchasedSubscription);
            purchasedSubscriptionService.SavePurchasedSubscription();

            return Json(new { result = true });
        }

        [HttpPost]
        public ActionResult RemoveTransaction(int transactionID)
        {
            var transaction = purchasedSubscriptionService.GetPurchasedSubscription(transactionID);
            if (transaction == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            purchasedSubscriptionService.RemovePurchasedSubscription(transaction);
            purchasedSubscriptionService.SavePurchasedSubscription();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}