﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class RegisteredAccountsStatisticsController : Controller
    {
        private readonly IStatisticService statisticService;

        public RegisteredAccountsStatisticsController(IStatisticService statisticService)
        {
            this.statisticService = statisticService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult DefaultRegisteredAccounts()
        {
            DateTime startDate = DateTime.Now.Date.AddDays(-14);
            DateTime endDate = DateTime.Now.Date;

            var dailyAmountOfSales = statisticService.GetDailyRegisteredAccounts(startDate, endDate);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = "bar",
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
            {
                label = "Liczba zarejestrowanych kont",
                data = dailyAmountOfSales.Data,
                backgroundColor = "#6495ED",
                borderColor = "#6495ED",
                borderWidth = 1,
                fill = false
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DailyRegisteredAccountsForm()
        {
            var dailyAmountOfSalesViewModel = new DailyAmountOfSalesFormViewModel
            {
                StartDate = DateTime.Now.Date.AddDays(-14).ToString("dd.MM.yyyy"),
                EndDate = DateTime.Now.Date.ToString("dd.MM.yyyy"),
                TypeOfGraph = "bar",
                GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Słupkowy", Value = "bar" },
                    new SelectListItem { Text = "Liniowy", Value = "line" }
                }
            };

            return PartialView("_DailyRegisteredAccountsForm", dailyAmountOfSalesViewModel);
        }

        [HttpPost]
        public ActionResult RenderRegisteredAccounts(DailyAmountOfSalesFormViewModel dailyAmountOfSalesViewModel)
        {
            if (!ModelState.IsValid)
            {
                dailyAmountOfSalesViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_DailyRegisteredAccountsForm", dailyAmountOfSalesViewModel);
            }

            DateTime startDateTime = dailyAmountOfSalesViewModel.StartDate.AsDateTime();
            DateTime endDateTime = dailyAmountOfSalesViewModel.EndDate.AsDateTime();

            if (startDateTime > endDateTime)
            {
                ModelState.AddModelError("StartDate", "Data początkowa nie może być późniejsza od daty końcowej");
                ModelState.AddModelError("EndDate", "Data końcowa nie może być wcześniejsza od daty początkowej");

                dailyAmountOfSalesViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_DailyRegisteredAccountsForm", dailyAmountOfSalesViewModel);
            }

            if (endDateTime.Subtract(startDateTime).Days > 14)
            {
                ModelState.AddModelError("StartDate", "Możesz wygenerować maksymalnie 14 dniowy raport");
                ModelState.AddModelError("EndDate", "Możesz wygenerować maksymalnie 14 dniowy raport");

                dailyAmountOfSalesViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_DailyRegisteredAccountsForm", dailyAmountOfSalesViewModel);
            }

            var dailyAmountOfSales = statisticService.GetDailyRegisteredAccounts(startDateTime, endDateTime);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = dailyAmountOfSalesViewModel.TypeOfGraph,
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
            {
                label = "Liczba zarejestrowanych kont",
                data = dailyAmountOfSales.Data,
                backgroundColor = "#6495ED",
                borderColor = "#6495ED",
                borderWidth = 1,
                fill = false
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MonthlyRegisteredAccountsForm()
        {
            var monthlyAmountOfSalesFormViewModel = new MonthlyAmountOfSalesFormViewModel()
            {
                StartDate = DateTime.Now.Date.AddMonths(-12).ToString("MM.yyyy"),
                EndDate = DateTime.Now.Date.ToString("MM.yyyy"),
                TypeOfGraph = "bar",
                GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Słupkowy", Value = "bar" },
                    new SelectListItem { Text = "Liniowy", Value = "line" }
                }
            };

            return PartialView("_MonthlyRegisteredAccountsForm", monthlyAmountOfSalesFormViewModel);
        }

        public ActionResult DefaultMonthlyRegisteredAccounts()
        {
            DateTime startDate = DateTime.Now.Date.AddMonths(-12);
            DateTime endDate = DateTime.Now.Date;

            var monthlyAmountOfSales = statisticService.GetMonthlyRegisteredAccounts(startDate, endDate);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = "bar",
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = monthlyAmountOfSales.Labels
                }
            };

            graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
            {
                label = "Liczba zarejestrowanych kont",
                data = monthlyAmountOfSales.Data,
                backgroundColor = "#ff4d4d",
                borderColor = "#ff4d4d",
                borderWidth = 1,
                fill = false
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenderMonthlyRegisteredAccounts(MonthlyAmountOfSalesFormViewModel monthlyAmountOfSalesFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                monthlyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_MonthlyRegisteredAccountsForm", monthlyAmountOfSalesFormViewModel);
            }

            DateTime startDateTime = monthlyAmountOfSalesFormViewModel.StartDate.AsDateTime();
            DateTime endDateTime = monthlyAmountOfSalesFormViewModel.EndDate.AsDateTime();

            if (startDateTime > endDateTime)
            {
                ModelState.AddModelError("StartDate", "Miesiąc początkowy nie może być późniejszy od miesiąca końcowego");
                ModelState.AddModelError("EndDate", "Miesiąc końcowy nie może być wcześniejszy od miesiąca początkowego");

                monthlyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_MonthlyRegisteredAccountsForm", monthlyAmountOfSalesFormViewModel);
            }

            if (Math.Abs(12 * (startDateTime.Year - endDateTime.Year) + startDateTime.Month - endDateTime.Month) > 12)
            {
                ModelState.AddModelError("StartDate", "Możesz wygenerować maksymalnie 12 miesięczny raport");
                ModelState.AddModelError("EndDate", "Możesz wygenerować maksymalnie 12 miesięczny raport");

                monthlyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_MonthlyRegisteredAccountsForm", monthlyAmountOfSalesFormViewModel);
            }

            var dailyAmountOfSales = statisticService.GetMonthlyRegisteredAccounts(startDateTime, endDateTime);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = monthlyAmountOfSalesFormViewModel.TypeOfGraph,
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
            {
                label = "Liczba zarejestrowanych kont",
                data = dailyAmountOfSales.Data,
                backgroundColor = "#ff4d4d",
                borderColor = "#ff4d4d",
                borderWidth = 1,
                fill = false
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AnnualRegisteredAccountsForm()
        {
            var annualAmountOfSalesFormViewModel = new AnnualAmountOfSalesFormViewModel
            {
                StartDate = DateTime.Now.Date.AddYears(-10).ToString("yyyy"),
                EndDate = DateTime.Now.Date.ToString("yyyy"),
                TypeOfGraph = "bar",
                GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Słupkowy", Value = "bar" },
                    new SelectListItem { Text = "Liniowy", Value = "line" }
                }
            };

            return PartialView("_AnnualRegisteredAccountsForm", annualAmountOfSalesFormViewModel);
        }

        public ActionResult DefaultAnnualRegisteredAccounts()
        {
            DateTime startDate = DateTime.Now.Date.AddYears(-10);
            DateTime endDate = DateTime.Now.Date;

            var annualAmountOfSales = statisticService.GetAnnualRegisteredAccounts(startDate, endDate);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = "bar",
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = annualAmountOfSales.Labels
                }
            };

            graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
            {
                label = "Liczba zarejestrowanych kont",
                data = annualAmountOfSales.Data,
                backgroundColor = "#32CD32",
                borderColor = "#32CD32",
                borderWidth = 1,
                fill = false
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenderAnnualRegisteredAccounts(AnnualAmountOfSalesFormViewModel annualAmountOfSalesFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                annualAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AnnualRegisteredAccountsForm", annualAmountOfSalesFormViewModel);
            }

            DateTime startDateTime = new DateTime(int.Parse(annualAmountOfSalesFormViewModel.StartDate), 1, 1);
            DateTime endDateTime = new DateTime(int.Parse(annualAmountOfSalesFormViewModel.EndDate), 1, 1);

            if (startDateTime > endDateTime)
            {
                ModelState.AddModelError("StartDate", "Rok początkowy nie może być późniejszy od roku końcowego");
                ModelState.AddModelError("EndDate", "Rok końcowy nie może być wcześniejszy od roku początkowego");

                annualAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AnnualRegisteredAccountsForm", annualAmountOfSalesFormViewModel);
            }

            if (Math.Abs(startDateTime.Year - endDateTime.Year) > 10)
            {
                ModelState.AddModelError("StartDate", "Możesz wygenerować maksymalnie 10 roczny raport");
                ModelState.AddModelError("EndDate", "Możesz wygenerować maksymalnie 10 roczny raport");

                annualAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AnnualRegisteredAccountsForm", annualAmountOfSalesFormViewModel);
            }

            var dailyAmountOfSales = statisticService.GetAnnualRegisteredAccounts(startDateTime, endDateTime);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = annualAmountOfSalesFormViewModel.TypeOfGraph,
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
            {
                label = "Liczba zarejestrowanych kont",
                data = dailyAmountOfSales.Data,
                backgroundColor = "#32CD32",
                borderColor = "#32CD32",
                borderWidth = 1,
                fill = false
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }
    }
}