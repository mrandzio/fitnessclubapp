﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class UsersInEventsByNameStatisticsController : Controller
    {
        private readonly List<string> colors = new List<string>
        {
            "#6495ED",
            "#FF4D4D",
            "#FFA500",
            "#32CD32",
            "#A0522D",
            "#DA70D6",
            "#708090",
            "#008080",
            "#FA8072",
            "#FF00FF",
            "#D2B48C"
        };

        private readonly IStatisticService statisticService;

        public UsersInEventsByNameStatisticsController(IStatisticService statisticService)
        {
            this.statisticService = statisticService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DefaultDailyAmountTypesOfSales()
        {
            DateTime startDate = DateTime.Now.Date.AddDays(-7);
            DateTime endDate = DateTime.Now.Date;

            var dailyAmountOfSales = statisticService.GetDailyUsersInEventsByName(startDate, endDate);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = "bar",
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            int i = 0;
            foreach (var amountOfObject in dailyAmountOfSales.Data)
            {
                graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
                {
                    label = amountOfObject.Key,
                    data = amountOfObject.Count,
                    backgroundColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderWidth = 1,
                    fill = false
                });

                i++;
            }

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DailyAmountTypesOfSalesForm()
        {
            var dailyAmountOfSalesViewModel = new DailyAmountOfSalesFormViewModel
            {
                StartDate = DateTime.Now.Date.AddDays(-7).ToString("dd.MM.yyyy"),
                EndDate = DateTime.Now.Date.ToString("dd.MM.yyyy"),
                TypeOfGraph = "bar",
                GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Słupkowy", Value = "bar" },
                    new SelectListItem { Text = "Liniowy", Value = "line" }
                }
            };

            return PartialView("_DailyUsersInEventsByNameForm", dailyAmountOfSalesViewModel);
        }

        public ActionResult RenderDailyAmountTypesOfSales(DailyAmountOfSalesFormViewModel dailyAmountOfSalesFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                dailyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_DailyUsersInEventsByNameForm", dailyAmountOfSalesFormViewModel);
            }

            DateTime startDateTime = dailyAmountOfSalesFormViewModel.StartDate.AsDateTime();
            DateTime endDateTime = dailyAmountOfSalesFormViewModel.EndDate.AsDateTime();

            if (startDateTime > endDateTime)
            {
                ModelState.AddModelError("StartDate", "Data początkowa nie może być późniejsza od daty końcowej");
                ModelState.AddModelError("EndDate", "Data końcowa nie może być wcześniejsza od daty początkowej");

                dailyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_DailyUsersInEventsByNameForm", dailyAmountOfSalesFormViewModel);
            }

            if (endDateTime.Subtract(startDateTime).Days > 7)
            {
                ModelState.AddModelError("StartDate", "Możesz wygenerować maksymalnie 7 dniowy raport");
                ModelState.AddModelError("EndDate", "Możesz wygenerować maksymalnie 7 dniowy raport");

                dailyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_DailyUsersInEventsByNameForm", dailyAmountOfSalesFormViewModel);
            }

            var dailyAmountOfSales = statisticService.GetDailyUsersInEventsByName(startDateTime, endDateTime);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = dailyAmountOfSalesFormViewModel.TypeOfGraph,
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            int i = 0;
            foreach (var amountOfObject in dailyAmountOfSales.Data)
            {
                graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
                {
                    label = amountOfObject.Key,
                    data = amountOfObject.Count,
                    backgroundColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderWidth = 1,
                    fill = false
                });

                i++;
            }

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MonthlyAmountTypesOfSalesForm()
        {
            var monthlyAmountOfSalesFormViewModel = new MonthlyAmountOfSalesFormViewModel()
            {
                StartDate = DateTime.Now.Date.AddMonths(-6).ToString("MM.yyyy"),
                EndDate = DateTime.Now.Date.ToString("MM.yyyy"),
                TypeOfGraph = "bar",
                GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Słupkowy", Value = "bar" },
                    new SelectListItem { Text = "Liniowy", Value = "line" }
                }
            };

            return PartialView("_MonthlyUsersInEventsByNameForm", monthlyAmountOfSalesFormViewModel);
        }

        public ActionResult DefaultMonthlyAmountTypesOfSales()
        {
            DateTime startDate = DateTime.Now.Date.AddMonths(-6);
            DateTime endDate = DateTime.Now.Date;

            var dailyAmountOfSales = statisticService.GetMonthlyUsersInEventsByName(startDate, endDate);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = "bar",
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            int i = 0;
            foreach (var amountOfObject in dailyAmountOfSales.Data)
            {
                graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
                {
                    label = amountOfObject.Key,
                    data = amountOfObject.Count,
                    backgroundColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderWidth = 1,
                    fill = false
                });

                i++;
            }

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenderMonthlyAmountTypesOfSales(MonthlyAmountOfSalesFormViewModel monthlyAmountOfSalesFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                monthlyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_MonthlyUsersInEventsByNameForm", monthlyAmountOfSalesFormViewModel);
            }

            DateTime startDateTime = monthlyAmountOfSalesFormViewModel.StartDate.AsDateTime();
            DateTime endDateTime = monthlyAmountOfSalesFormViewModel.EndDate.AsDateTime();

            if (startDateTime > endDateTime)
            {
                ModelState.AddModelError("StartDate", "Miesiąc początkowy nie może być późniejszy od miesiąca końcowego");
                ModelState.AddModelError("EndDate", "Miesiąc końcowy nie może być wcześniejszy od miesiąca początkowego");

                monthlyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_MonthlyUsersInEventsByNameForm", monthlyAmountOfSalesFormViewModel);
            }

            if (Math.Abs(12 * (startDateTime.Year - endDateTime.Year) + startDateTime.Month - endDateTime.Month) > 6)
            {
                ModelState.AddModelError("StartDate", "Możesz wygenerować maksymalnie 6 miesięczny raport");
                ModelState.AddModelError("EndDate", "Możesz wygenerować maksymalnie 6 miesięczny raport");

                monthlyAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_MonthlyUsersInEventsByNameForm", monthlyAmountOfSalesFormViewModel);
            }

            var dailyAmountOfSales = statisticService.GetMonthlyUsersInEventsByName(startDateTime, endDateTime);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = monthlyAmountOfSalesFormViewModel.TypeOfGraph,
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            int i = 0;
            foreach (var amountOfObject in dailyAmountOfSales.Data)
            {
                graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
                {
                    label = amountOfObject.Key,
                    data = amountOfObject.Count,
                    backgroundColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderWidth = 1,
                    fill = false
                });

                i++;
            }

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AnnualAmountTypesOfSalesForm()
        {
            var annualAmountOfSalesFormViewModel = new AnnualAmountOfSalesFormViewModel
            {
                StartDate = DateTime.Now.Date.AddYears(-5).ToString("yyyy"),
                EndDate = DateTime.Now.Date.ToString("yyyy"),
                TypeOfGraph = "bar",
                GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Słupkowy", Value = "bar" },
                    new SelectListItem { Text = "Liniowy", Value = "line" }
                }
            };

            return PartialView("_AnnualUsersInEventsByNameForm", annualAmountOfSalesFormViewModel);
        }

        public ActionResult DefaultAnnualAmountTypesOfSales()
        {
            DateTime startDate = DateTime.Now.Date.AddYears(-5);
            DateTime endDate = DateTime.Now.Date;

            var dailyAmountOfSales = statisticService.GetAnnualUsersInEventsByName(startDate, endDate);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = "bar",
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            int i = 0;
            foreach (var amountOfObject in dailyAmountOfSales.Data)
            {
                graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
                {
                    label = amountOfObject.Key,
                    data = amountOfObject.Count,
                    backgroundColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderWidth = 1,
                    fill = false
                });

                i++;
            }

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenderAnnualAmountTypesOfSales(AnnualAmountOfSalesFormViewModel annualAmountOfSalesFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                annualAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AnnualUsersInEventsByNameForm", annualAmountOfSalesFormViewModel);
            }

            DateTime startDateTime = new DateTime(int.Parse(annualAmountOfSalesFormViewModel.StartDate), 1, 1);
            DateTime endDateTime = new DateTime(int.Parse(annualAmountOfSalesFormViewModel.EndDate), 1, 1);

            if (startDateTime > endDateTime)
            {
                ModelState.AddModelError("StartDate", "Rok początkowy nie może być późniejszy od roku końcowego");
                ModelState.AddModelError("EndDate", "Rok końcowy nie może być wcześniejszy od roku początkowego");

                annualAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AnnualUsersInEventsByNameForm", annualAmountOfSalesFormViewModel);
            }

            if (Math.Abs(startDateTime.Year - endDateTime.Year) > 5)
            {
                ModelState.AddModelError("StartDate", "Możesz wygenerować maksymalnie 5 roczny raport");
                ModelState.AddModelError("EndDate", "Możesz wygenerować maksymalnie 5 roczny raport");

                annualAmountOfSalesFormViewModel.GraphTypes = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Słupkowy", Value = "bar"},
                    new SelectListItem {Text = "Liniowy", Value = "line"}
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AnnualUsersInEventsByNameForm", annualAmountOfSalesFormViewModel);
            }

            var dailyAmountOfSales = statisticService.GetAnnualUsersInEventsByName(startDateTime, endDateTime);

            var graphViewModel = new DisplayDefaultGraphViewModel
            {
                type = annualAmountOfSalesFormViewModel.TypeOfGraph,
                data = new DisplayDefaultGraphViewModel.GraphContent
                {
                    labels = dailyAmountOfSales.Labels
                }
            };

            int i = 0;
            foreach (var amountOfObject in dailyAmountOfSales.Data)
            {
                graphViewModel.data.datasets.Add(new DisplayDefaultGraphViewModel.GraphContent.GraphData
                {
                    label = amountOfObject.Key,
                    data = amountOfObject.Count,
                    backgroundColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderColor = i <= colors.Count - 1 ? colors[i] : string.Empty,
                    borderWidth = 1,
                    fill = false
                });

                i++;
            }

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChosenSubscription()
        {
            var chosenSubscription = statisticService.GetChosenSubscription();

            var graphViewModel = new DisplayPieGraphViewModel
            {
                labels = chosenSubscription.Labels
            };

            graphViewModel.datasets.Add(new DisplayPieGraphViewModel.GraphData
            {
                data = chosenSubscription.Data,
                backgroundColor = colors.GetRange(0, chosenSubscription.Data.Count),
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChosenEvents()
        {
            var chosenSubscription = statisticService.GetChosenEvents();

            var graphViewModel = new DisplayPieGraphViewModel
            {
                labels = chosenSubscription.Labels
            };

            graphViewModel.datasets.Add(new DisplayPieGraphViewModel.GraphData
            {
                data = chosenSubscription.Data,
                backgroundColor = colors.GetRange(0, chosenSubscription.Data.Count),
            });

            return Json(graphViewModel, JsonRequestBehavior.AllowGet);
        }
    }
}