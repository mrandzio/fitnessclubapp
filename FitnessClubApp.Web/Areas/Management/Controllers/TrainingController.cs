﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class TrainingController : Controller
    {
        private readonly IResourceService resourceService;

        public TrainingController(IResourceService resourceService)
        {
            this.resourceService = resourceService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllTrainings()
        {
            var trainings = resourceService.GetTrainings().OrderBy(r => r.Name).ToList();
            return PartialView("_AllTrainings", trainings);
        }

        [HttpGet]
        public ActionResult AddTraining()
        {
            return PartialView("_AddTrainingForm");
        }

        [HttpGet]
        public ActionResult EditTraining(int trainingID)
        {
            var training = resourceService.GetTraining(trainingID);

            if (training == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var trainingFormViewModel = Mapper.Map<Training, EditTrainingFormViewModel>(training);

            return PartialView("_EditTrainingForm", trainingFormViewModel);
        }

        [HttpGet]
        public ActionResult EditImageTraining(int trainingID)
        {
            var training = resourceService.GetTraining(trainingID);

            if (training == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var trainingFormViewModel = Mapper.Map<Training, EditImageTrainingFormViewModel>(training);

            return PartialView("_EditImageTrainingForm", trainingFormViewModel);
        }

        [HttpPost]
        public ActionResult AddTraining(AddTrainingFormViewModel trainingFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AddTrainingForm", trainingFormViewModel);
            }

            var training = Mapper.Map<AddTrainingFormViewModel, Training>(trainingFormViewModel);

            var trainingExists = resourceService.TrainingExists(training);
            if (trainingExists)
            {
                ModelState.AddModelError("Name", "Zajęcia o podanej nazwie już istnieją");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AddTrainingForm", trainingFormViewModel);
            }

            resourceService.CreateTraining(training);
            resourceService.SaveTraining();

            return GetAllTrainings();
        }

        [HttpPost]
        public ActionResult EditTraining(EditTrainingFormViewModel trainingFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditTrainingForm", trainingFormViewModel);
            }

            var training = resourceService.GetTraining(trainingFormViewModel.TrainingID);
            if (training == null)
            {
                ModelState.AddModelError("Name", "Niepoprawny identyfikator zajęć");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditTrainingForm", trainingFormViewModel);
            }

            training = Mapper.Map(trainingFormViewModel, training);

            var trainingExists = resourceService.TrainingExists(training);
            if (trainingExists)
            {
                ModelState.AddModelError("Name", "Zajęcia o podanej nazwie już istnieją");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditTrainingForm", trainingFormViewModel);
            }

            resourceService.EditTraining(training);
            resourceService.SaveRoom();

            return GetAllTrainings();
        }

        [HttpPost]
        public ActionResult EditImageTraining(EditImageTrainingFormViewModel trainingFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditImageTrainingForm", trainingFormViewModel);
            }

            var training = resourceService.GetTraining(trainingFormViewModel.TrainingID);
            if (training == null)
            {
                ModelState.AddModelError("Name", "Niepoprawny identyfikator zajęć");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditImageTrainingForm", trainingFormViewModel);
            }

            training = Mapper.Map(trainingFormViewModel, training);

            resourceService.EditTraining(training);
            resourceService.SaveRoom();

            return GetAllTrainings();
        }

        [HttpGet]
        public ActionResult RemoveTraining(int trainingID)
        {
            var training = resourceService.GetTraining(trainingID);

            if (training == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("_ConfirmationRemoveTraining", training);
        }

        [HttpPost]
        public ActionResult ConfirmationRemoveTraining(int trainingID)
        {
            var training = resourceService.GetTraining(trainingID);

            if (training == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            resourceService.RemoveTraining(training);
            resourceService.SaveTraining();

            return GetAllTrainings();
        }
    }
}