﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Trainer,Receptionist,Manager,Admin")]
    public class MainPanelController : Controller
    {
        private readonly IStatisticService statisticService;

        public MainPanelController(IStatisticService statisticService)
        {
            this.statisticService = statisticService;
        }

        public ActionResult Index()
        {
            var mainPanelData = new DisplayMainPanelData
            {
                DailyRegisteredEntries = statisticService.GetDailyRegisteredEntries(),
                DailyUsersInEvents = statisticService.GetDailyUsersInEvents(),
                DailyRegisteredAccounts = statisticService.GetDailyRegisteredAccounts(),
                DailyTransactions = statisticService.GetDailyAmountOfSales(),
                MonthlyRegisteredAccounts = statisticService.GetMonthlyRegisteredAccounts(),
                MonthlyRegisteredEntries = statisticService.GetMonthlyRegisteredEntries(),
                MonthlyTransactions = statisticService.GetMonthlyAmountOfSales(),
                MonthlyUsersInEvents = statisticService.GetMonthlyUsersInEvents(),
                AnnualRegisteredAccounts = statisticService.GetAnnualRegisteredAccounts(),
                AnnualRegisteredEntries = statisticService.GetAnnualRegisteredEntries(),
                AnnualTransactions = statisticService.GetAnnualAmountOfSales(),
                AnnualUsersInEvents = statisticService.GetAnnualUsersInEvents(),
                AllRegisteredAccounts = statisticService.GetAllRegisteredAccounts(),
                AllRegisteredEntries = statisticService.GetAllRegisteredEntries(),
                AllTransactions = statisticService.GetAllAmountOfSales(),
                AllUsersInEvents = statisticService.GetAllUsersInEvents(),
                AmountOfEquipments = statisticService.GetAmountOfEquipments(),
                AmountOfManagers = statisticService.GetAmountOfManagers(),
                AmountOfReceptionists = statisticService.GetAmountOfReceptionists(),
                AmountOfRooms = statisticService.GetAmountOfRooms(),
                AmountOfTrainers = statisticService.GetAmountOfTrainers(),
                AmountOfTrainings = statisticService.GetAmountOfTrainings(),
                AmountOfSubscriptions = statisticService.GetAmountOfSubscriptions()
            };

            return View(mainPanelData);
        }
    }
}