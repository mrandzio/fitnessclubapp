﻿using FitnessClubApp.Service;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class AccountController : Controller
    {
        private readonly IUserService userService;
        private readonly IResourceService resourceService;

        public AccountController(IUserService userService, IResourceService resourceService)
        {
            this.userService = userService;
            this.resourceService = resourceService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetUserRoles()
        {
            var userRoles = userService.GetUserRoles().ToList();
            var query = Mapper.Map<IEnumerable<UserRole>, IEnumerable<DisplayUserRolesViewModel>>(userRoles);
            var total = userRoles.Count;
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditRoleUser(EditUserRoleFormViewModel userViewModel)
        {
            var user = userService.GetUser(userViewModel.UserID);

            if (user.UserRoleID == userViewModel.UserRoleID)
                return Json(new { result = true });

            if (userService.GetUserRoleName(userViewModel.UserRoleID) == "Trainer")
            {
                MemoryStream stream = new MemoryStream();
                Image.FromFile(Server.MapPath(@"~\Images\avatar.png")).Save(stream, ImageFormat.Png);

                var descriptionOfUser = new DescriptionOfUser
                {
                    DescriptionOfUserID = user.UserID,
                    Description = string.Empty,
                    Image = stream.ToArray()
                };

                stream.Close();

                resourceService.CreateDescriptionOfUser(descriptionOfUser);
                resourceService.SaveDescriptionOfUser();
            }

            if (userService.GetUserRoleName(user.UserRoleID) == "Trainer")
            {
                var descriptionOfUser = resourceService.GetDescriptionOfUser(user.UserID);
                if (descriptionOfUser != null)
                {
                    resourceService.RemoveDescriptionOfUser(descriptionOfUser);
                    resourceService.SaveDescriptionOfUser();
                }
            }

            user = Mapper.Map(userViewModel, user);

            userService.EditUser(user);
            userService.SaveUser();

            return Json(new { result = true });
        }

        public JsonResult GetUsers(int? page, int? limit, string sortBy, string direction, string id, string name, string email)
        {
            List<DisplayAccountViewModel> records;
            int total;
            var users = userService.GetUsersWithRole();

            var query = Mapper.Map<IEnumerable<User>, IEnumerable<DisplayAccountViewModel>>(users);

            if (!string.IsNullOrEmpty(id))
            {
                List<DisplayAccountViewModel> query2 = query.ToList();
                int integerID;
                if (int.TryParse(id, out integerID))
                    query2 = query2.Where(q => q.UserID == integerID).ToList();

                if (query2.Any())
                {
                    total = query2.Count;
                    return Json(new { records = query2, total }, JsonRequestBehavior.AllowGet);
                }
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(q => $"{q.FirstName} {q.LastName}".ToLower().Trim().Contains(name.ToLower().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(email))
            {
                query = query.Where(q => q.Email.ToLower().Trim().Contains(email.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderBy(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderBy(q => q.Email);
                            break;
                        case "DateCreated":
                            query = query.OrderBy(q => q.DateCreated);
                            break;
                        case "UserRoleName":
                            query = query.OrderBy(q => q.UserRoleName);
                            break;
                        case "UserID":
                            query = query.OrderBy(q => q.UserID);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderByDescending(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderByDescending(q => q.Email);
                            break;
                        case "DateCreated":
                            query = query.OrderByDescending(q => q.DateCreated);
                            break;
                        case "UserRoleName":
                            query = query.OrderByDescending(q => q.UserRoleName);
                            break;
                        case "UserID":
                            query = query.OrderByDescending(q => q.UserID);
                            break;
                    }
                }
            }
            else
            {
                query = query.OrderByDescending(q => q.DateCreated);
            }

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
    }
}