﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class EquipmentController : Controller
    {
        private readonly IResourceService resourceService;

        public EquipmentController(IResourceService resourceService)
        {
            this.resourceService = resourceService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEquipments(int? page, int? limit, string sortBy, string direction, string brand, string name)
        {
            List<DisplayEquipmentViewModel> records;
            int total;

            var equipments = resourceService.GetEquipments();
            var query = Mapper.Map<IEnumerable<Equipment>, IEnumerable<DisplayEquipmentViewModel>>(equipments);

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(q => q.Name.ToLower().Trim().Contains(name.ToLower().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(brand))
            {
                query = query.Where(q => q.Brand.ToLower().Trim().Contains(brand.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    switch (sortBy)
                    {
                        case "Brand":
                            query = query.OrderBy(q => q.Brand);
                            break;
                        case "Name":
                            query = query.OrderBy(q => q.Name);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "Brand":
                            query = query.OrderByDescending(q => q.Brand);
                            break;
                        case "Name":
                            query = query.OrderByDescending(q => q.Name);
                            break;
                    }
                }
            }
            else
            {
                query = query.OrderBy(q => q.Name);
            }

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddEquipment()
        {
            return PartialView("_AddEquipmentForm");
        }

        [HttpGet]
        public ActionResult EditEquipment(int equipmentID)
        {
            var equipment = resourceService.GetEquipment(equipmentID);

            if (equipment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var equipmentFormViewModel = Mapper.Map<Equipment, EquipmentFormViewModel>(equipment);

            return PartialView("_EditEquipmentForm", equipmentFormViewModel);
        }

        [HttpGet]
        public ActionResult RemoveEquipment(int equipmentID)
        {
            var equipment = resourceService.GetEquipment(equipmentID);

            if (equipment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("_ConfirmationRemoveEquipment", equipment);
        }

        [HttpPost]
        public ActionResult AddEquipment(EquipmentFormViewModel equipmentFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AddEquipmentForm", equipmentFormViewModel);
            }
                
            var equipment = Mapper.Map<EquipmentFormViewModel, Equipment>(equipmentFormViewModel);

            var equipmentExists = resourceService.EquipmentExists(equipment);
            if (equipmentExists)
            {
                ModelState.AddModelError("Name", "Istnieje już sprzęt o podanej nazwie i marce");
                ModelState.AddModelError("Brand", "Istnieje już sprzęt o podanej nazwie i marce");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_AddEquipmentForm", equipmentFormViewModel);
            }

            resourceService.CreateEquipment(equipment);
            resourceService.SaveEquipment();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult EditEquipment(EquipmentFormViewModel equipmentFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditEquipmentForm", equipmentFormViewModel);
            }

            var equipment = Mapper.Map<EquipmentFormViewModel, Equipment>(equipmentFormViewModel);

            var equipmentExists = resourceService.EquipmentExists(equipment);
            if (equipmentExists)
            {
                ModelState.AddModelError("Name", "Istnieje już sprzęt o podanej nazwie i marce");
                ModelState.AddModelError("Brand", "Istnieje już sprzęt o podanej nazwie i marce");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditEquipmentForm", equipmentFormViewModel);
            }

            resourceService.EditEquipment(equipment);
            resourceService.SaveEquipment();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult ConfirmationRemoveEquipment(int equipmentID)
        {
            var equipment = resourceService.GetEquipment(equipmentID);

            if (equipment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            resourceService.RemoveEquipment(equipment);
            resourceService.SaveEquipment();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}