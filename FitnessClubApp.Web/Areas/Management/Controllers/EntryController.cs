﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Trainer,Receptionist,Manager,Admin")]
    public class EntryController : Controller
    {
        private readonly IUserService userService;
        private readonly IEntryRegisterService entryRegisterService;
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;

        public EntryController(IUserService userService, IEntryRegisterService entryRegisterService, IPurchasedSubscriptionService purchasedSubscriptionService)
        {
            this.userService = userService;
            this.entryRegisterService = entryRegisterService;
            this.purchasedSubscriptionService = purchasedSubscriptionService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetUsers(int? page, int? limit, string sortBy, string direction, string id, string name, string email)
        {
            List<DisplayAccountViewModel> records;
            int total;
            var users = userService.GetUsersWithRole().Where(u =>
                u.UserRole.Name == "User" && u.IsEmailVerified &&
                purchasedSubscriptionService.GetActiveUserSubscription(u.Email) != null);

            var query = Mapper.Map<IEnumerable<User>, IEnumerable<DisplayAccountViewModel>>(users);

            if (!string.IsNullOrEmpty(id))
            {
                List<DisplayAccountViewModel> query2 = query.ToList();
                int integerID;
                if (int.TryParse(id, out integerID))
                    query2 = query2.Where(q => q.UserID == integerID).ToList();

                if (query2.Any())
                {
                    total = query2.Count;
                    return Json(new { records = query2, total }, JsonRequestBehavior.AllowGet);
                }
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(q => $"{q.FirstName} {q.LastName}".ToLower().Trim().Contains(name.ToLower().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(email))
            {
                query = query.Where(q => q.Email.ToLower().Trim().Contains(email.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderBy(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderBy(q => q.Email);
                            break;
                        case "UserID":
                            query = query.OrderBy(q => q.UserID);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderByDescending(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderByDescending(q => q.Email);
                            break;
                        case "UserID":
                            query = query.OrderByDescending(q => q.UserID);
                            break;
                    }
                }
            }
            else
            {
                query = query.OrderByDescending(q => q.DateCreated);
            }

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUnfinishedEntries(int? page, int? limit, string sortBy, string direction, string id, string name, string email)
        {
            List<DisplayUnfinishedEntryViewModel> records;
            int total;
            var users = entryRegisterService.GetEntriesRegister().Where(u => !u.EndDate.HasValue);

            var query = Mapper.Map<IEnumerable<Entry>, IEnumerable<DisplayUnfinishedEntryViewModel>>(users);

            if (!string.IsNullOrEmpty(id))
            {
                List<DisplayUnfinishedEntryViewModel> query2 = query.ToList();
                int integerID;
                if (int.TryParse(id, out integerID))
                    query2 = query2.Where(q => q.UserID == integerID).ToList();

                if (query2.Any())
                {
                    total = query2.Count;
                    return Json(new { records = query2, total }, JsonRequestBehavior.AllowGet);
                }
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(q => $"{q.FirstName} {q.LastName}".ToLower().Trim().Contains(name.ToLower().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(email))
            {
                query = query.Where(q => q.Email.ToLower().Trim().Contains(email.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderBy(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderBy(q => q.Email);
                            break;
                        case "UserID":
                            query = query.OrderBy(q => q.UserID);
                            break;
                        case "StartDate":
                            query = query.OrderBy(q => q.StartDate);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderByDescending(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderByDescending(q => q.Email);
                            break;
                        case "UserID":
                            query = query.OrderByDescending(q => q.UserID);
                            break;
                        case "StartDate":
                            query = query.OrderByDescending(q => q.StartDate);
                            break;
                    }
                }
            }
            else
            {
                query = query.OrderByDescending(q => q.StartDate);
            }

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllEntries(int? page, int? limit, string sortBy, string direction, string id, string name, string email)
        {
            List<DisplayCompletedEntryViewModel> records;
            int total;
            var users = entryRegisterService.GetEntriesRegister();

            var query = Mapper.Map<IEnumerable<Entry>, IEnumerable<DisplayCompletedEntryViewModel>>(users);

            if (!string.IsNullOrEmpty(id))
            {
                List<DisplayCompletedEntryViewModel> query2 = query.ToList();
                int integerID;
                if (int.TryParse(id, out integerID))
                    query2 = query2.Where(q => q.UserID == integerID).ToList();

                if (query2.Any())
                {
                    total = query2.Count;
                    return Json(new { records = query2, total }, JsonRequestBehavior.AllowGet);
                }
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(q => $"{q.FirstName} {q.LastName}".ToLower().Trim().Contains(name.ToLower().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(email))
            {
                query = query.Where(q => q.Email.ToLower().Trim().Contains(email.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderBy(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderBy(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderBy(q => q.Email);
                            break;
                        case "UserID":
                            query = query.OrderBy(q => q.UserID);
                            break;
                        case "StartDate":
                            query = query.OrderBy(q => q.StartDate);
                            break;
                        case "EndDate":
                            query = query.OrderBy(q => q.EndDate);
                            break;
                    }
                }
                else
                {
                    switch (sortBy)
                    {
                        case "FirstName":
                            query = query.OrderByDescending(q => q.FirstName);
                            break;
                        case "LastName":
                            query = query.OrderByDescending(q => q.LastName);
                            break;
                        case "Email":
                            query = query.OrderByDescending(q => q.Email);
                            break;
                        case "UserID":
                            query = query.OrderByDescending(q => q.UserID);
                            break;
                        case "StartDate":
                            query = query.OrderByDescending(q => q.StartDate);
                            break;
                        case "EndDate":
                            query = query.OrderByDescending(q => q.EndDate);
                            break;
                    }
                }
            }
            else
            {
                query = query.OrderByDescending(q => q.StartDate);
            }

            total = query.Count();
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = query.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                records = query.ToList();
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddEntry(int userID)
        {
            var user = userService.GetUser(userID);
            if (user == null || userService.GetUserRoleName(user.UserRoleID) != "User" || !user.IsEmailVerified &&
                purchasedSubscriptionService.GetActiveUserSubscription(user.Email) == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var entry = new Entry
            {
                UserID = user.UserID,
                StartDate = DateTime.Now
            };

            var entryExists = entryRegisterService.EntryExists(entry);
            if (entryExists)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            entryRegisterService.CreateEntry(entry);
            entryRegisterService.SaveEntry();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult RemoveEntry(int entryID)
        {
            var entry = entryRegisterService.GetEntry(entryID);
            if (entry == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            entryRegisterService.RemoveEntry(entry);
            entryRegisterService.SaveEntry();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult FinishEntry(int entryID)
        {
            var entry = entryRegisterService.GetEntry(entryID);
            if (entry == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            entry.EndDate = DateTime.Now;
            entryRegisterService.EditEntry(entry);
            entryRegisterService.SaveEntry();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        public ActionResult EditEntry(int entryID)
        {
            var entry = entryRegisterService.GetEntry(entryID);
            if (entry == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var editEntryViewModel = Mapper.Map<Entry, EditEntryViewModel>(entry);

            return PartialView("_EditEntry", editEntryViewModel);
        }

        [HttpPost]
        public ActionResult EditEntry(EditEntryViewModel entryViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditEntry", entryViewModel);
            }

            var entry = entryRegisterService.GetEntry(entryViewModel.EntryID);
            if (entry == null)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditEntry", entryViewModel);
            }

            entry = Mapper.Map(entryViewModel, entry);

            if (entry.StartDate >= entry.EndDate)
            {
                ModelState.AddModelError("StartDate", "Data wejścia nie może być równa lub późniejsza od daty wyjścia");
                ModelState.AddModelError("EndDate", "Data wyjścia nie może być równa lub wcześniejsza od daty wejścia");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditEntry", entryViewModel);
            }

            entryRegisterService.EditEntry(entry);
            entryRegisterService.SaveEntry();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}