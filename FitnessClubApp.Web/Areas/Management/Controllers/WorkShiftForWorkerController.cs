﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using WebGrease.Css.Extensions;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Trainer,Receptionist")]
    public class WorkShiftForWorkerController : Controller
    {
        private readonly IWorkScheduleService workScheduleService;
        private readonly IResourceService resourceService;
        private readonly IUserService userService;

        public WorkShiftForWorkerController(IWorkScheduleService workScheduleService,
            IResourceService resourceService, IUserService userService)
        {
            this.workScheduleService = workScheduleService;
            this.resourceService = resourceService;
            this.userService = userService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEventEvents(DateTime start, DateTime end)
        {
            var user = userService.GetUser(User.Identity.Name);
            var events = workScheduleService.GetUserEvents(user.UserID, start, end);

            var eventsAfterMapping =
                Mapper.Map<IEnumerable<Event>, IEnumerable<DisplaySingleDataForWorkerViewModel>>(events);

            return Json(eventsAfterMapping, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWorkShiftEvents(DateTime start, DateTime end)
        {
            var user = userService.GetUser(User.Identity.Name);
            var workShifts = workScheduleService.GetUserWorkShifts(user.UserID, start, end);

            var workShiftsAfterMapping =
                Mapper.Map<IEnumerable<WorkShift>, IEnumerable<DisplaySingleDataForWorkerViewModel>>(workShifts);

            return Json(workShiftsAfterMapping, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllResource()
        {
            var workers = workScheduleService.GetWorkers();
            var rooms = workScheduleService.GetRooms();

            var resource = Mapper.Map<IEnumerable<User>, IEnumerable<DisplayAllResourceViewModel>>(workers);
            resource.ForEach(res =>
            {
                res.children = Mapper.Map<IEnumerable<Room>, IEnumerable<DisplayAllResourceViewModel.Room>>(rooms);
                res.children.ForEach(room => room.id += $"_{res.id}");
            });

            return Json(resource, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllEvents(DateTime start, DateTime end)
        {
            var user = userService.GetUser(User.Identity.Name);
            var workShifts = workScheduleService.GetWorkShifts(start, end);
            var events = workScheduleService.GetEvents(start, end);

            var eventsInSchedule = Mapper.Map<IEnumerable<WorkShift>, IEnumerable<DisplayAllEventViewModel>>(workShifts)
                .Concat(Mapper.Map<IEnumerable<Event>, IEnumerable<DisplayAllEventViewModel>>(events));

            foreach (var @event in eventsInSchedule)
            {
                if (@event.userId != user.UserID)
                    continue;

                if (@event.id[0] == 'W')
                    @event.color = "#DA4A2A";
                else if (@event.id[0] == 'E')
                    @event.color = "#87CEFA";
            }

            return Json(eventsInSchedule, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWorkShiftAndEventEvents(DateTime start, DateTime end)
        {
            var user = userService.GetUser(User.Identity.Name);
            var workShifts = workScheduleService.GetUserWorkShifts(user.UserID, start, end);
            var events = workScheduleService.GetUserEvents(user.UserID, start, end);

            var eventsInSchedule = Mapper.Map<IEnumerable<WorkShift>, IEnumerable<DisplayMultipleDataForWorkerViewModel>>(workShifts)
                .Concat(Mapper.Map<IEnumerable<Event>, IEnumerable<DisplayMultipleDataForWorkerViewModel>>(events));

            return Json(eventsInSchedule, JsonRequestBehavior.AllowGet);
        }
    }
}