﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]

    public class DescriptionOfWorkerController : Controller
    {
        private readonly IResourceService resourceService;

        public DescriptionOfWorkerController(IResourceService resourceService)
        {
            this.resourceService = resourceService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllDescriptions()
        {
            var descriptionsOfUsers = resourceService.GetDescriptionsOfUsers().OrderBy(d => $"{d.User.FirstName} {d.User.LastName}").ToList();
            return PartialView("_AllDescriptionsOfWorkers", descriptionsOfUsers);
        }

        [HttpGet]
        public ActionResult EditDescription(int descriptionID)
        {
            var descriptionOfUser = resourceService.GetDescriptionOfUser(descriptionID);

            if (descriptionOfUser == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var descriptionWorkerFormViewModel = Mapper.Map<DescriptionOfUser, EditDescriptionWorkerFormViewModel>(descriptionOfUser);

            return PartialView("_EditDescriptionWorkerForm", descriptionWorkerFormViewModel);
        }

        [HttpGet]
        public ActionResult EditImageWorker(int descriptionID)
        {
            var descriptionOfUser = resourceService.GetDescriptionOfUser(descriptionID);

            if (descriptionOfUser == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var imageWorkerFormViewModel = Mapper.Map<DescriptionOfUser, EditImageWorkerFormViewModel>(descriptionOfUser);

            return PartialView("_EditImageWorkerForm", imageWorkerFormViewModel);
        }

    
        [HttpPost]
        public ActionResult EditDescription(EditDescriptionWorkerFormViewModel descriptionWorkerFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditDescriptionWorkerForm", descriptionWorkerFormViewModel);
            }

            var descriptionOfUser = resourceService.GetDescriptionOfUser(descriptionWorkerFormViewModel.DescriptionOfUserID);
            if (descriptionOfUser == null)
            {
                ModelState.AddModelError("Name", "Niepoprawny identyfikator zajęć");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditDescriptionWorkerForm", descriptionWorkerFormViewModel);
            }

            descriptionOfUser = Mapper.Map(descriptionWorkerFormViewModel, descriptionOfUser);

            resourceService.EditDescriptionOfUser(descriptionOfUser);
            resourceService.SaveDescriptionOfUser();

            return GetAllDescriptions();
        }

        [HttpPost]
        public ActionResult EditImageWorker(EditImageWorkerFormViewModel imageWorkerFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditImageWorkerForm", imageWorkerFormViewModel);
            }

            var descriptionOfUser = resourceService.GetDescriptionOfUser(imageWorkerFormViewModel.DescriptionOfUserID);
            if (descriptionOfUser == null)
            {
                ModelState.AddModelError("Name", "Niepoprawny identyfikator zajęć");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_EditImageWorkerForm", imageWorkerFormViewModel);
            }

            descriptionOfUser = Mapper.Map(imageWorkerFormViewModel, descriptionOfUser);

            resourceService.EditDescriptionOfUser(descriptionOfUser);
            resourceService.SaveDescriptionOfUser();

            return GetAllDescriptions();
        }
    }
}