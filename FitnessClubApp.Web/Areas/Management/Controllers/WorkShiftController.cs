﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using Newtonsoft.Json;
using WebGrease.Css.Extensions;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class WorkShiftController : Controller
    {
        private readonly IWorkScheduleService workScheduleService;
        private readonly IResourceService resourceService;

        public WorkShiftController(IWorkScheduleService workScheduleService, IResourceService resourceService)
        {
            this.workScheduleService = workScheduleService;
            this.resourceService = resourceService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetWorkScheduleResource()
        {
            var workers = workScheduleService.GetWorkers();
            var rooms = workScheduleService.GetRooms();

            var resource = Mapper.Map<IEnumerable<User>, IEnumerable<DisplayWorkScheduleResourceViewModel>>(workers);
            resource.ForEach(res =>
            {
                res.children = Mapper.Map<IEnumerable<Room>, IEnumerable<DisplayWorkScheduleResourceViewModel.Room>>(rooms);
                res.children.ForEach(room => room.id += $"_{res.id}");
            });

            return Json(resource, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWorkScheduleEvents(DateTime start, DateTime end)
        {
            var workShifts = workScheduleService.GetWorkShifts(start, end);
            var events = workScheduleService.GetEvents(start, end);

            var eventsInSchedule = Mapper.Map<IEnumerable<WorkShift>, IEnumerable<DisplayWorkScheduleEventViewModel>>(workShifts)
                .Concat(Mapper.Map<IEnumerable<Event>, IEnumerable<DisplayWorkScheduleEventViewModel>>(events));

            return Json(eventsInSchedule, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisposableWorkShiftForm()
        {
            var workers = workScheduleService.GetWorkers();
            var disposableWorkShiftFormViewModel = new DisposableWorkShiftFormViewModel
            {
                Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers)
            };
            return PartialView("_DisposableWorkShiftForm", disposableWorkShiftFormViewModel);
        }

        public ActionResult PeriodicWorkShiftForm()
        {
            var workers = workScheduleService.GetWorkers();
            var periodicWorkShiftFormViewModel = new PeriodicWorkShiftFormViewModel()
            {
                Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers),
                DaysOfWeek = new List<SelectListItem> {
                    new SelectListItem { Value = "2", Text = "Poniedziałek"},
                    new SelectListItem { Value = "3", Text = "Wtorek" },
                    new SelectListItem { Value = "4", Text = "Środa" },
                    new SelectListItem { Value = "5", Text = "Czwartek" },
                    new SelectListItem { Value = "6", Text = "Piątek" },
                    new SelectListItem { Value = "7", Text = "Sobota" },
                    new SelectListItem { Value = "1", Text = "Niedziela" }
                }
            };
            return PartialView("_PeriodicWorkShiftForm", periodicWorkShiftFormViewModel);
        }

        public ActionResult DisposableEventForm()
        {
            var workers = workScheduleService.GetWorkers();
            var rooms = workScheduleService.GetRooms();
            var trainings = resourceService.GetTrainings();
            var disposableEventFormViewModel = new DisposableEventFormViewModel
            {
                Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers),
                Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms),
                Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings)
            };
            return PartialView("_DisposableEventForm", disposableEventFormViewModel);
        }

        public ActionResult Details(string elementID)
        {
            if (elementID[0] == 'W')
            {
                var workShiftID = int.Parse(elementID.Substring(2));
                var workShift = workScheduleService.GetWorkShift(workShiftID);

                return PartialView("_DisplayWorkShiftDetails", workShift);
            }

            var eventID = int.Parse(elementID.Substring(2));
            var @event = workScheduleService.GetEvent(eventID);

            return PartialView("_DisplayEventDetails", @event);
        }

        public ActionResult PeriodicEventForm()
        {
            var workers = workScheduleService.GetWorkers();
            var rooms = workScheduleService.GetRooms();
            var trainings = resourceService.GetTrainings();
            var periodicEventFormViewModel = new PeriodicEventFormViewModel()
            {
                Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers),
                Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms),
                Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings),
                DaysOfWeek = new List<SelectListItem>
                {
                    new SelectListItem { Value = "2", Text = "Poniedziałek"},
                    new SelectListItem { Value = "3", Text = "Wtorek" },
                    new SelectListItem { Value = "4", Text = "Środa" },
                    new SelectListItem { Value = "5", Text = "Czwartek" },
                    new SelectListItem { Value = "6", Text = "Piątek" },
                    new SelectListItem { Value = "7", Text = "Sobota" },
                    new SelectListItem { Value = "1", Text = "Niedziela" }
                }
            };
            return PartialView("_PeriodicEventForm", periodicEventFormViewModel);
        }

        public IEnumerable<User> GetWorkers()
        {
            var workers = workScheduleService.GetWorkers();
            return workers;
        }

        [HttpPost]
        public ActionResult AddDisposableWorkShift(DisposableWorkShiftFormViewModel disposableWorkShift)
        {
            if (!ModelState.IsValid)
            {
                var workers = workScheduleService.GetWorkers();
                disposableWorkShift.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_DisposableWorkShiftForm", disposableWorkShift);
            }

            var workShift = Mapper.Map<DisposableWorkShiftFormViewModel, WorkShift>(disposableWorkShift);

            if (workShift.StartDate >= workShift.EndDate)
            {
                ModelState.AddModelError("StartDate", "Data rozpoczęcia nie może być równa lub późniejsza od daty zakończenia");
                ModelState.AddModelError("EndDate", "Data zakończenia nie może być równa lub wcześniejsza od daty rozpoczęcia");

                var workers = workScheduleService.GetWorkers();
                disposableWorkShift.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_DisposableWorkShiftForm", disposableWorkShift);
            }

            var workShiftExists = workScheduleService.WorkShiftExists(workShift);
            if (workShiftExists)
            {
                ModelState.AddModelError("StartDate", "W wybranych datach został już zdefiniowany dyżur dla wybranego pracownika");
                ModelState.AddModelError("EndDate", "W wybranych datach został już zdefiniowany dyżur dla wybranego pracownika");

                var workers = workScheduleService.GetWorkers();
                disposableWorkShift.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_DisposableWorkShiftForm", disposableWorkShift);
            }

            workScheduleService.CreateWorkShift(workShift);
            workScheduleService.SaveWorkShift();

            return DisposableWorkShiftForm();
        }

        [HttpPost]
        public ActionResult AddDisposableEvent(DisposableEventFormViewModel disposableEvent)
        {
            if (!ModelState.IsValid)
            {
                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                disposableEvent.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                disposableEvent.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                disposableEvent.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_DisposableEventForm", disposableEvent);
            }

            var @event = Mapper.Map<DisposableEventFormViewModel, Event>(disposableEvent);

            if (@event.StartDate >= @event.EndDate)
            {
                ModelState.AddModelError("StartDate", "Data rozpoczęcia nie może być równa lub późniejsza od daty zakończenia");
                ModelState.AddModelError("EndDate", "Data zakończenia nie może być równa lub wcześniejsza od daty rozpoczęcia");

                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                disposableEvent.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                disposableEvent.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                disposableEvent.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_DisposableEventForm", disposableEvent);
            }

            var userWorkShift = workScheduleService.GetWorkShift(disposableEvent.WorkerID, @event.StartDate, @event.EndDate);
            if (userWorkShift == null)
            {
                ModelState.AddModelError("StartDate", "W wybranych datach nie ma zdefiniowanego dyżuru dla wybranego pracownika");
                ModelState.AddModelError("EndDate", "W wybranych datach nie ma zdefiniowanego dyżuru dla wybranego pracownika");

                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                disposableEvent.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                disposableEvent.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                disposableEvent.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_DisposableEventForm", disposableEvent);
            }

            @event.WorkShiftID = userWorkShift.WorkShiftID;

            var workShiftExists = workScheduleService.EventExists(@event);
            if (workShiftExists)
            {
                ModelState.AddModelError("StartDate", "W wybranych datach zostały już zdefiniowane zajęcia dla wybranego pracownika lub sali");
                ModelState.AddModelError("EndDate", "W wybranych datach zostały już zdefiniowane zajęcia dla wybranego pracownika lub sali");

                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                disposableEvent.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                disposableEvent.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                disposableEvent.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_DisposableEventForm", disposableEvent);
            }

            workScheduleService.CreateEvent(@event);
            workScheduleService.SaveEvent();

            return DisposableEventForm();
        }

        [HttpPost]
        public ActionResult AddPeriodicWorkShift(PeriodicWorkShiftFormViewModel periodicWorkShift)
        {
            if (!ModelState.IsValid)
            {
                var workers = workScheduleService.GetWorkers();
                periodicWorkShift.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                periodicWorkShift.DaysOfWeek = new List<SelectListItem>
                {
                    new SelectListItem { Value = "2", Text = "Poniedziałek"},
                    new SelectListItem { Value = "3", Text = "Wtorek" },
                    new SelectListItem { Value = "4", Text = "Środa" },
                    new SelectListItem { Value = "5", Text = "Czwartek" },
                    new SelectListItem { Value = "6", Text = "Piątek" },
                    new SelectListItem { Value = "7", Text = "Sobota" },
                    new SelectListItem { Value = "1", Text = "Niedziela" }
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_PeriodicWorkShiftForm", periodicWorkShift);
            }

            if (periodicWorkShift.StartDate.AsDateTime() >= periodicWorkShift.EndDate.AsDateTime())
            {
                ModelState.AddModelError("StartDate", "Data rozpoczęcia nie może być równa lub późniejsza od daty zakończenia");
                ModelState.AddModelError("EndDate", "Data zakończenia nie może być równa lub wcześniejsza od daty rozpoczęcia");

                var workers = workScheduleService.GetWorkers();
                periodicWorkShift.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                periodicWorkShift.DaysOfWeek = new List<SelectListItem>
                {
                    new SelectListItem { Value = "2", Text = "Poniedziałek"},
                    new SelectListItem { Value = "3", Text = "Wtorek" },
                    new SelectListItem { Value = "4", Text = "Środa" },
                    new SelectListItem { Value = "5", Text = "Czwartek" },
                    new SelectListItem { Value = "6", Text = "Piątek" },
                    new SelectListItem { Value = "7", Text = "Sobota" },
                    new SelectListItem { Value = "1", Text = "Niedziela" }
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_PeriodicWorkShiftForm", periodicWorkShift);
            }

            var startDateDateTime = periodicWorkShift.StartDate.AsDateTime();
            var endDateDateTime = periodicWorkShift.EndDate.AsDateTime();

            var differenceDays = (int)startDateDateTime.DayOfWeek - (periodicWorkShift.DayOfWeek - 1);
            startDateDateTime = startDateDateTime.AddDays(differenceDays > 0 ? 7 - differenceDays : Math.Abs(differenceDays));

            var startTimeDateTime = periodicWorkShift.StartTime.AsDateTime();
            var endTimeDateTime = periodicWorkShift.EndTime.AsDateTime();

            var startTimeWorkShift = new TimeSpan(startTimeDateTime.Hour, startTimeDateTime.Minute, 00);
            var endTimeWorkShift = new TimeSpan(startTimeDateTime < endTimeDateTime ? 0 : 1, endTimeDateTime.Hour, endTimeDateTime.Minute, 00);

            for (; startDateDateTime.Date <= endDateDateTime.Date; startDateDateTime = startDateDateTime.AddDays(7))
            {
                var workShift = new WorkShift
                {
                    StartDate = startDateDateTime.Date + startTimeWorkShift,
                    EndDate = startDateDateTime.Date + endTimeWorkShift,
                    UserID = periodicWorkShift.WorkerID
                };

                var workShiftExists = workScheduleService.WorkShiftExists(workShift);
                if (workShiftExists) continue;

                workScheduleService.CreateWorkShift(workShift);
                workScheduleService.SaveWorkShift();
            }

            return PeriodicWorkShiftForm();
        }

        [HttpPost]
        public ActionResult AddPeriodicEvent(PeriodicEventFormViewModel periodicEvent)
        {
            if (!ModelState.IsValid)
            {
                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                periodicEvent.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                periodicEvent.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                periodicEvent.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);
                periodicEvent.DaysOfWeek = new List<SelectListItem>
                {
                    new SelectListItem { Value = "2", Text = "Poniedziałek"},
                    new SelectListItem { Value = "3", Text = "Wtorek" },
                    new SelectListItem { Value = "4", Text = "Środa" },
                    new SelectListItem { Value = "5", Text = "Czwartek" },
                    new SelectListItem { Value = "6", Text = "Piątek" },
                    new SelectListItem { Value = "7", Text = "Sobota" },
                    new SelectListItem { Value = "1", Text = "Niedziela" }
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_PeriodicEventForm", periodicEvent);
            }

            if (periodicEvent.StartDate.AsDateTime() >= periodicEvent.EndDate.AsDateTime())
            {
                ModelState.AddModelError("StartDate", "Data rozpoczęcia nie może być równa lub późniejsza od daty zakończenia");
                ModelState.AddModelError("EndDate", "Data zakończenia nie może być równa lub wcześniejsza od daty rozpoczęcia");

                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                periodicEvent.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                periodicEvent.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                periodicEvent.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);
                periodicEvent.DaysOfWeek = new List<SelectListItem>
                {
                    new SelectListItem { Value = "2", Text = "Poniedziałek"},
                    new SelectListItem { Value = "3", Text = "Wtorek" },
                    new SelectListItem { Value = "4", Text = "Środa" },
                    new SelectListItem { Value = "5", Text = "Czwartek" },
                    new SelectListItem { Value = "6", Text = "Piątek" },
                    new SelectListItem { Value = "7", Text = "Sobota" },
                    new SelectListItem { Value = "1", Text = "Niedziela" }
                };

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_PeriodicEventForm", periodicEvent);
            }

            var startDateDateTime = periodicEvent.StartDate.AsDateTime();
            var endDateDateTime = periodicEvent.EndDate.AsDateTime();

            var differenceDays = (int)startDateDateTime.DayOfWeek - (periodicEvent.DayOfWeek - 1);
            startDateDateTime = startDateDateTime.AddDays(differenceDays > 0 ? 7 - differenceDays : Math.Abs(differenceDays));

            var startTimeDateTime = periodicEvent.StartTime.AsDateTime();
            var endTimeDateTime = periodicEvent.EndTime.AsDateTime();

            var startTimeWorkShift = new TimeSpan(startTimeDateTime.Hour, startTimeDateTime.Minute, 00);
            var endTimeWorkShift = new TimeSpan(startTimeDateTime < endTimeDateTime ? 0 : 1, endTimeDateTime.Hour, endTimeDateTime.Minute, 00);

            for (; startDateDateTime.Date <= endDateDateTime.Date; startDateDateTime = startDateDateTime.AddDays(7))
            {
                var @event = new Event
                {
                    StartDate = startDateDateTime.Date + startTimeWorkShift,
                    EndDate = startDateDateTime.Date + endTimeWorkShift,
                    LimitOfPlaces = periodicEvent.LimitOfPlaces,
                    Name = periodicEvent.Name,
                    RoomID = periodicEvent.RoomID
                };

                var userWorkShift = workScheduleService.GetWorkShift(periodicEvent.WorkerID, @event.StartDate, @event.EndDate);
                if (userWorkShift == null) continue;

                @event.WorkShiftID = userWorkShift.WorkShiftID;

                var workShiftExists = workScheduleService.EventExists(@event);
                if (workShiftExists) continue;

                workScheduleService.CreateEvent(@event);
                workScheduleService.SaveEvent();
            }

            return PeriodicEventForm();
        }

        [HttpGet]
        public ActionResult RemoveEvent(int eventID)
        {
            var @event = workScheduleService.GetEvent(eventID);

            if (@event == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("_ConfirmationRemoveEvent", @event);
        }

        [HttpGet]
        public ActionResult RemoveWorkShift(int workShiftID)
        {
            var workShift = workScheduleService.GetWorkShift(workShiftID);

            if (workShift == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("_ConfirmationRemoveWorkShift", workShift);
        }

        [HttpGet]
        public ActionResult EditEvent(int eventID)
        {
            var @event = workScheduleService.GetEvent(eventID);

            if (@event == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var editEventViewModel = Mapper.Map<Event, EditEventFormViewModel>(@event);

            var workers = workScheduleService.GetWorkers();
            var rooms = workScheduleService.GetRooms();
            var trainings = resourceService.GetTrainings();
            editEventViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
            editEventViewModel.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
            editEventViewModel.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

            return PartialView("_EditEventForm", editEventViewModel);
        }

        [HttpPost]
        public ActionResult EditEvent(EditEventFormViewModel eventFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                eventFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                eventFormViewModel.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                eventFormViewModel.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditEventForm", eventFormViewModel);
            }

            var @event = workScheduleService.GetEvent(eventFormViewModel.EventID);
            if (@event == null)
            {
                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                eventFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                eventFormViewModel.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                eventFormViewModel.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditEventForm", eventFormViewModel);
            }

            @event = Mapper.Map(eventFormViewModel, @event);

            if (@event.StartDate >= @event.EndDate)
            {
                ModelState.AddModelError("StartDate", "Data rozpoczęcia nie może być równa lub późniejsza od daty zakończenia");
                ModelState.AddModelError("EndDate", "Data zakończenia nie może być równa lub wcześniejsza od daty rozpoczęcia");

                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                eventFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                eventFormViewModel.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                eventFormViewModel.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditEventForm", eventFormViewModel);
            }

            var userWorkShift = workScheduleService.GetWorkShift(eventFormViewModel.WorkerID, @event.StartDate, @event.EndDate);
            if (userWorkShift == null)
            {
                ModelState.AddModelError("StartDate", "W wybranych datach nie ma zdefiniowanego dyżuru dla wybranego pracownika");
                ModelState.AddModelError("EndDate", "W wybranych datach nie ma zdefiniowanego dyżuru dla wybranego pracownika");

                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                eventFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                eventFormViewModel.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                eventFormViewModel.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditEventForm", eventFormViewModel);
            }

            @event.WorkShiftID = userWorkShift.WorkShiftID;

            var workShiftExists = workScheduleService.EventExists(@event);
            if (workShiftExists)
            {
                ModelState.AddModelError("StartDate", "W wybranych datach zostały już zdefiniowane zajęcia dla wybranego pracownika lub sali");
                ModelState.AddModelError("EndDate", "W wybranych datach zostały już zdefiniowane zajęcia dla wybranego pracownika lub sali");

                var workers = workScheduleService.GetWorkers();
                var rooms = workScheduleService.GetRooms();
                var trainings = resourceService.GetTrainings();
                eventFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);
                eventFormViewModel.Rooms = Mapper.Map<IEnumerable<Room>, IEnumerable<SelectListItem>>(rooms);
                eventFormViewModel.Trainings = Mapper.Map<IEnumerable<Training>, IEnumerable<SelectListItem>>(trainings);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditEventForm", eventFormViewModel);
            }

            var usersOnEvent = @event.UsersOnEvent.ToList();
            usersOnEvent.ForEach(u => { @event.UsersOnEvent.Remove(u); });

            workScheduleService.EditEvent(@event);
            workScheduleService.SaveEvent();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult ConfirmationRemoveEvent(int eventID)
        {
            var @event = workScheduleService.GetEvent(eventID);

            if (@event == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            workScheduleService.RemoveEvent(@event);
            workScheduleService.SaveEvent();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult ConfirmationRemoveWorkShift(int workShiftID)
        {
            var workShift = workScheduleService.GetWorkShift(workShiftID);

            if (workShift == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            workScheduleService.RemoveWorkShift(workShift);
            workScheduleService.SaveWorkShift();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        public ActionResult EditWorkShift(int workShiftID)
        {
            var workShift = workScheduleService.GetWorkShift(workShiftID);

            if (workShift == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var editWorkShiftViewModel = Mapper.Map<WorkShift, EditWorkShiftFormViewModel>(workShift);

            var workers = workScheduleService.GetWorkers();
            editWorkShiftViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

            return PartialView("_EditWorkShiftForm", editWorkShiftViewModel);
        }

        [HttpPost]
        public ActionResult EditWorkShift(EditWorkShiftFormViewModel editWorkShiftFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                var workers = workScheduleService.GetWorkers();
                editWorkShiftFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditWorkShiftForm", editWorkShiftFormViewModel);
            }

            var workShift = workScheduleService.GetWorkShift(editWorkShiftFormViewModel.WorkShiftID);
            if (workShift == null)
            {
                var workers = workScheduleService.GetWorkers();
                editWorkShiftFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditWorkShiftForm", editWorkShiftFormViewModel);
            }

            workShift = Mapper.Map(editWorkShiftFormViewModel, workShift);

            if (workShift.StartDate >= workShift.EndDate)
            {
                ModelState.AddModelError("StartDate", "Data rozpoczęcia nie może być równa lub późniejsza od daty zakończenia");
                ModelState.AddModelError("EndDate", "Data zakończenia nie może być równa lub wcześniejsza od daty rozpoczęcia");

                var workers = workScheduleService.GetWorkers();
                editWorkShiftFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditWorkShiftForm", editWorkShiftFormViewModel);
            }

            var workShiftExists = workScheduleService.WorkShiftExists(workShift);
            if (workShiftExists)
            {
                ModelState.AddModelError("StartDate", "W wybranych datach został już zdefiniowany dyżur dla wybranego pracownika");
                ModelState.AddModelError("EndDate", "W wybranych datach został już zdefiniowany dyżur dla wybranego pracownika");

                var workers = workScheduleService.GetWorkers();
                editWorkShiftFormViewModel.Workers = Mapper.Map<IEnumerable<User>, IEnumerable<SelectListItem>>(workers);

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();

                return PartialView("_EditWorkShiftForm", editWorkShiftFormViewModel);
            }

            workScheduleService.EditWorkShift(workShift);
            workScheduleService.SaveWorkShift();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}