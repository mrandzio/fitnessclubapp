﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Service;
using FitnessClubApp.Web.Areas.Management.ViewModels;

namespace FitnessClubApp.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Manager,Admin")]
    public class RoomController : Controller
    {
        private readonly IResourceService resourceService;

        public RoomController(IResourceService resourceService)
        {
            this.resourceService = resourceService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllRooms()
        {
            var rooms = resourceService.GetRooms().OrderBy(r => r.Name).ToList();
            return PartialView("_AllRooms", rooms);
        }

        [HttpGet]
        public ActionResult AddRoom()
        {
            return PartialView("_RoomForm");
        }

        [HttpGet]
        public ActionResult EditRoom(int roomID)
        {
            var room = resourceService.GetRoom(roomID);

            if (room == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var roomFormViewModel = Mapper.Map<Room, RoomFormViewModel>(room);

            return PartialView("_RoomForm", roomFormViewModel);
        }

        [HttpGet]
        public ActionResult RemoveRoom(int roomID)
        {
            var room = resourceService.GetRoom(roomID);
            if (room == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var eventInRoomExists = resourceService.EventInRoomExists(roomID);
            if(eventInRoomExists)
                return PartialView("_ErrorRemoveRoom", room);

            return PartialView("_ConfirmationRemoveRoom", room);
        }

        [HttpPost]
        public ActionResult ConfirmationRemoveRoom(int roomID)
        {
            var room = resourceService.GetRoom(roomID);
            if (room == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var eventInRoomExists = resourceService.EventInRoomExists(roomID);
            if (eventInRoomExists)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            resourceService.RemoveRoom(room);
            resourceService.SaveRoom();

            return GetAllRooms();
        }

        [HttpPost]
        public ActionResult AddRoom(RoomFormViewModel roomFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_RoomForm", roomFormViewModel);
            }

            var room = Mapper.Map<RoomFormViewModel, Room>(roomFormViewModel);

            var roomExists = resourceService.RoomExists(room);
            if (roomExists)
            {
                ModelState.AddModelError("Name", "Sala o podanej nazwie już istnieje");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_RoomForm", roomFormViewModel);
            }

            resourceService.CreateRoom(room);
            resourceService.SaveRoom();

            return GetAllRooms();
        }

        [HttpPost]
        public ActionResult EditRoom(RoomFormViewModel roomFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_RoomForm", roomFormViewModel);
            }

            var room = resourceService.GetRoom(roomFormViewModel.RoomID);
            if (room == null)
            {
                ModelState.AddModelError("Name", "Niepoprawny identyfikator sali");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_RoomForm", roomFormViewModel);
            }

            room = Mapper.Map(roomFormViewModel, room);

            var roomExists = resourceService.RoomExists(room);
            if (roomExists)
            {
                ModelState.AddModelError("Name", "Sala o podanej nazwie już istnieje");

                Response.StatusCode = HttpStatusCode.BadRequest.GetHashCode();
                return PartialView("_RoomForm", roomFormViewModel);
            }

            resourceService.EditRoom(room);
            resourceService.SaveRoom();

            return GetAllRooms();
        }
    }
}