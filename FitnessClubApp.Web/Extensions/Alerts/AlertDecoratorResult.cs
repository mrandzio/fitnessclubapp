﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitnessClubApp.Web.Extensions.Alerts
{
    public class AlertDecoratorResult : ActionResult
    {
        public ActionResult Result { get; }
        public string Type { get; }
        public string Title { get; }
        public string Body { get; }

        public AlertDecoratorResult(ActionResult result, string type, string title, string body)
        {
            Result = result;
            Type = type;
            Title = title;
            Body = body;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var tempData = context.Controller.TempData;

            if (tempData != null)
            {
                tempData["_alert.type"] = Type;
                tempData["_alert.title"] = Title;
                tempData["_alert.body"] = Body;
            }

            Result.ExecuteResult(context);
        }
    }
}