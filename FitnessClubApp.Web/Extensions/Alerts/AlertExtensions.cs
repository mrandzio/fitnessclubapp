﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitnessClubApp.Web.Extensions.Alerts
{
    public static class AlertExtensions
    {
        public static ActionResult WithSuccess(this ActionResult result, string title, string body)
        {
            return Alert(result, "success", title, body);
        }

        public static ActionResult WithInfo(this ActionResult result, string title, string body)
        {
            return Alert(result, "info", title, body);
        }

        public static ActionResult WithWarning(this ActionResult result, string title, string body)
        {
            return Alert(result, "warning", title, body);
        }

        public static ActionResult WithDanger(this ActionResult result, string title, string body)
        {
            return Alert(result, "danger", title, body);
        }

        private static ActionResult Alert(ActionResult result, string type, string title, string body)
        {
            return new AlertDecoratorResult(result, type, title, body);
        }
    }
}