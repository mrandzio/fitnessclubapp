﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Extensions.Validators
{
    public class UserAgeValidation : ValidationAttribute
    {
        public int Age { get; set; }

        public override bool IsValid(object value)
        {
            var parse = DateTime.TryParseExact(value as string, "dd'.'MM'.'yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out var date);

            if (!parse)
            {
                ErrorMessage = "Podaj prawidłową datę urodzenia";
                return false;
            }

            ErrorMessage = $"Musisz mieć {Age} lat, aby być członkiem klubu";
            return date <= DateTime.Now.Date.AddYears(-Age) && date > DateTime.ParseExact("01.01.1900", "dd'.'MM'.'yyyy",
                       CultureInfo.InvariantCulture, DateTimeStyles.None);
        }
    }
}