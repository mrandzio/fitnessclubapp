﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Extensions.Validators
{
    public class TimeValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var parse = DateTime.TryParseExact(value as string, "HH':'mm",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out _);

            return parse;
        }
    }
}