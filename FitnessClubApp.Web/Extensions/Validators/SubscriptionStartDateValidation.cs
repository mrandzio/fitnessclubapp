﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Extensions.Validators
{
    public class SubscriptionStartDateValidation : ValidationAttribute
    {
        public int MaximumLateness { get; set; }

        public override bool IsValid(object value)
        {
            var parse = DateTime.TryParseExact(value as string, "dd'.'MM'.'yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out var date);

            if (!parse)
            {
                ErrorMessage = "Podaj prawidłową datę rozpoczęcia";
                return false;
            }

            ErrorMessage = $"Dzień rozpoczęcia subskrypcji możesz opóźnić maksymalnie o {MaximumLateness} dni od dnia dzisiejszego";
            return date >= DateTime.Now.Date && date <= DateTime.Now.Date.AddDays(30);
        }
    }
}