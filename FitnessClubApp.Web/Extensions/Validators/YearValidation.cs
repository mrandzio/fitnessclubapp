﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.Extensions.Validators
{
    public class YearValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var parse = DateTime.TryParseExact(value as string, "yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out var date);

            if (!parse)
                return false;

            return date > DateTime.ParseExact("01.01.2000", "dd'.'MM'.'yyyy",
                       CultureInfo.InvariantCulture, DateTimeStyles.None);
        }
    }
}