﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AutoMapper;

namespace FitnessClubApp.Web.Extensions.Converters
{
    public class HttpPostedFileBaseTypeConverter : ITypeConverter<HttpPostedFileBase, byte[]>
    {
        public byte[] Convert(HttpPostedFileBase source, byte[] destination, ResolutionContext context)
        {
            MemoryStream target = new MemoryStream();
            source.InputStream.Seek(0, SeekOrigin.Begin);
            source.InputStream.CopyTo(target);
            return target.ToArray();
        }
    }
}