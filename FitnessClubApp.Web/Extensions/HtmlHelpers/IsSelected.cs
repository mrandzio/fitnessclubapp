﻿using System.Linq;
using static System.String;

namespace FitnessClubApp.Web.Extensions.HtmlHelpers
{
    public static class HtmlHelperExtensions
    {
        public static string IsSelected(this System.Web.Mvc.HtmlHelper html, string actions, string controllers, string area = "", string cssClass = "active")
        {
            var viewContext = html.ViewContext;
            var isChildAction = viewContext.Controller.ControllerContext.IsChildAction;

            if (isChildAction)
                viewContext = html.ViewContext.ParentActionViewContext;

            var routeValues = viewContext.RouteData.Values;
            var currentArea = viewContext.RouteData.DataTokens["area"]?.ToString().ToLower();
            var currentAction = routeValues["action"].ToString().ToLower();
            var currentController = routeValues["controller"].ToString().ToLower();

            var acceptedArea = area.Trim().ToLower();
            var acceptedActions = actions.Trim().ToLower().Split(',').Distinct().ToArray();
            var acceptedControllers = controllers.Trim().ToLower().Split(',').Distinct().ToArray();

            return acceptedActions.Contains(currentAction) && acceptedControllers.Contains(currentController) || acceptedArea.Equals(currentArea) ?
                cssClass : Empty;
        }
    }
}