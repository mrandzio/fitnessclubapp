﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.ViewModels
{
    public class ChangePasswordFormViewModel
    {
        public int UserID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj obecne hasło")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj nowe hasło")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Nowe hasło musi zawierać co najmniej {2} znaków", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Potwierdź nowe hasło")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Nowe hasło i jego potwierdzenie są niezgodne")]
        public string ConfirmNewPassword { get; set; }
    }
}