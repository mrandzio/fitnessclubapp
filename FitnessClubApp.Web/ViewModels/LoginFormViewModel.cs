﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.ViewModels
{
    public class LoginFormViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj adres email")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj hasło")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}