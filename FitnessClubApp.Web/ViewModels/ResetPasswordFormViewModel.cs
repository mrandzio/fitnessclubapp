﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.ViewModels
{
    public class ResetPasswordFormViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj hasło")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Hasło musi zawierać co najmniej {2} znaków", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Potwierdź hasło")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Hasło i jego potwierdzenie są niezgodne")]
        public string ConfirmNewPassword { get; set; }

        public Guid ResetPasswordCode { get; set; }
    }
}