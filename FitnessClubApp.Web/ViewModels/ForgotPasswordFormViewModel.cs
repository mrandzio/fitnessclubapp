﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessClubApp.Web.ViewModels
{
    public class ForgotPasswordFormViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj adres email")]
        public string Email { get; set; }
    }
}