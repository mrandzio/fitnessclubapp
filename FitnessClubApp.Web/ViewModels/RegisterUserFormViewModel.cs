﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessClubApp.Web.Extensions.Validators;

namespace FitnessClubApp.Web.ViewModels
{
    public class RegisterUserFormViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj imię")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj nazwisko")]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj adres e-mail")]
        [EmailAddress(ErrorMessage = "Podaj prawidłowy adres e-mail")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj numer telefonu")]
        [RegularExpression(@"^\d{9}$", ErrorMessage = "Podaj prawidłowy numer telefonu")]
        public string PhoneNumber { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj datę urodzenia")]
        [UserAgeValidation(Age = 12)]
        public string DateOfBirth { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj hasło")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Hasło musi zawierać co najmniej {2} znaków", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Potwierdź hasło")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Hasło i jego potwierdzenie są niezgodne")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Wybierz płeć")]
        public bool? Male { get; set; }

        [CheckboxIsChecked(ErrorMessage = "Musisz zaakceptować regulamin klubu")]
        public bool IsAcceptedRules { get; set; }

    }
}