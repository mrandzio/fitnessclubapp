﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Helpers;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using FitnessClubApp.Web.Areas.Membership.ViewModels;
using FitnessClubApp.Web.Extensions.Converters;
using FitnessClubApp.Web.ViewModels;

namespace FitnessClubApp.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainMappings";

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<RegisterUserFormViewModel, User>()
                .ForMember(u => u.IsEmailVerified, map => map.MapFrom(vm => false))
                .ForMember(u => u.UniqueCode, map => map.MapFrom(vm => Guid.NewGuid()))
                .ForMember(u => u.Password, map => map.MapFrom(vm => Crypto.HashPassword(vm.Password)))
                .ForMember(u => u.DateCreated, map => map.MapFrom(vm => DateTime.Now));

            CreateMap<SubscriptionFormViewModel, Subscription>();

            CreateMap<SubscriptionOrderFormViewModel, PurchasedSubscription>()
                .ForMember(u => u.OrderDate, map => map.MapFrom(vm => DateTime.Now))
                .ForMember(u => u.SubscriptionName, map => map.MapFrom(vm => vm.Subscription.Name))
                .ForMember(u => u.Price, map => map.MapFrom(vm => vm.Subscription.Price))
                .ForMember(u => u.Paid, map => map.MapFrom(vm => false))
                .ForMember(u => u.Complete, map => map.MapFrom(vm => false))
                .ForMember(u => u.UserID, map => map.MapFrom(vm => vm.User.UserID))
                .ForMember(u => u.EndDate, map => map.MapFrom(vm => DateTime.ParseExact(vm.StartDate, "dd'.'MM'.'yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None).AddDays(vm.Subscription.Days)));

            CreateMap<EditUserRoleFormViewModel, User>();

            CreateMap<EditTransactionStatusFormViewModel, PurchasedSubscription>();

            CreateMap<DisposableWorkShiftFormViewModel, WorkShift>()
                .ForMember(ws => ws.UserID, map => map.MapFrom(vm => vm.WorkerID));

            CreateMap<RoomFormViewModel, Room>();

            CreateMap<EditTrainingFormViewModel, Training>()
                .ForMember(model => model.Image, map => map.Ignore());

            CreateMap<HttpPostedFileBase, byte[]>()
                .ConvertUsing<HttpPostedFileBaseTypeConverter>();

            CreateMap<AddTrainingFormViewModel, Training>();

            CreateMap<EditImageTrainingFormViewModel, Training>();

            CreateMap<EditDescriptionWorkerFormViewModel, DescriptionOfUser>()
                .ForMember(model => model.Image, map => map.Ignore());

            CreateMap<EditImageWorkerFormViewModel, DescriptionOfUser>();

            CreateMap<DisposableEventFormViewModel, Event>()
                .ForMember(model => model.WorkShiftID, map => map.Ignore());

            CreateMap<EditEventFormViewModel, Event>()
                .ForMember(model => model.WorkShiftID, map => map.Ignore());

            CreateMap<EditWorkShiftFormViewModel, WorkShift>()
                .ForMember(model => model.UserID, map => map.MapFrom(vm => vm.WorkerID));

            CreateMap<EditEntryViewModel, Entry>()
                .ForMember(model => model.UserID, map => map.Ignore());
        }
    }
}