﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FitnessClubApp.Model;
using FitnessClubApp.Web.Areas.Management.ViewModels;
using FitnessClubApp.Web.Areas.Membership.ViewModels;
using FitnessClubApp.Web.ViewModels;

namespace FitnessClubApp.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName => "DomainToViewModelMappings";

        public DomainToViewModelMappingProfile()
        {
            CreateMap<User, ResetPasswordFormViewModel>()
                .ForMember(vm => vm.ResetPasswordCode, map => map.MapFrom(u => u.UniqueCode));

            CreateMap<Subscription, SubscriptionFormViewModel>();

            CreateMap<UserRole, DisplayUserRolesViewModel>()
                .ForMember(vm => vm.Name, map => map.MapFrom(ps => ps.PolishName));

            CreateMap<User, DisplayAccountViewModel>()
                .ForMember(vm => vm.UserRoleName, map => map.MapFrom(u => u.UserRole.PolishName));

            CreateMap<PurchasedSubscription, SubscriptionOrderFormViewModel>()
                .ForMember(vm => vm.Shipment, map => map.Ignore())
                .ForMember(vm => vm.SubscriptionID, map => map.Ignore())
                .ForMember(vm => vm.StartDate, map => map.Ignore())
                .ForMember(vm => vm.User, map => map.Ignore())
                .ForMember(vm => vm.Subscription, map => map.Ignore())
                .ForMember(vm => vm.UserID, map => map.Ignore());

            CreateMap<PurchasedSubscription, DisplayTransactionViewModel>()
                .ForMember(vm => vm.PurcharserFirstName, map => map.MapFrom(ps => ps.FirstName))
                .ForMember(vm => vm.PurcharserLastName, map => map.MapFrom(ps => ps.LastName))
                .ForMember(vm => vm.PurcharserPersonalData, map => map.MapFrom(ps => $"{ ps.FirstName } { ps.LastName }"))
                .ForMember(vm => vm.SubscriberFirstName, map => map.MapFrom(ps => ps.User.FirstName))
                .ForMember(vm => vm.SubscriberLastName, map => map.MapFrom(ps => ps.User.LastName))
                .ForMember(vm => vm.SubscriberPersonalData, map => map.MapFrom(ps => $"{ ps.User.FirstName } { ps.User.LastName }"))
                .ForMember(vm => vm.Email, map => map.MapFrom(ps => ps.User.Email))
                .ForMember(vm => vm.Price, map => map.MapFrom(ps => $"{ps.Price:0.# zł}"))
                .ForMember(vm => vm.PhoneNumber, map => map.MapFrom(ps => ps.User.PhoneNumber));

            CreateMap<User, DisplayWorkScheduleResourceViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(u => $"U_{u.UserID}"))
                .ForMember(vm => vm.title, map => map.MapFrom(u => $"{u.FirstName} {u.LastName}"))
                .ForMember(vm => vm.color, map => map.MapFrom(u => u.UserRole.Name.Equals("Trainer") ? "#33CC33" : "orange"))
                .ForMember(vm => vm.role, map => map.MapFrom(u => u.UserRole.PolishName));

            CreateMap<Room, DisplayWorkScheduleResourceViewModel.Room>()
                .ForMember(vm => vm.id, map => map.MapFrom(r => $"R_{r.RoomID}"))
                .ForMember(vm => vm.title, map => map.MapFrom(r => $"Sala {r.Name}"))
                .ForMember(vm => vm.color, map => map.MapFrom(u => "#87CEFA"));

            CreateMap<WorkShift, DisplayWorkScheduleEventViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(ws => $"W_{ws.WorkShiftID}"))
                .ForMember(vm => vm.resourceId, map => map.MapFrom(ws => $"U_{ws.UserID}"))
                .ForMember(vm => vm.start, map => map.MapFrom(ws => ws.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(ws => ws.EndDate))
                .ForMember(vm => vm.title, map => map.MapFrom(ws => $"{ws.User.FirstName} {ws.User.LastName} | Dyżur"))
                .ForMember(vm => vm.description, 
                    map => map.MapFrom(ws => $"{ws.User.FirstName} {ws.User.LastName} | {ws.StartDate:HH:mm} - {ws.EndDate:HH:mm}"));

            CreateMap<Event, DisplayWorkScheduleEventViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(e => $"E_{e.EventID}"))
                .ForMember(vm => vm.resourceId, map => map.MapFrom(e => $"R_{e.RoomID}_U_{e.WorkShift.UserID}"))
                .ForMember(vm => vm.start, map => map.MapFrom(e => e.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(e => e.EndDate))
                .ForMember(vm => vm.title, map => map.MapFrom(e => $"{e.Name} | {e.WorkShift.User.FirstName} {e.WorkShift.User.LastName} | {e.UsersOnEvent.Count} / {e.LimitOfPlaces}"))
                .ForMember(vm => vm.description, 
                    map => map.MapFrom(e => $"Zajęcia | {e.StartDate:HH:mm} - {e.EndDate:HH:mm} | Sala {e.Room.Name}"));

            CreateMap<Event, DisplaySingleDataForWorkerViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(e => e.EventID))
                .ForMember(vm => vm.start, map => map.MapFrom(e => e.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(e => e.EndDate))
                .ForMember(vm => vm.title, map => map.MapFrom(e => $"{e.Name} | Sala {e.Room.Name}"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(e => $"{e.StartDate:HH:mm} - {e.EndDate:HH:mm} | {e.UsersOnEvent.Count} / {e.LimitOfPlaces}"));

            CreateMap<WorkShift, DisplaySingleDataForWorkerViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(ws => ws.WorkShiftID))
                .ForMember(vm => vm.start, map => map.MapFrom(ws => ws.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(ws => ws.EndDate))
                .ForMember(vm => vm.title, map => map.MapFrom(ws => "Dyżur"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(ws => $"{ws.StartDate:HH:mm} - {ws.EndDate:HH:mm}"));

            CreateMap<Event, DisplayMultipleDataForWorkerViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(e => e.EventID))
                .ForMember(vm => vm.start, map => map.MapFrom(e => e.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(e => e.EndDate))
                .ForMember(vm => vm.title, map => map.MapFrom(e => $"{e.Name} | Sala {e.Room.Name}"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(e => $"{e.StartDate:HH:mm} - {e.EndDate:HH:mm} | {e.UsersOnEvent.Count} / {e.LimitOfPlaces}"))
                .ForMember(vm => vm.color, map => map.MapFrom(e => "#87CEFA"));

            CreateMap<WorkShift, DisplayMultipleDataForWorkerViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(ws => ws.WorkShiftID))
                .ForMember(vm => vm.start, map => map.MapFrom(ws => ws.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(ws => ws.EndDate))
                .ForMember(vm => vm.title, map => map.MapFrom(ws => "Dyżur"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(ws => $"{ws.StartDate:HH:mm} - {ws.EndDate:HH:mm}"))
                .ForMember(vm => vm.color, map => map.MapFrom(ws => "orange"));

            CreateMap<User, SelectListItem>()
                .ForMember(vm => vm.Text, map => map.MapFrom(u => $"{u.FirstName} {u.LastName} ({u.UserRole.PolishName[0]})"))
                .ForMember(vm => vm.Value, map => map.MapFrom(u => u.UserID));

            CreateMap<Training, SelectListItem>()
                .ForMember(vm => vm.Text, map => map.MapFrom(t => t.Name))
                .ForMember(vm => vm.Value, map => map.MapFrom(t => t.Name));

            CreateMap<Room, SelectListItem>()
                .ForMember(vm => vm.Text, map => map.MapFrom(r => r.Name))
                .ForMember(vm => vm.Value, map => map.MapFrom(r => r.RoomID));

            CreateMap<Room, RoomFormViewModel>();

            CreateMap<Training, EditImageTrainingFormViewModel>()
                .ForMember(model => model.Image, map => map.Ignore());

            CreateMap<DescriptionOfUser, EditImageWorkerFormViewModel>()
                .ForMember(model => model.Image, map => map.Ignore());

            CreateMap<Event, EditEventFormViewModel>()
                .ForMember(model => model.WorkerID, map => map.MapFrom(e => e.WorkShift.UserID));

            CreateMap<WorkShift, EditWorkShiftFormViewModel>()
                .ForMember(model => model.WorkerID, map => map.MapFrom(e => e.UserID));

            CreateMap<Entry, DisplayUnfinishedEntryViewModel>()
                .ForMember(model => model.FirstName, map => map.MapFrom(e => e.User.FirstName))
                .ForMember(model => model.LastName, map => map.MapFrom(e => e.User.LastName))
                .ForMember(model => model.Email, map => map.MapFrom(e => e.User.Email))
                .ForMember(model => model.UserID, map => map.MapFrom(e => e.User.UserID));

            CreateMap<Entry, DisplayCompletedEntryViewModel>()
                .ForMember(model => model.FirstName, map => map.MapFrom(e => e.User.FirstName))
                .ForMember(model => model.LastName, map => map.MapFrom(e => e.User.LastName))
                .ForMember(model => model.Email, map => map.MapFrom(e => e.User.Email))
                .ForMember(model => model.UserID, map => map.MapFrom(e => e.User.UserID));

            CreateMap<Entry, DisplayEntryViewModel>()
                .ForMember(model => model.StartTime, map => map.MapFrom(e => e.StartDate));

            CreateMap<Event, DisplaySeatReservationScheduleEventViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(e => e.EventID))
                .ForMember(vm => vm.start, map => map.MapFrom(e => e.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(e => e.EndDate))
                .ForMember(vm => vm.title,
                    map => map.MapFrom(e => $"Zajęcia | {e.Name} | {e.UsersOnEvent.Count} / {e.LimitOfPlaces}"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(e =>
                        $"{e.WorkShift.User.FirstName} {e.WorkShift.User.LastName} | {e.StartDate:HH:mm} - {e.EndDate:HH:mm} | Sala {e.Room.Name}"))
                .ForMember(vm => vm.color,
                    map => map.MapFrom(e => e.StartDate.AddHours(-1) < DateTime.Now ? "red" : e.UsersOnEvent.Count >= e.LimitOfPlaces ? "orange" : "green"));

            CreateMap<WorkShift, DisplayWorkShiftsTrainersScheduleEventViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(e => e.WorkShiftID))
                .ForMember(vm => vm.start, map => map.MapFrom(e => e.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(e => e.EndDate))
                .ForMember(vm => vm.color, map => map.MapFrom(e => "orange"))
                .ForMember(vm => vm.title, map => map.MapFrom(ws => $"{ws.User.FirstName} {ws.User.LastName} | Dyżur"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(ws => $"{ws.StartDate:HH:mm} - {ws.EndDate:HH:mm}"));

            CreateMap<User, DisplayAllResourceViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(u => $"U_{u.UserID}"))
                .ForMember(vm => vm.title, map => map.MapFrom(u => $"{u.FirstName} {u.LastName}"))
                .ForMember(vm => vm.role, map => map.MapFrom(u => u.UserRole.PolishName));

            CreateMap<Room, DisplayAllResourceViewModel.Room>()
                .ForMember(vm => vm.id, map => map.MapFrom(r => $"R_{r.RoomID}"))
                .ForMember(vm => vm.title, map => map.MapFrom(r => $"Sala {r.Name}"));

            CreateMap<WorkShift, DisplayAllEventViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(ws => $"W_{ws.WorkShiftID}"))
                .ForMember(vm => vm.resourceId, map => map.MapFrom(ws => $"U_{ws.UserID}"))
                .ForMember(vm => vm.start, map => map.MapFrom(ws => ws.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(ws => ws.EndDate))
                .ForMember(vm => vm.userId, map => map.MapFrom(e => e.UserID))
                .ForMember(vm => vm.color, map => map.MapFrom(e => "orange"))
                .ForMember(vm => vm.title, map => map.MapFrom(ws => $"{ws.User.FirstName} {ws.User.LastName} | Dyżur"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(ws => $"{ws.StartDate:HH:mm} - {ws.EndDate:HH:mm}"));

            CreateMap<Event, DisplayAllEventViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(e => $"E_{e.EventID}"))
                .ForMember(vm => vm.resourceId, map => map.MapFrom(e => $"R_{e.RoomID}_U_{e.WorkShift.UserID}"))
                .ForMember(vm => vm.start, map => map.MapFrom(e => e.StartDate))
                .ForMember(vm => vm.end, map => map.MapFrom(e => e.EndDate))
                .ForMember(vm => vm.userId, map => map.MapFrom(e => e.WorkShift.UserID))
                .ForMember(vm => vm.color, map => map.MapFrom(e => "#33CC33"))
                .ForMember(vm => vm.title, map => map.MapFrom(e => $"{e.WorkShift.User.FirstName} {e.WorkShift.User.LastName} | Zajęcia"))
                .ForMember(vm => vm.description,
                    map => map.MapFrom(e => $"{e.StartDate:HH:mm} - {e.EndDate:HH:mm} | {e.Name} | Sala {e.Room.Name} | {e.UsersOnEvent.Count} / {e.LimitOfPlaces}" ));
        }
    }
}