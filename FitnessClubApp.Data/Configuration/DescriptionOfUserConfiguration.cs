﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class DescriptionOfUserConfiguration : EntityTypeConfiguration<DescriptionOfUser>
    {
        public DescriptionOfUserConfiguration()
        {
            ToTable("DescriptionsOfUsers");
            Property(u => u.Description).IsOptional();
            Property(u => u.Image).IsOptional();

            HasRequired(u => u.User)
                .WithRequiredDependent(u => u.DescriptionOfUser);
        }
    }
}
