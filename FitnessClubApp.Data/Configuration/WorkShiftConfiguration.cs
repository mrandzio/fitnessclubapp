﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class WorkShiftConfiguration : EntityTypeConfiguration<WorkShift>
    {
        public WorkShiftConfiguration()
        {
            ToTable("WorkShifts");
            Property(u => u.StartDate).IsRequired();
            Property(u => u.EndDate).IsRequired();
            Property(u => u.UserID).IsRequired();
        }
    }
}
