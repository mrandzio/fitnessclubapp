﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class TrainingConfiguration : EntityTypeConfiguration<Training>
    {
        public TrainingConfiguration()
        {
            ToTable("Trainings");
            Property(u => u.Name).IsRequired().HasMaxLength(50);
            Property(u => u.Description).IsRequired();
            Property(u => u.Image).IsRequired();
        }
    }
}
