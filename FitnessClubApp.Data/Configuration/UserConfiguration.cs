﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("Users");
            Property(u => u.FirstName).IsRequired().HasMaxLength(50);
            Property(u => u.LastName).IsRequired().HasMaxLength(50);
            Property(u => u.Email).IsRequired().HasMaxLength(254);
            Property(u => u.PhoneNumber).IsRequired().HasMaxLength(20);
            Property(u => u.DateOfBirth).IsRequired();
            Property(u => u.Password).IsRequired().IsMaxLength();
            Property(u => u.IsEmailVerified).IsRequired();
            Property(u => u.UniqueCode);
            Property(u => u.DateCreated).IsRequired();
            Property(u => u.UserRoleID).IsRequired();
            Property(u => u.Male).IsRequired();
        }
    }
}
