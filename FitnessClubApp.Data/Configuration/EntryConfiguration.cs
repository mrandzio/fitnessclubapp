﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class EntryConfiguration : EntityTypeConfiguration<Entry>
    {
        public EntryConfiguration()
        {
            ToTable("Entries");
            Property(u => u.StartDate).IsRequired();
            Property(u => u.EndDate).IsOptional();
            Property(u => u.UserID).IsRequired();
        }
    }
}
