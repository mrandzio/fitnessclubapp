﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class SubscriptionConfiguration : EntityTypeConfiguration<Subscription>
    {
        public SubscriptionConfiguration()
        {
            ToTable("Subscriptions");
            Property(u => u.Name).IsRequired().HasMaxLength(50);
            Property(u => u.Price).IsRequired();
            Property(u => u.PercentPromotion).IsRequired();
            Property(u => u.Days).IsRequired();
        }
    }
}
