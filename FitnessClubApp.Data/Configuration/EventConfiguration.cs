﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class EventConfiguration : EntityTypeConfiguration<Event>
    {
        public EventConfiguration()
        {
            ToTable("Events");
            Property(u => u.Name).IsRequired().HasMaxLength(50);
            Property(u => u.StartDate).IsRequired();
            Property(u => u.EndDate).IsRequired();
            Property(u => u.LimitOfPlaces).IsRequired();
            Property(u => u.RoomID).IsRequired();
            Property(u => u.WorkShiftID).IsRequired();

            HasMany(s => s.UsersOnEvent)
                .WithMany(c => c.UserOnEvents)
                .Map(cs =>
                {
                    cs.MapLeftKey("EventID");
                    cs.MapRightKey("UserID");
                    cs.ToTable("UsersInEvents");
                });
        }
    }
}
