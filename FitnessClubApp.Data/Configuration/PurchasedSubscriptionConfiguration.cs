﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class PurchasedSubscriptionConfiguration : EntityTypeConfiguration<PurchasedSubscription>
    {
        public PurchasedSubscriptionConfiguration()
        {
            ToTable("PurchasedSubscriptions");
            Property(u => u.OrderDate).IsRequired();
            Property(u => u.SubscriptionName).IsRequired().HasMaxLength(50);
            Property(u => u.Price).IsRequired();
            Property(u => u.StartDate).IsRequired();
            Property(u => u.EndDate).IsRequired();
            Property(u => u.FirstName).IsRequired().HasMaxLength(50);
            Property(u => u.LastName).IsRequired().HasMaxLength(50);
            Property(u => u.StreetAddress).IsRequired();
            Property(u => u.ZipCode).IsRequired().HasMaxLength(10);
            Property(u => u.City).IsRequired().HasMaxLength(30);
            Property(u => u.Paid).IsRequired();
            Property(u => u.Complete).IsRequired();
            Property(u => u.Shipment).IsRequired();
            Property(u => u.UserID).IsRequired();
        }
    }
}
