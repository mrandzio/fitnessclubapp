﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Configuration
{
    public class EquipmentConfiguration : EntityTypeConfiguration<Equipment>
    {
        public EquipmentConfiguration()
        {
            ToTable("Equipments");
            Property(u => u.Brand).IsRequired().HasMaxLength(50);
            Property(u => u.Name).IsRequired().HasMaxLength(50);
            Property(u => u.Amount).IsRequired();
        }
    }
}
