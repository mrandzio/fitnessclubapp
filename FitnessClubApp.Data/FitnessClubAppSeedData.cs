﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data
{
    public class FitnessClubAppSeedData : DropCreateDatabaseIfModelChanges<FitnessClubAppEntities>
    {
        protected override void Seed(FitnessClubAppEntities context)
        {
            GetUserRoles().ForEach(g => context.UserRoles.Add(g));

            context.Commit();
        }

        private static List<UserRole> GetUserRoles()
        {
            return new List<UserRole>
            {
                new UserRole
                {
                    Name = "User",
                    PolishName = "Użytkownik"
                },
                new UserRole
                {
                    Name = "Receptionist",
                    PolishName = "Recepcjonista"
                },         
                new UserRole
                {
                    Name = "Trainer",
                    PolishName = "Trener"
                },
                new UserRole
                {
                    Name = "Manager",
                    PolishName = "Menedżer"
                },
                new UserRole
                {
                    Name = "Admin",
                    PolishName = "Administrator"
                }
            };
        }
    }
}
