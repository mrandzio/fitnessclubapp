﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class EventRepository : RepositoryBase<Event>, IEventRepository
    {
        private readonly IDbSet<Event> dbSet;

        public EventRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            dbSet = DbContext.Set<Event>();
        }

        public IEnumerable<Event> GetAllWithEntities()
        {
            return dbSet.Include(e => e.WorkShift.User).Include(e => e.Room).Include(e => e.UsersOnEvent).ToList();
        }

        public Event GetWithEntities(Expression<Func<Event, bool>> where)
        {
            return dbSet.Where(where).Include(e => e.WorkShift.User).Include(e => e.Room).Include(e => e.UsersOnEvent).FirstOrDefault();
        }
    }

    public interface IEventRepository : IRepository<Event>
    {
        IEnumerable<Event> GetAllWithEntities();
        Event GetWithEntities(Expression<Func<Event, bool>> where);
    }
}
