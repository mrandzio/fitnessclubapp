﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class DescriptionOfUserRepository : RepositoryBase<DescriptionOfUser>, IDescriptionOfUserRepository
    {
        private readonly IDbSet<DescriptionOfUser> dbSet;

        public DescriptionOfUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            dbSet = DbContext.Set<DescriptionOfUser>();
        }

        public IEnumerable<DescriptionOfUser> GetAllWithEntities()
        {
            return dbSet.Include(e => e.User).ToList();
        }
    }

    public interface IDescriptionOfUserRepository : IRepository<DescriptionOfUser>
    {
        IEnumerable<DescriptionOfUser> GetAllWithEntities();
    }
}
