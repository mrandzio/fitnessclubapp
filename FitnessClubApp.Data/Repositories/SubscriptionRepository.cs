﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class SubscriptionRepository : RepositoryBase<Subscription>, ISubscriptionRepository
    {
        public SubscriptionRepository(IDbFactory dbFactory) : base(dbFactory) { }
    }

    public interface ISubscriptionRepository : IRepository<Subscription>
    {

    }
}
