﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class EntryRepository : RepositoryBase<Entry>, IEntryRepository
    {
        private readonly IDbSet<Entry> dbSet;

        public EntryRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            dbSet = DbContext.Set<Entry>();
        }

        public IEnumerable<Entry> GetAllWithEntities()
        {
            var entries = dbSet.Include(e => e.User.UserRole).ToList();
            return entries;
        }

        public Entry GetWithEntities(Expression<Func<Entry, bool>> where)
        {
            var entry = dbSet.Where(where).Include(e => e.User.UserRole).FirstOrDefault();
            return entry;
        }
    }

    public interface IEntryRepository : IRepository<Entry>
    {
        IEnumerable<Entry> GetAllWithEntities();
        Entry GetWithEntities(Expression<Func<Entry, bool>> where);
    }
}
