﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        private readonly IDbSet<User> dbSet;

        public UserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            dbSet = DbContext.Set<User>();
        }

        public bool IsEmailExists(string email)
        {
            return dbSet.Any(u => u.Email == email);
        }

        public User GetByActivationCode(Guid activationCode)
        {
            return dbSet.FirstOrDefault(u => u.UniqueCode == activationCode);
        }

        public User GetByEmail(string email)
        {
            return dbSet.Where(u => u.Email.Equals(email, StringComparison.CurrentCultureIgnoreCase)).Include(u => u.UserRole).FirstOrDefault();
        }

        public IEnumerable<User>GetAllWithRoleUser()
        {
            return dbSet.Include(u => u.UserRole).ToList();
        }
    }

    public interface IUserRepository : IRepository<User>
    {
        bool IsEmailExists(string email);

        User GetByActivationCode(Guid activationCode);

        User GetByEmail(string email);

        IEnumerable<User> GetAllWithRoleUser();
    }
}
