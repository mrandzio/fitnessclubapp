﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class WorkShiftRepository : RepositoryBase<WorkShift>, IWorkShiftRepository
    {
        private readonly IDbSet<WorkShift> dbSet;

        public WorkShiftRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            dbSet = DbContext.Set<WorkShift>();
        }

        public IEnumerable<WorkShift> GetAllWithEntities()
        {
            return dbSet.Include(ps => ps.User.UserRole).Include(ps => ps.Events).ToList();
        }

        public WorkShift GetWithEntities(Expression<Func<WorkShift, bool>> where)
        {
            return dbSet.Where(where).Include(e => e.User.UserRole).Include(e => e.Events).FirstOrDefault();
        }
    }

    public interface IWorkShiftRepository : IRepository<WorkShift>
    {
        IEnumerable<WorkShift> GetAllWithEntities();
        WorkShift GetWithEntities(Expression<Func<WorkShift, bool>> where);
    }
}
