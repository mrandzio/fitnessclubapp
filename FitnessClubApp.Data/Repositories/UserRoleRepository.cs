﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class UserRoleRepository : RepositoryBase<UserRole>, IUserRoleRepository
    {
        private readonly IDbSet<UserRole> dbSet;

        public UserRoleRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            dbSet = DbContext.Set<UserRole>();
        }

        public UserRole GetByName(string name)
        {
            return dbSet.FirstOrDefault(ur => ur.Name == name);
        }
    }

    public interface IUserRoleRepository : IRepository<UserRole>
    {
        UserRole GetByName(string name);
    }
}
