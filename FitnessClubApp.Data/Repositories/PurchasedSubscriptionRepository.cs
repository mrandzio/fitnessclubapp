﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data.Repositories
{
    public class PurchasedSubscriptionRepository : RepositoryBase<PurchasedSubscription>, IPurchasedSubscriptionRepository
    {
        private readonly IDbSet<PurchasedSubscription> dbSet;

        public PurchasedSubscriptionRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            dbSet = DbContext.Set<PurchasedSubscription>();
        }

        public IEnumerable<PurchasedSubscription> GetAllWithUser()
        {
            return dbSet.Include(ps => ps.User).ToList();
        }
    }

    public interface IPurchasedSubscriptionRepository : IRepository<PurchasedSubscription>
    {
        IEnumerable<PurchasedSubscription> GetAllWithUser();
    }
}
