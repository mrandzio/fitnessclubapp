﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Configuration;
using FitnessClubApp.Model;

namespace FitnessClubApp.Data
{
    public class FitnessClubAppEntities : DbContext
    {
        public FitnessClubAppEntities() : base("FitnessClubAppDatabase") { }

        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<PurchasedSubscription> PurchasedSubscriptions { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<WorkShift> WorkShifts { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<DescriptionOfUser> DescriptionsOfUsers { get; set; }
        public DbSet<Entry> Entries { get; set; }
        public DbSet<Equipment> Equipments { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());
            modelBuilder.Configurations.Add(new SubscriptionConfiguration());
            modelBuilder.Configurations.Add(new PurchasedSubscriptionConfiguration());
            modelBuilder.Configurations.Add(new RoomConfiguration());
            modelBuilder.Configurations.Add(new WorkShiftConfiguration());
            modelBuilder.Configurations.Add(new EventConfiguration());
            modelBuilder.Configurations.Add(new TrainingConfiguration());
            modelBuilder.Configurations.Add(new DescriptionOfUserConfiguration());
            modelBuilder.Configurations.Add(new EntryConfiguration());
            modelBuilder.Configurations.Add(new EquipmentConfiguration());
        }
    }
}
