namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInTwoTables : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DescriptionsOfUsers", "Description", c => c.String());
            AlterColumn("dbo.DescriptionsOfUsers", "Image", c => c.Binary());
            DropColumn("dbo.DescriptionsOfUsers", "UserID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DescriptionsOfUsers", "UserID", c => c.Int(nullable: false));
            AlterColumn("dbo.DescriptionsOfUsers", "Image", c => c.Binary(nullable: false));
            AlterColumn("dbo.DescriptionsOfUsers", "Description", c => c.String(nullable: false));
        }
    }
}
