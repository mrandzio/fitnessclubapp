namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTwoColumnsToTrainingsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trainings", "Description", c => c.String(nullable: false));
            AddColumn("dbo.Trainings", "Image", c => c.Binary(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trainings", "Image");
            DropColumn("dbo.Trainings", "Description");
        }
    }
}
