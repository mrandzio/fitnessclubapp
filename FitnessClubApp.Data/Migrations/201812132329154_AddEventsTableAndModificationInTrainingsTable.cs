namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEventsTableAndModificationInTrainingsTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Trainings", "RoomID", "dbo.Rooms");
            DropForeignKey("dbo.Trainings", "WorkShiftID", "dbo.WorkShifts");
            DropIndex("dbo.Trainings", new[] { "RoomID" });
            DropIndex("dbo.Trainings", new[] { "WorkShiftID" });
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        LimitOfPlaces = c.Int(nullable: false),
                        RoomID = c.Int(nullable: false),
                        WorkShiftID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventID)
                .ForeignKey("dbo.Rooms", t => t.RoomID, cascadeDelete: true)
                .ForeignKey("dbo.WorkShifts", t => t.WorkShiftID, cascadeDelete: true)
                .Index(t => t.RoomID)
                .Index(t => t.WorkShiftID);
            
            DropColumn("dbo.Trainings", "StartDate");
            DropColumn("dbo.Trainings", "EndDate");
            DropColumn("dbo.Trainings", "RoomID");
            DropColumn("dbo.Trainings", "WorkShiftID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Trainings", "WorkShiftID", c => c.Int(nullable: false));
            AddColumn("dbo.Trainings", "RoomID", c => c.Int(nullable: false));
            AddColumn("dbo.Trainings", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Trainings", "StartDate", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.Events", "WorkShiftID", "dbo.WorkShifts");
            DropForeignKey("dbo.Events", "RoomID", "dbo.Rooms");
            DropIndex("dbo.Events", new[] { "WorkShiftID" });
            DropIndex("dbo.Events", new[] { "RoomID" });
            DropTable("dbo.Events");
            CreateIndex("dbo.Trainings", "WorkShiftID");
            CreateIndex("dbo.Trainings", "RoomID");
            AddForeignKey("dbo.Trainings", "WorkShiftID", "dbo.WorkShifts", "WorkShiftID", cascadeDelete: true);
            AddForeignKey("dbo.Trainings", "RoomID", "dbo.Rooms", "RoomID", cascadeDelete: true);
        }
    }
}
