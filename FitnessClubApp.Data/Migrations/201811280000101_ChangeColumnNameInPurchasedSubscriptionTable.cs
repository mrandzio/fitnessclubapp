namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeColumnNameInPurchasedSubscriptionTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchasedSubscriptions", "EndDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.PurchasedSubscriptions", "EndTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchasedSubscriptions", "EndTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.PurchasedSubscriptions", "EndDate");
        }
    }
}
