namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFourTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DescriptionsOfUsers",
                c => new
                    {
                        DescriptionOfUserID = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Image = c.Binary(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DescriptionOfUserID)
                .ForeignKey("dbo.Users", t => t.DescriptionOfUserID)
                .Index(t => t.DescriptionOfUserID);
            
            CreateTable(
                "dbo.Entries",
                c => new
                    {
                        EntryID = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntryID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Equipments",
                c => new
                    {
                        EquipmentID = c.Int(nullable: false, identity: true),
                        Brand = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 50),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EquipmentID);
            
            CreateTable(
                "dbo.UsersInEvents",
                c => new
                    {
                        EventID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EventID, t.UserID })
                .ForeignKey("dbo.Events", t => t.EventID, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: false)
                .Index(t => t.EventID)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DescriptionsOfUsers", "DescriptionOfUserID", "dbo.Users");
            DropForeignKey("dbo.UsersInEvents", "UserID", "dbo.Users");
            DropForeignKey("dbo.UsersInEvents", "EventID", "dbo.Events");
            DropForeignKey("dbo.Entries", "UserID", "dbo.Users");
            DropIndex("dbo.UsersInEvents", new[] { "UserID" });
            DropIndex("dbo.UsersInEvents", new[] { "EventID" });
            DropIndex("dbo.Entries", new[] { "UserID" });
            DropIndex("dbo.DescriptionsOfUsers", new[] { "DescriptionOfUserID" });
            DropTable("dbo.UsersInEvents");
            DropTable("dbo.Equipments");
            DropTable("dbo.Entries");
            DropTable("dbo.DescriptionsOfUsers");
        }
    }
}
