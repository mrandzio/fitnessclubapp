namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPurchasedSubscriptionsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchasedSubscriptions",
                c => new
                    {
                        PurchasedSubscriptionID = c.Int(nullable: false, identity: true),
                        OrderDate = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartDate = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        StreetAddress = c.String(nullable: false),
                        ZipCode = c.String(nullable: false, maxLength: 10),
                        City = c.String(nullable: false, maxLength: 30),
                        Paid = c.Boolean(nullable: false),
                        Complete = c.Boolean(nullable: false),
                        Shipment = c.Boolean(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PurchasedSubscriptionID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchasedSubscriptions", "UserID", "dbo.Users");
            DropIndex("dbo.PurchasedSubscriptions", new[] { "UserID" });
            DropTable("dbo.PurchasedSubscriptions");
        }
    }
}
