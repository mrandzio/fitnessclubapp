namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnsToPurchasedSubscriptionsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchasedSubscriptions", "SubscriptionName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.PurchasedSubscriptions", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.PurchasedSubscriptions", "LastName", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.PurchasedSubscriptions", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchasedSubscriptions", "Name", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.PurchasedSubscriptions", "LastName");
            DropColumn("dbo.PurchasedSubscriptions", "FirstName");
            DropColumn("dbo.PurchasedSubscriptions", "SubscriptionName");
        }
    }
}
