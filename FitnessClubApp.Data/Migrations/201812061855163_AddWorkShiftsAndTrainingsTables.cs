namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWorkShiftsAndTrainingsTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkShifts",
                c => new
                    {
                        WorkShiftID = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.WorkShiftID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Trainings",
                c => new
                    {
                        TrainingID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        RoomID = c.Int(nullable: false),
                        WorkShiftID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TrainingID)
                .ForeignKey("dbo.Rooms", t => t.RoomID, cascadeDelete: true)
                .ForeignKey("dbo.WorkShifts", t => t.WorkShiftID, cascadeDelete: true)
                .Index(t => t.RoomID)
                .Index(t => t.WorkShiftID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trainings", "WorkShiftID", "dbo.WorkShifts");
            DropForeignKey("dbo.Trainings", "RoomID", "dbo.Rooms");
            DropForeignKey("dbo.WorkShifts", "UserID", "dbo.Users");
            DropIndex("dbo.Trainings", new[] { "WorkShiftID" });
            DropIndex("dbo.Trainings", new[] { "RoomID" });
            DropIndex("dbo.WorkShifts", new[] { "UserID" });
            DropTable("dbo.Trainings");
            DropTable("dbo.WorkShifts");
        }
    }
}
