namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TypeConversionForOrderDateInPurchasedSubscriptionsTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PurchasedSubscriptions", "OrderDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PurchasedSubscriptions", "OrderDate", c => c.Int(nullable: false));
        }
    }
}
