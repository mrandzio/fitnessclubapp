namespace FitnessClubApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTableNameAndAddingColumnInUserRolesTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UserRole", newName: "UserRoles");
            AddColumn("dbo.UserRoles", "PolishName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserRoles", "PolishName");
            RenameTable(name: "dbo.UserRoles", newName: "UserRole");
        }
    }
}
