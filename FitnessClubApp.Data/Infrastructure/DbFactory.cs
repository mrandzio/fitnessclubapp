﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClubApp.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        FitnessClubAppEntities dbContext;

        public FitnessClubAppEntities Init()
        {
            return dbContext ?? (dbContext = new FitnessClubAppEntities());
        }

        protected override void DisposeCore()
        {
            dbContext?.Dispose();
        }
    }
}
