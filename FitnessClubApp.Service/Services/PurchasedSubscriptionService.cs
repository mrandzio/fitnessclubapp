﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.WebPages;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Data.Repositories;
using FitnessClubApp.Model;
using Org.BouncyCastle.Bcpg;

namespace FitnessClubApp.Service
{
    public interface IPurchasedSubscriptionService
    {
        IEnumerable<PurchasedSubscription> GetPurchasedSubscriptions();
        IEnumerable<PurchasedSubscription> GetUserPurchasedSubscriptions(User user);
        IEnumerable<PurchasedSubscription> GetPurchasedSubscriptionsWithUser();
        void CreatePurchasedSubscription(PurchasedSubscription purchasedSubscription);
        void SavePurchasedSubscription();
        PurchasedSubscription GetPurchasedSubscription(int id);
        void EditPurchasedSubscription(PurchasedSubscription purchasedSubscription);
        void RemovePurchasedSubscription(PurchasedSubscription purchasedSubscription);
        bool UserIsActiveSubscriber(string userEmail);
        PurchasedSubscription GetActiveUserSubscription(string userEmail);
        bool PurchasedSubscriptionExists(int userID, string startDate, int days);
        PurchasedSubscription GetLastUserPurchasedSubscription(int userID);

        bool UserInActiveSubscription(string userEmail, DateTime date);
    }

    public class PurchasedSubscriptionService : IPurchasedSubscriptionService
    {
        private readonly IPurchasedSubscriptionRepository purchasedSubscriptionRepository;
        private readonly IUnitOfWork unitOfWork;

        public PurchasedSubscriptionService(IPurchasedSubscriptionRepository purchasedSubscriptionRepository, IUnitOfWork unitOfWork)
        {
            this.purchasedSubscriptionRepository = purchasedSubscriptionRepository;
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<PurchasedSubscription> GetPurchasedSubscriptions()
        {
            var subscriptions = purchasedSubscriptionRepository.GetAll();
            return subscriptions;
        }
        public IEnumerable<PurchasedSubscription> GetPurchasedSubscriptionsWithUser()
        {
            var users = purchasedSubscriptionRepository.GetAllWithUser().OrderByDescending(ps => ps.OrderDate);
            return users;
        }

        public IEnumerable<PurchasedSubscription> GetUserPurchasedSubscriptions(User user)
        {
            var subscriptions = purchasedSubscriptionRepository.GetMany(ps => ps.UserID == user.UserID).OrderByDescending(ps => ps.StartDate);
            return subscriptions;
        }

        public void CreatePurchasedSubscription(PurchasedSubscription purchasedSubscription)
        {
            purchasedSubscriptionRepository.Add(purchasedSubscription);
        }

        public void SavePurchasedSubscription()
        {
            unitOfWork.Commit();
        }

        public void EditPurchasedSubscription(PurchasedSubscription purchasedSubscription)
        {
            purchasedSubscriptionRepository.Update(purchasedSubscription);
        }

        public void RemovePurchasedSubscription(PurchasedSubscription purchasedSubscription)
        {
            purchasedSubscriptionRepository.Delete(purchasedSubscription);
        }

        public PurchasedSubscription GetPurchasedSubscription(int id)
        {
            var purchasedSubscription = purchasedSubscriptionRepository.GetById(id);
            return purchasedSubscription;
        }

        public bool UserIsActiveSubscriber(string userEmail)
        {
            var activeUserSubscription = purchasedSubscriptionRepository.GetAllWithUser().FirstOrDefault(ps =>
                ps.User.Email == userEmail && ps.Complete && ps.StartDate.Date <= DateTime.Now.Date && ps.EndDate.Date >= DateTime.Now.Date);

            return activeUserSubscription != null;
        }

        public bool UserInActiveSubscription(string userEmail, DateTime date)
        {
            var activeUserSubscription = purchasedSubscriptionRepository.GetAllWithUser().FirstOrDefault(ps =>
                ps.User.Email == userEmail && ps.Complete && ps.StartDate.Date <= date.Date && ps.EndDate.Date >= date.Date);

            return activeUserSubscription != null;
        }

        public PurchasedSubscription GetActiveUserSubscription(string userEmail)
        {
            var activeUserSubscription = purchasedSubscriptionRepository.GetAllWithUser().FirstOrDefault(ps =>
                ps.User.Email == userEmail && ps.Complete && ps.StartDate.Date <= DateTime.Now.Date && ps.EndDate >= DateTime.Now.Date);

            return activeUserSubscription;
        }

        public bool PurchasedSubscriptionExists(int userID, string startDate, int days)
        {
            var purchasedSubscription = new PurchasedSubscription
            {
                UserID = userID,
                StartDate = startDate.AsDateTime(),
                EndDate = startDate.AsDateTime().AddDays(days)
            };

            var workShiftExists = purchasedSubscriptionRepository.GetAll().Any(ws => ws.UserID == purchasedSubscription.UserID
                                                                                                                 && (purchasedSubscription.StartDate >= ws.StartDate && purchasedSubscription.StartDate <= ws.EndDate
                                                                                                                     || purchasedSubscription.EndDate <= ws.EndDate && purchasedSubscription.StartDate >= ws.StartDate
                                                                                                                     || purchasedSubscription.EndDate >= ws.EndDate && ws.StartDate >= purchasedSubscription.StartDate
                                                                                                                     || purchasedSubscription.StartDate <= ws.StartDate && ws.StartDate <= purchasedSubscription.EndDate));

            return workShiftExists;
        }

        public PurchasedSubscription GetLastUserPurchasedSubscription(int userID)
        {
            var purchasedSubscription = purchasedSubscriptionRepository.Get(ws => ws.UserID == userID);
            return purchasedSubscription;
        }
    }
}
