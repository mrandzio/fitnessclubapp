﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Data.Repositories;
using FitnessClubApp.Model;

namespace FitnessClubApp.Service
{
    public interface IResourceService
    {
        IEnumerable<Room> GetRooms();
        IEnumerable<Training> GetTrainings();
        void CreateRoom(Room room);
        void SaveRoom();
        void EditRoom(Room room);
        Room GetRoom(int id);
        bool RoomExists(Room room);
        void CreateTraining(Training training);
        void SaveTraining();
        void EditTraining(Training training);
        Training GetTraining(int id);
        bool TrainingExists(Training training);
        void RemoveTraining(Training training);
        IEnumerable<DescriptionOfUser> GetDescriptionsOfUsers();
        void EditDescriptionOfUser(DescriptionOfUser descriptionOfUser);
        void CreateDescriptionOfUser(DescriptionOfUser descriptionOfUser);
        void RemoveDescriptionOfUser(DescriptionOfUser descriptionOfUser);
        void SaveDescriptionOfUser();
        DescriptionOfUser GetDescriptionOfUser(int descriptionID);
        IEnumerable<Equipment> GetEquipments();
        Equipment GetEquipment(int id);
        void CreateEquipment(Equipment equipment);
        void EditEquipment(Equipment equipment);
        void RemoveEquipment(Equipment equipment);
        void SaveEquipment();
        bool EquipmentExists(Equipment equipment);
        bool EventInRoomExists(int roomID);
        void RemoveRoom(Room room);
    }

    public class ResourceService : IResourceService
    {
        private readonly ITrainingRepository trainingRepository;
        private readonly IRoomRepository roomRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IDescriptionOfUserRepository descriptionOfUserRepository;
        private readonly IEquipmentRepository equipmentRepository;
        private readonly IEventRepository eventRepository;

        public ResourceService(ITrainingRepository trainingRepository, IRoomRepository roomRepository, 
            IUnitOfWork unitOfWork, IDescriptionOfUserRepository descriptionOfUserRepository, IEquipmentRepository equipmentRepository, IEventRepository eventRepository)
        {
            this.trainingRepository = trainingRepository;
            this.roomRepository = roomRepository;
            this.unitOfWork = unitOfWork;
            this.descriptionOfUserRepository = descriptionOfUserRepository;
            this.equipmentRepository = equipmentRepository;
            this.eventRepository = eventRepository;
        }

        public IEnumerable<Room> GetRooms()
        {
            var rooms = roomRepository.GetAll();
            return rooms;
        }

        public IEnumerable<Training> GetTrainings()
        {
            var trainings = trainingRepository.GetAll();
            return trainings;
        }

        public void CreateRoom(Room room)
        {
            roomRepository.Add(room);
        }

        public void SaveTraining()
        {
            unitOfWork.Commit();
        }

        public void EditRoom(Room room)
        {
            roomRepository.Update(room);
        }

        public Room GetRoom(int id)
        {
            var room = roomRepository.GetById(id);
            return room;
        }

        public bool RoomExists(Room room)
        {
            var roomExists = roomRepository.GetAll().Any(r =>
                r.RoomID != room.RoomID && r.Name.Equals(room.Name, StringComparison.CurrentCultureIgnoreCase));
            return roomExists;
        } 

        public void CreateTraining(Training training)
        {
            trainingRepository.Add(training);
        }

        public void SaveRoom()
        {
            unitOfWork.Commit();
        }

        public void EditTraining(Training training)
        {
            trainingRepository.Update(training);
        }

        public Training GetTraining(int id)
        {
            var training = trainingRepository.GetById(id);
            return training;
        }

        public void RemoveTraining(Training training)
        {
            trainingRepository.Delete(training);
        }

        public IEnumerable<DescriptionOfUser> GetDescriptionsOfUsers()
        {
            var descriptionsOfUsers = descriptionOfUserRepository.GetAllWithEntities();
            return descriptionsOfUsers;
        }

        public void EditDescriptionOfUser(DescriptionOfUser descriptionOfUser)
        {
            descriptionOfUserRepository.Update(descriptionOfUser);
        }

        public void CreateDescriptionOfUser(DescriptionOfUser descriptionOfUser)
        {
            descriptionOfUserRepository.Add(descriptionOfUser);
        }

        public void RemoveDescriptionOfUser(DescriptionOfUser descriptionOfUser)
        {
            descriptionOfUserRepository.Delete(descriptionOfUser);
        }

        public void SaveDescriptionOfUser()
        {
            unitOfWork.Commit();
        }

        public DescriptionOfUser GetDescriptionOfUser(int descriptionID)
        {
            var descriptionOfUser = descriptionOfUserRepository.GetById(descriptionID);
            return descriptionOfUser;
        }

        public bool TrainingExists(Training training)
        {
            var roomExists = trainingRepository.GetAll().Any(r =>
                r.TrainingID != training.TrainingID && r.Name.Equals(training.Name, StringComparison.CurrentCultureIgnoreCase));
            return roomExists;
        }

        public IEnumerable<Equipment> GetEquipments()
        {
            var equipments = equipmentRepository.GetAll();
            return equipments;
        }

        public Equipment GetEquipment(int id)
        {
            var equipment = equipmentRepository.GetById(id);
            return equipment;
        }

        public void CreateEquipment(Equipment equipment)
        {
            equipmentRepository.Add(equipment);
        }

        public void EditEquipment(Equipment equipment)
        {
            equipmentRepository.Update(equipment);
        }

        public void RemoveEquipment(Equipment equipment)
        {
            equipmentRepository.Delete(equipment);
        }

        public void SaveEquipment()
        {
            unitOfWork.Commit();
        }

        public bool EquipmentExists(Equipment equipment)
        {
            var equipmentExists = equipmentRepository.Get(eq => eq.EquipmentID != equipment.EquipmentID
                                                                && eq.Name.Trim().Equals(equipment.Name.Trim(),
                                                                    StringComparison.CurrentCultureIgnoreCase)
                                                                && eq.Brand.Trim().Equals(equipment.Brand.Trim(),
                                                                    StringComparison.CurrentCultureIgnoreCase));

            return equipmentExists != null;
        }

        public bool EventInRoomExists(int roomID)
        {
            var @event = eventRepository.Get(eq => eq.RoomID == roomID);
            return @event != null;
        }

        public void RemoveRoom(Room room)
        {
            roomRepository.Delete(room);
        }
    }
}
