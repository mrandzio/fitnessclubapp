﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Data.Repositories;
using FitnessClubApp.Model;

namespace FitnessClubApp.Service
{
    public interface IWorkScheduleService
    {
        IEnumerable<Room> GetRooms();
        IEnumerable<User> GetWorkers();
        IEnumerable<WorkShift> GetWorkShifts(DateTime startDate, DateTime endDate);
        IEnumerable<Event> GetEvents();
        IEnumerable<Event> GetEvents(DateTime startDate, DateTime endDate);
        void SaveWorkShift();
        void CreateWorkShift(WorkShift workShift);
        void CreateEvent(Event @event);
        void EditEvent(Event @event);
        void SaveEvent();
        bool WorkShiftExists(WorkShift workShift);
        bool EventExists(Event @event);
        WorkShift GetWorkShift(int userID, DateTime startDate, DateTime endDate);
        Event GetEvent(int eventID);
        WorkShift GetWorkShift(int workShiftID);
        void RemoveWorkShift(WorkShift workShift);
        void RemoveEvent(Event @event);
        void EditWorkShift(WorkShift workShift);
        IEnumerable<WorkShift> GetUserWorkShifts(int userID, DateTime startDate, DateTime endDate);
        IEnumerable<Event> GetUserEvents(int userID, DateTime startDate, DateTime endDate);
    }

    public class WorkScheduleService : IWorkScheduleService
    {
        private readonly IUserRepository userRepository;
        private readonly IRoomRepository roomRepository;
        private readonly IWorkShiftRepository workShiftRepository;
        private readonly IEventRepository eventRepository;
        private readonly IUnitOfWork unitOfWork;

        public WorkScheduleService(IUserRepository userRepository, IRoomRepository roomRepository, 
            IWorkShiftRepository workShiftRepository, IEventRepository eventRepository, IUnitOfWork unitOfWork)
        {
            this.userRepository = userRepository;
            this.roomRepository = roomRepository;
            this.workShiftRepository = workShiftRepository;
            this.eventRepository = eventRepository;
            this.unitOfWork = unitOfWork;
        }
    
        public IEnumerable<Room> GetRooms()
        {
            var rooms = roomRepository.GetAll();
            return rooms;
        }

        public IEnumerable<User> GetWorkers()
        {
            var workers = userRepository.GetAllWithRoleUser().Where(u => new[] { "Trainer", "Receptionist" }.Any(ur => ur.Equals(u.UserRole.Name)))
                .OrderBy(u => u.UserRole.Name).ThenBy(u => $"{u.FirstName} {u.LastName}");
            return workers;
        }

        public IEnumerable<WorkShift> GetWorkShifts(DateTime startDate, DateTime endDate)
        {
            var workShifts = workShiftRepository.GetAllWithEntities().Where(ws => ws.StartDate >= startDate && ws.StartDate <= endDate || ws.EndDate >= startDate && ws.EndDate <= endDate);
            return workShifts;
        }

        public IEnumerable<Event> GetEvents()
        {
            var events = eventRepository.GetAllWithEntities();
            return events;
        }

        public IEnumerable<Event> GetEvents(DateTime startDate, DateTime endDate)
        {
            var events = eventRepository.GetAllWithEntities().Where(ws => ws.StartDate >= startDate && ws.StartDate <= endDate || ws.EndDate >= startDate && ws.EndDate <= endDate);
            return events;
        }

        public IEnumerable<Event> GetUserEvents(int userID, DateTime startDate, DateTime endDate)
        {
            var events = eventRepository.GetAllWithEntities().Where(ws => ws.WorkShift.UserID == userID && (ws.StartDate >= startDate && ws.StartDate <= endDate || ws.EndDate >= startDate && ws.EndDate <= endDate));
            return events;
        }

        public IEnumerable<WorkShift> GetUserWorkShifts(int userID, DateTime startDate, DateTime endDate)
        {
            var workShifts = workShiftRepository.GetAllWithEntities().Where(ws => ws.UserID == userID && (ws.StartDate >= startDate && ws.StartDate <= endDate || ws.EndDate >= startDate && ws.EndDate <= endDate));
            return workShifts;
        }

        public void SaveWorkShift()
        {
            unitOfWork.Commit();
        }

        public bool WorkShiftExists(WorkShift workShift)
        {
            var workShiftExists = workShiftRepository.GetAll().Any(ws => ws.WorkShiftID != workShift.WorkShiftID && ws.UserID == workShift.UserID 
                && (workShift.StartDate >= ws.StartDate && workShift.StartDate <= ws.EndDate
                || workShift.EndDate <= ws.EndDate && workShift.StartDate >= ws.StartDate
                || workShift.EndDate >= ws.EndDate && ws.StartDate >= workShift.StartDate
                || workShift.StartDate <= ws.StartDate && ws.StartDate <= workShift.EndDate));

            return workShiftExists;
        }

        public bool EventExists(Event @event)
        {
            var eventExists = eventRepository.GetAll().Any(e => e.EventID != @event.EventID && (e.WorkShiftID == @event.WorkShiftID || e.RoomID == @event.RoomID)
                                                                         && (@event.StartDate >= e.StartDate && @event.StartDate <= e.EndDate
                                                                             || @event.EndDate <= e.EndDate && @event.StartDate >= e.StartDate
                                                                             || @event.EndDate >= e.EndDate && e.StartDate >= @event.StartDate
                                                                             || @event.StartDate <= e.StartDate && e.StartDate <= @event.EndDate));

            return eventExists;
        }

        public void CreateWorkShift(WorkShift workShift)
        {
            workShiftRepository.Add(workShift);
        }

        public void CreateEvent(Event @event)
        {
            eventRepository.Add(@event);
        }

        public void SaveEvent()
        {
            unitOfWork.Commit();
        }

        public WorkShift GetWorkShift(int userID, DateTime startDate, DateTime endDate)
        {
            var workShift = workShiftRepository.Get(ws => ws.UserID == userID && ws.StartDate <= startDate && ws.EndDate >= endDate);
            return workShift;
        }

        public Event GetEvent(int eventID)
        {
            var @event = eventRepository.GetWithEntities(e => e.EventID == eventID);
            return @event;
        }

        public WorkShift GetWorkShift(int workShiftID)
        {
            var workShift = workShiftRepository.GetWithEntities(e => e.WorkShiftID == workShiftID);
            return workShift;
        }

        public void RemoveWorkShift(WorkShift workShift)
        {
            var events = workShift.Events.ToList();
            events.ForEach(e =>
            {
                var @event = eventRepository.GetWithEntities(ev => e.EventID == ev.EventID);
                eventRepository.Delete(@event);
            });
            workShiftRepository.Delete(workShift);
        }

        public void RemoveEvent(Event @event)
        {
            eventRepository.Delete(@event);
        }

        public void EditEvent(Event @event)
        {
            eventRepository.Update(@event);
        }

        public void EditWorkShift(WorkShift workShift)
        {
            var events = workShift.Events.ToList();
            events.ForEach(e =>
            {
                var @event = eventRepository.GetWithEntities(ev => e.EventID == ev.EventID);
                eventRepository.Delete(@event);
            });
            workShiftRepository.Update(workShift);
        }
    }
}
