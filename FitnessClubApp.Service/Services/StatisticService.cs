﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.WebPages;
using FitnessClubApp.Model;

namespace FitnessClubApp.Service
{
    public class StatisticModel
    {
        public List<string> Labels { get; set; } = new List<string>();
        public List<int> Data { get; set; } = new List<int>();
    }

    public class TemporaryClass
    {
        public string Date { get; set; }
        public string Type { get; set; }
        public int Count { get; set; }
    }

    public class AmountOfObjects
    {
        public string Key { get; set; }

        public List<int> Count { get; set; } = new List<int>();
    }

    public class MultipleStatisticModel
    {
        public List<string> Labels { get; set; } = new List<string>();
        public List<AmountOfObjects> Data { get; set; } = new List<AmountOfObjects>();
    }

    public interface IStatisticService
    {
        StatisticModel GetDailyAmountOfSales(DateTime startDate, DateTime endDate);
        StatisticModel GetMonthlyAmountOfSales(DateTime startDate, DateTime endDate);
        StatisticModel GetAnnualAmountOfSales(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetDailyAmountTypesOfSales(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetMonthlyAmountTypesOfSales(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetAnnualAmountTypesOfSales(DateTime startDate, DateTime endDate);
        StatisticModel GetChosenSubscription();
        StatisticModel GetChosenEvents();
        StatisticModel GetDailyRegisteredAccounts(DateTime startDate, DateTime endDate);
        StatisticModel GetMonthlyRegisteredAccounts(DateTime startDate, DateTime endDate);
        StatisticModel GetAnnualRegisteredAccounts(DateTime startDate, DateTime endDate);
        StatisticModel GetDailyRegisteredEntries(DateTime startDate, DateTime endDate);
        StatisticModel GetMonthlyRegisteredEntries(DateTime startDate, DateTime endDate);
        StatisticModel GetAnnualRegisteredEntries(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetDailyRegisteredEntriesByGender(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetMonthlyRegisteredEntriesByGender(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetAnnualRegisteredEntriesByGender(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetDailyRegisteredEntriesByDateOfBirth(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetMonthlyRegisteredEntriesByDateOfBirth(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetAnnualRegisteredEntriesByDateOfBirth(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetDailyRegisteredEntriesByEntryTime(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetMonthlyRegisteredEntriesByEntryTime(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetAnnualRegisteredEntriesByEntryTime(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetDailyUsersInEventsByName(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetMonthlyUsersInEventsByName(DateTime startDate, DateTime endDate);
        MultipleStatisticModel GetAnnualUsersInEventsByName(DateTime startDate, DateTime endDate);
        int GetDailyAmountOfSales();
        int GetMonthlyAmountOfSales();
        int GetAnnualAmountOfSales();
        int GetAllAmountOfSales();
        int GetDailyRegisteredAccounts();
        int GetMonthlyRegisteredAccounts();
        int GetAnnualRegisteredAccounts();
        int GetAllRegisteredAccounts();
        int GetDailyRegisteredEntries();
        int GetMonthlyRegisteredEntries();
        int GetAnnualRegisteredEntries();
        int GetAllRegisteredEntries();
        int GetDailyUsersInEvents();
        int GetMonthlyUsersInEvents();
        int GetAnnualUsersInEvents();
        int GetAllUsersInEvents();
        int GetAmountOfReceptionists();
        int GetAmountOfTrainers();
        int GetAmountOfManagers();
        int GetAmountOfEquipments();
        int GetAmountOfRooms();
        int GetAmountOfTrainings();
        int GetAmountOfSubscriptions();
    }

    public class StatisticService : IStatisticService
    {
        private readonly IPurchasedSubscriptionService purchasedSubscriptionService;
        private readonly IUserService userService;
        private readonly IEntryRegisterService entryRegisterService;
        private readonly IWorkScheduleService workScheduleService;
        private readonly IResourceService resourceService;
        private readonly ISubscriptionService subscriptionService;

        public StatisticService(IPurchasedSubscriptionService purchasedSubscriptionService, IUserService userService, 
            IEntryRegisterService entryRegisterService, IWorkScheduleService workScheduleService, IResourceService resourceService, ISubscriptionService subscriptionService)
        {
            this.purchasedSubscriptionService = purchasedSubscriptionService;
            this.userService = userService;
            this.entryRegisterService = entryRegisterService;
            this.workScheduleService = workScheduleService;
            this.resourceService = resourceService;
            this.subscriptionService = subscriptionService;
        }

        public StatisticModel GetDailyAmountOfSales(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptions();

            for (; startDate <= endDate; startDate = startDate.AddDays(1))
            {
                statisticModel.Labels.Add(startDate.ToString("dd.MM.yy"));
                statisticModel.Data.Add(purchasedSubscriptions.Count(ps => ps.OrderDate.Date == startDate));
            }

            return statisticModel;
        }

        public StatisticModel GetMonthlyAmountOfSales(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptions();

            for (; startDate <= endDate; startDate = startDate.AddMonths(1))
            {
                statisticModel.Labels.Add(startDate.ToString("MM.yyyy"));
                statisticModel.Data.Add(purchasedSubscriptions.Count(ps => ps.OrderDate.Date.Month == startDate.Month
                                                                           && ps.OrderDate.Date.Year == startDate.Year));
            }

            return statisticModel;
        }

        public StatisticModel GetAnnualAmountOfSales(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptions();

            for (; startDate <= endDate; startDate = startDate.AddYears(1))
            {
                statisticModel.Labels.Add(startDate.ToString("yyyy"));
                statisticModel.Data.Add(purchasedSubscriptions.Count(ps => ps.OrderDate.Date.Year == startDate.Year));
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetDailyAmountTypesOfSales(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptions();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddDays(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("dd.MM.yy"));
            }

            var purchasedSubscriptionsAfterGrouping = purchasedSubscriptions.Where(ps => ps.OrderDate.Date >= startDate && ps.OrderDate.Date <= endDate)
                    .GroupBy(ps => new { Date = ps.OrderDate.Date.ToString("dd.MM.yy"), Type = $"{ps.EndDate.Subtract(ps.StartDate).Days} dni za {ps.Price} zł" })
                    .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = purchasedSubscriptionsAfterGrouping.Select(ps => ps.Type).Distinct().ToList();
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {       
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(purchasedSubscriptionsAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetMonthlyAmountTypesOfSales(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptions();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddMonths(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("MM.yyyy"));
            }

            var purchasedSubscriptionsAfterGrouping = purchasedSubscriptions.Where(ps => statisticModel.Labels.Any(month => ps.OrderDate.ToString("MM.yyyy") == month))
                .GroupBy(ps => new { Date = ps.OrderDate.Date.ToString("MM.yyyy"), Type = $"{ps.EndDate.Subtract(ps.StartDate).Days} dni za {ps.Price} zł" })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = purchasedSubscriptionsAfterGrouping.Select(ps => ps.Type).Distinct().ToList();
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(purchasedSubscriptionsAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetAnnualAmountTypesOfSales(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptions();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddYears(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("yyyy"));
            }

            var purchasedSubscriptionsAfterGrouping = purchasedSubscriptions.Where(ps => ps.OrderDate.Year >= startDate.Year && ps.OrderDate.Year <= endDate.Year)
                .GroupBy(ps => new { Date = ps.OrderDate.Date.ToString("yyyy"), Type = $"{ps.EndDate.Subtract(ps.StartDate).Days} dni za {ps.Price} zł" })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = purchasedSubscriptionsAfterGrouping.Select(ps => ps.Type).Distinct().ToList();
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(purchasedSubscriptionsAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public StatisticModel GetChosenSubscription()
        {
            var statisticModel = new StatisticModel();

            var purchasedSubscriptions = purchasedSubscriptionService.GetPurchasedSubscriptions();

            var purchasedSubscriptionsAfterGrouping = purchasedSubscriptions.GroupBy(ps => $"{ps.EndDate.Subtract(ps.StartDate).Days} dniowe")
                .Select(ps => new TemporaryClass { Type = ps.Key, Count = ps.Count() }).ToList();

            statisticModel.Labels = purchasedSubscriptionsAfterGrouping.Select(ps => ps.Type).ToList();
            statisticModel.Data = purchasedSubscriptionsAfterGrouping.Select(ps => ps.Count).ToList();

            return statisticModel;
        }

        public StatisticModel GetDailyRegisteredAccounts(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var users = userService.GetUsersWithRole();

            for (; startDate <= endDate; startDate = startDate.AddDays(1))
            {
                statisticModel.Labels.Add(startDate.ToString("dd.MM.yy"));
                statisticModel.Data.Add(users.Count(u => u.DateCreated.Date == startDate && u.UserRole.Name == "User"));
            }

            return statisticModel;
        }

        public StatisticModel GetMonthlyRegisteredAccounts(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var users = userService.GetUsersWithRole();

            for (; startDate <= endDate; startDate = startDate.AddMonths(1))
            {
                statisticModel.Labels.Add(startDate.ToString("MM.yyyy"));
                statisticModel.Data.Add(users.Count(u => u.DateCreated.Date.Month == startDate.Month
                                                         && u.DateCreated.Date.Year == startDate.Year && u.UserRole.Name == "User"));
            }

            return statisticModel;
        }

        public StatisticModel GetAnnualRegisteredAccounts(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var users = userService.GetUsersWithRole();

            for (; startDate <= endDate; startDate = startDate.AddYears(1))
            {
                statisticModel.Labels.Add(startDate.ToString("yyyy"));
                statisticModel.Data.Add(users.Count(u => u.DateCreated.Date.Year == startDate.Year && u.UserRole.Name == "User"));
            }

            return statisticModel;
        }

        public StatisticModel GetDailyRegisteredEntries(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; startDate <= endDate; startDate = startDate.AddDays(1))
            {
                statisticModel.Labels.Add(startDate.ToString("dd.MM.yy"));
                statisticModel.Data.Add(entriesRegister.Count(u => u.StartDate.Date == startDate));
            }

            return statisticModel;
        }

        public StatisticModel GetMonthlyRegisteredEntries(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; startDate <= endDate; startDate = startDate.AddMonths(1))
            {
                statisticModel.Labels.Add(startDate.ToString("MM.yyyy"));
                statisticModel.Data.Add(entriesRegister.Count(u => u.StartDate.Date.Month == startDate.Month
                                                                   && u.StartDate.Date.Year == startDate.Year));
            }

            return statisticModel;
        }

        public StatisticModel GetAnnualRegisteredEntries(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new StatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; startDate <= endDate; startDate = startDate.AddYears(1))
            {
                statisticModel.Labels.Add(startDate.ToString("yyyy"));
                statisticModel.Data.Add(entriesRegister.Count(u => u.StartDate.Date.Year == startDate.Year));
            }

            return statisticModel;
        }

        public int GetDailyAmountOfSales()
        {
            return purchasedSubscriptionService.GetPurchasedSubscriptions()
                .Count(ps => ps.OrderDate.Date == DateTime.Now.Date);
        }

        public int GetMonthlyAmountOfSales()
        {
            return purchasedSubscriptionService.GetPurchasedSubscriptions()
                .Count(ps => ps.OrderDate.Month == DateTime.Now.Month
                             && ps.OrderDate.Year == DateTime.Now.Year);
        }

        public int GetAnnualAmountOfSales()
        {
            return purchasedSubscriptionService.GetPurchasedSubscriptions()
                .Count(ps => ps.OrderDate.Year == DateTime.Now.Year);
        }

        public int GetDailyRegisteredAccounts()
        {
            return userService.GetUsersWithRole()
                .Count(u => u.DateCreated.Date == DateTime.Now.Date && u.UserRole.Name == "User");
        }

        public int GetMonthlyRegisteredAccounts()
        {
            return userService.GetUsersWithRole()
                .Count(u => u.DateCreated.Month == DateTime.Now.Month && u.DateCreated.Date.Year == DateTime.Now.Year && u.UserRole.Name == "User");
        }

        public int GetAnnualRegisteredAccounts()
        {
            return userService.GetUsersWithRole()
                .Count(u => u.DateCreated.Year == DateTime.Now.Year && u.UserRole.Name == "User");
        }

        public int GetDailyRegisteredEntries()
        {
            return entryRegisterService.GetEntriesRegister()
                .Count(ps => ps.StartDate.Date == DateTime.Now.Date);
        }

        public int GetMonthlyRegisteredEntries()
        {
            return entryRegisterService.GetEntriesRegister()
                .Count(ps => ps.StartDate.Month == DateTime.Now.Month
                             && ps.StartDate.Year == DateTime.Now.Year);
        }

        public int GetAnnualRegisteredEntries()
        {
            return entryRegisterService.GetEntriesRegister()
                .Count(ps => ps.StartDate.Year == DateTime.Now.Year);
        }

        public int GetDailyUsersInEvents()
        {
            return workScheduleService.GetEvents()
                .Where(ps => ps.StartDate.Date == DateTime.Now.Date).Sum(ps => ps.UsersOnEvent.Count);
        }

        public int GetMonthlyUsersInEvents()
        {
            return workScheduleService.GetEvents()
                .Where(ps => ps.StartDate.Month == DateTime.Now.Month && ps.StartDate.Year == DateTime.Now.Year).Sum(ps => ps.UsersOnEvent.Count);
        }

        public int GetAnnualUsersInEvents()
        {
            return workScheduleService.GetEvents()
                .Where(ps => ps.StartDate.Year == DateTime.Now.Year).Sum(ps => ps.UsersOnEvent.Count);
        }

        public int GetAllAmountOfSales()
        {
            return purchasedSubscriptionService.GetPurchasedSubscriptions().Count();
        }

        public int GetAllRegisteredAccounts()
        {
            return userService.GetUsersWithRole().Count(u => u.UserRole.Name == "User");
        }

        public int GetAllRegisteredEntries()
        {
            return entryRegisterService.GetEntriesRegister().Count();
        }

        public int GetAllUsersInEvents()
        {
            return workScheduleService.GetEvents().Sum(ps => ps.UsersOnEvent.Count);
        }

        public int GetAmountOfReceptionists()
        {
            return userService.GetUsersWithRole().Count(u => u.UserRole.Name == "Receptionist");
        }

        public int GetAmountOfTrainers()
        {
            return userService.GetUsersWithRole().Count(u => u.UserRole.Name == "Trainer");
        }

        public int GetAmountOfManagers()
        {
            return userService.GetUsersWithRole().Count(u => u.UserRole.Name == "Manager");
        }

        public int GetAmountOfEquipments()
        {
            return resourceService.GetEquipments().GroupBy(e => e.Name).Count();
        }

        public int GetAmountOfRooms()
        {
            return resourceService.GetRooms().Count();
        }

        public int GetAmountOfTrainings()
        {
            return resourceService.GetTrainings().Count();
        }

        public int GetAmountOfSubscriptions()
        {
            return subscriptionService.GetSubscriptions().Count();
        }

        public MultipleStatisticModel GetDailyRegisteredEntriesByGender(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddDays(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("dd.MM.yy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => ps.StartDate.Date >= startDate && ps.StartDate.Date <= endDate)
                    .GroupBy(ps => new { Date = ps.StartDate.Date.ToString("dd.MM.yy"), Type = ps.User.Male ? "Mężczyzna" : "Kobieta" })
                    .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "Mężczyzna", "Kobieta" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetMonthlyRegisteredEntriesByGender(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddMonths(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("MM.yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("MM.yyyy") == month))
                .GroupBy(ps => new { Date = ps.StartDate.Date.ToString("MM.yyyy"), Type = ps.User.Male ? "Mężczyzna" : "Kobieta" })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "Mężczyzna", "Kobieta" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetAnnualRegisteredEntriesByGender(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddYears(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("yyyy") == month))
                .GroupBy(ps => new { Date = ps.StartDate.Date.ToString("yyyy"), Type = ps.User.Male ? "Mężczyzna" : "Kobieta" })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "Mężczyzna", "Kobieta" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetDailyRegisteredEntriesByDateOfBirth(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddDays(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("dd.MM.yy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => ps.StartDate.Date >= startDate && ps.StartDate.Date <= endDate)
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("dd.MM.yy"),
                    Type = ps.StartDate.Year - ps.User.DateOfBirth.Year < 26 ? "12 - 25 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 36 ? "26 - 35 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 46 ? "36 - 45 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 56 ? "46 - 55 lat"
                        : "56+ lat"
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "12 - 25 lat", "26 - 35 lat", "36 - 45 lat", "46 - 55 lat", "56+ lat" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetMonthlyRegisteredEntriesByDateOfBirth(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddMonths(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("MM.yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("MM.yyyy") == month))
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("MM.yyyy"),
                    Type = ps.StartDate.Year - ps.User.DateOfBirth.Year < 26 ? "12 - 25 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 36 ? "26 - 35 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 46 ? "36 - 45 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 56 ? "46 - 55 lat"
                        : "56+ lat"
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "12 - 25 lat", "26 - 35 lat", "36 - 45 lat", "46 - 55 lat", "56+ lat" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetAnnualRegisteredEntriesByDateOfBirth(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddYears(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("yyyy") == month))
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("yyyy"),
                    Type = ps.StartDate.Year - ps.User.DateOfBirth.Year < 26 ? "12 - 25 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 36 ? "26 - 35 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 46 ? "36 - 45 lat"
                        : ps.StartDate.Year - ps.User.DateOfBirth.Year < 56 ? "46 - 55 lat"
                        : "56+ lat"
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "12 - 25 lat", "26 - 35 lat", "36 - 45 lat", "46 - 55 lat", "56+ lat" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetDailyRegisteredEntriesByEntryTime(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddDays(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("dd.MM.yy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => ps.StartDate.Date >= startDate && ps.StartDate.Date <= endDate)
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("dd.MM.yy"),
                    Type = ps.StartDate.Hour + 1 < 8 ? "0:00 - 7:59"
                        : ps.StartDate.Hour + 1 < 12 ? "8:00 - 11:59"
                        : ps.StartDate.Hour + 1 < 16 ? "12:00 - 15:59"
                        : ps.StartDate.Hour + 1 < 20 ? "16:00 - 19:59"
                        : "20:00 - 23:59"
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "0:00 - 7:59", "8:00 - 11:59", "12:00 - 15:59", "16:00 - 19:59", "20:00 - 23:59" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetMonthlyRegisteredEntriesByEntryTime(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddMonths(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("MM.yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("MM.yyyy") == month))
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("MM.yyyy"),
                    Type = ps.StartDate.Hour + 1 < 8 ? "0:00 - 7:59"
                        : ps.StartDate.Hour + 1 < 12 ? "8:00 - 11:59"
                        : ps.StartDate.Hour + 1 < 16 ? "12:00 - 15:59"
                        : ps.StartDate.Hour + 1 < 20 ? "16:00 - 19:59"
                        : "20:00 - 23:59"
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "0:00 - 7:59", "8:00 - 11:59", "12:00 - 15:59", "16:00 - 19:59", "20:00 - 23:59" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetAnnualRegisteredEntriesByEntryTime(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = entryRegisterService.GetEntriesRegister();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddYears(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("yyyy") == month))
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("yyyy"),
                    Type = ps.StartDate.Hour + 1 < 8 ? "0:00 - 7:59"
                        : ps.StartDate.Hour + 1 < 12 ? "8:00 - 11:59"
                        : ps.StartDate.Hour + 1 < 16 ? "12:00 - 15:59"
                        : ps.StartDate.Hour + 1 < 20 ? "16:00 - 19:59"
                        : "20:00 - 23:59"
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Count() }).ToList();

            var labels = new List<string> { "0:00 - 7:59", "8:00 - 11:59", "12:00 - 15:59", "16:00 - 19:59", "20:00 - 23:59" };
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetDailyUsersInEventsByName(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = workScheduleService.GetEvents();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddDays(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("dd.MM.yy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => ps.StartDate.Date >= startDate && ps.StartDate.Date <= endDate)
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("dd.MM.yy"),
                    Type = ps.Name
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Sum(p => p.UsersOnEvent.Count) }).ToList();

            var labels = entriesRegisterAfterGrouping.Select(ps => ps.Type).Distinct().ToList();
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetMonthlyUsersInEventsByName(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = workScheduleService.GetEvents();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddMonths(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("MM.yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("MM.yyyy") == month))
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("MM.yyyy"),
                    Type = ps.Name
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Sum(p => p.UsersOnEvent.Count) }).ToList();

            var labels = entriesRegisterAfterGrouping.Select(ps => ps.Type).Distinct().ToList();
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public MultipleStatisticModel GetAnnualUsersInEventsByName(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            var iteratorStartDate = startDate.Date;
            endDate = endDate.Date;
            var statisticModel = new MultipleStatisticModel();

            var entriesRegister = workScheduleService.GetEvents();

            for (; iteratorStartDate <= endDate; iteratorStartDate = iteratorStartDate.AddYears(1))
            {
                statisticModel.Labels.Add(iteratorStartDate.ToString("yyyy"));
            }

            var entriesRegisterAfterGrouping = entriesRegister.Where(ps => statisticModel.Labels.Any(month => ps.StartDate.ToString("yyyy") == month))
                .GroupBy(ps => new
                {
                    Date = ps.StartDate.Date.ToString("yyyy"),
                    Type = ps.Name
                })
                .Select(ps => new TemporaryClass { Date = ps.Key.Date, Type = ps.Key.Type, Count = ps.Sum(p => p.UsersOnEvent.Count) }).ToList();

            var labels = entriesRegisterAfterGrouping.Select(ps => ps.Type).Distinct().ToList();
            foreach (var label in labels)
            {
                statisticModel.Data.Add(new AmountOfObjects { Key = label });
            }

            foreach (var statisticModelLabel in statisticModel.Labels)
            {
                foreach (var amountOfObject in statisticModel.Data)
                {
                    amountOfObject.Count.Add(entriesRegisterAfterGrouping.FirstOrDefault(ps => ps.Type == amountOfObject.Key && ps.Date == statisticModelLabel)?.Count ?? 0);
                }
            }

            return statisticModel;
        }

        public StatisticModel GetChosenEvents()
        {
            var statisticModel = new StatisticModel();

            var purchasedSubscriptions = workScheduleService.GetEvents();

            var purchasedSubscriptionsAfterGrouping = purchasedSubscriptions.GroupBy(ps => ps.Name)
                .Select(ps => new TemporaryClass { Type = ps.Key, Count = ps.Sum(p => p.UsersOnEvent.Count) }).ToList();

            statisticModel.Labels = purchasedSubscriptionsAfterGrouping.Select(ps => ps.Type).ToList();
            statisticModel.Data = purchasedSubscriptionsAfterGrouping.Select(ps => ps.Count).ToList();

            return statisticModel;
        }
    }
}
