﻿using System.Collections.Generic;
using System.Linq;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Data.Repositories;
using FitnessClubApp.Model;

namespace FitnessClubApp.Service
{
    public interface ISubscriptionService
    {
        IEnumerable<Subscription> GetSubscriptions();
        IEnumerable<Subscription> GetSortedSubscriptions();
        void CreateSubscription(Subscription subscription);
        Subscription GetSubscription(int id);
        void SaveSubscription();
        void EditSubscription(Subscription subscription);
        void RemoveSubscription(Subscription subscription);
    }

    public class SubscriptionService : ISubscriptionService
    {
        private readonly ISubscriptionRepository subscriptionRepository;
        private readonly IUnitOfWork unitOfWork;
        public SubscriptionService(ISubscriptionRepository subscriptionRepository, IUnitOfWork unitOfWork)
        {
            this.subscriptionRepository = subscriptionRepository;
            this.unitOfWork = unitOfWork;
        }
        public IEnumerable<Subscription> GetSubscriptions()
        {
            var subscriptions = subscriptionRepository.GetAll();
            return subscriptions;
        }

        public IEnumerable<Subscription> GetSortedSubscriptions()
        {
            var subscriptions = subscriptionRepository.GetAll().OrderBy(s => s.Days).ThenBy(s => s.Price);
            return subscriptions;
        }

        public void CreateSubscription(Subscription subscription)
        {
            subscriptionRepository.Add(subscription);
        }

        public void SaveSubscription()
        {
            unitOfWork.Commit();
        }

        public void EditSubscription(Subscription subscription)
        {
            subscriptionRepository.Update(subscription);
        }

        public void RemoveSubscription(Subscription subscription)
        {
            subscriptionRepository.Delete(subscription);
        }

        public Subscription GetSubscription(int id)
        {
            var subscription = subscriptionRepository.GetById(id);
            return subscription;
        }
    }
}
