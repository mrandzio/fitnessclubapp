﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Data.Repositories;
using FitnessClubApp.Model;

namespace FitnessClubApp.Service
{
    public interface IUserService
    {
        bool UserExists(string email);
        User GetUser(int id);
        User GetUser(string email);
        User GetUser(Guid activationCode);
        void ActivateAccount(User user);
        void GenerateNewUniqueCode(User user);
        void RemoveUniqueCode(User user);
        IEnumerable<User> GetUsers();
        void CreateUser(User user);
        void ChangePassword(User user, string password);
        void SaveUser();
        void EditUser(User user);
        string GetUserRoleName(int id);
        IEnumerable<UserRole> GetUserRoles();
        IEnumerable<User> GetUsersWithRole();
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository usersRepository;
        private readonly IUserRoleRepository userRolesRepository;
        private readonly IUnitOfWork unitOfWork;

        public UserService(IUserRepository usersRepository, IUserRoleRepository userRolesRepository, IUnitOfWork unitOfWork)
        {
            this.usersRepository = usersRepository;
            this.userRolesRepository = userRolesRepository;
            this.unitOfWork = unitOfWork;
        }

        #region IGadgetService Members

        public bool UserExists(string email)
        {
           var userExists = usersRepository.IsEmailExists(email);
           return userExists;
        }

        public User GetUser(int id)
        {
            var user = usersRepository.GetById(id);
            return user;
        }

        public User GetUser(string email)
        {
            var user = usersRepository.GetByEmail(email);
            return user;
        }

        public User GetUser(Guid activationCode)
        {
            var user = usersRepository.GetByActivationCode(activationCode);
            return user;
        }

        public IEnumerable<User> GetUsers()
        {
            var users = usersRepository.GetAll().OrderByDescending(u => u.DateCreated);
            return users;
        }

        public IEnumerable<User> GetUsersWithRole()
        {
            var users = usersRepository.GetAllWithRoleUser();
            return users;
        }

        public void CreateUser(User user)
        {
            var normalUserRole = userRolesRepository.GetByName("User");
            user.UserRole = normalUserRole;

            usersRepository.Add(user);
        }

        public void EditUser(User user)
        {
            usersRepository.Update(user);
        }

        public void SaveUser()
        {
            unitOfWork.Commit();
        }

        public void ActivateAccount(User user)
        {
            user.IsEmailVerified = true;
        }

        public void GenerateNewUniqueCode(User user)
        {
            user.UniqueCode = Guid.NewGuid();
        }

        public void RemoveUniqueCode(User user)
        {
            user.UniqueCode = null;
        }

        public void ChangePassword(User user, string password)
        {
            user.Password = Crypto.HashPassword(password);
        }

        public string GetUserRoleName(int id)
        {
            var userRoleName = userRolesRepository.GetById(id).Name;
            return userRoleName;
        }

        public IEnumerable<UserRole> GetUserRoles()
        {
            var userRoles = userRolesRepository.GetAll();
            return userRoles;
        }

        #endregion

    }
}
