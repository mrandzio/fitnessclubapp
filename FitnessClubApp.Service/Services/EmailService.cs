﻿using System;
using System.Configuration;
using System.Web;
using FitnessClubApp.Model;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace FitnessClubApp.Service
{
    public interface IEmailService
    {
        void SendVerificationLinkEmail(User user, string uri);
        void SendResetPasswordLinkEmail(User user, string resetPasswordLink);
        void SendConfirmationPurchaseSubscription(User user, PurchasedSubscription purchasedSubscription);
    }

    public class EmailService : IEmailService
    {
        #region IEmailService Members

        #region Get login and password for the email account

        private readonly string emailAccount = ConfigurationManager.AppSettings["EmailAccount"];
        private readonly string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

        #endregion

        public void SendVerificationLinkEmail(User user, string activationLink)
        {
            #region Creating a new message

            var subject = "Aktywacja konta w serwisie FitnessClubApp";
            var htmlBody = $"<p style=\"text-align: center;\"><strong>Witaj, {user.FirstName}!</strong></p>\r\n<p style=\"text-align: center;\">Zweryfikuj sw&oacute;j adres e-mail!</p>\r\n<p style=\"text-align: center;\">Ktoś (mamy nadzieję, że Ty) użył tego adresu e-mail w serwisie FitnessClubApp. Wejdź w poniższy link, aby zweryfikować posiadanie tego konta. Do momentu wykonania tej czynności nie będzie można korzystać z serwisu.</p>\r\n<p style=\"text-align: center;\"><a href=\'{activationLink}\'>Aktywuj konto</a></p>\r\n<blockquote>\r\n<p style=\"text-align: center;\">Konto nie zostało zarejestrowane przez Ciebie? Nie wchodź w podany link.</p>\r\n<p style=\"text-align: center;\">&copy; {DateTime.Now.Year} FitnessClubApp</p>\r\n</blockquote>";

            var message = CreateEmailToUser(user, subject, htmlBody);

            #endregion

            #region Connection with a smtp client

            ConnectionAndSendEmail(message);

            #endregion
        }

        public void SendResetPasswordLinkEmail(User user, string resetPasswordLink)
        {
            #region Creating a new message

            var subject = "Zmiana hasła w serwisie FitnessClubApp";
            var htmlBody = $"<p style=\"text-align: center;\"><strong>Witaj, { user.FirstName }!</strong></p>\r\n<p style=\"text-align: center;\">Zmień hasło na swoim koncie!</p>\r\n<p style=\"text-align: center;\">Ktoś (mamy nadzieję, że Ty) chce zmienić hasło na koncie zarejestrowanym pod tym adresem e-mail w serwisie FitnessClubApp. Wejdź w poniższy link, aby dokończyć operację zmiany hasła.</p>\r\n<p style=\"text-align: center;\"><a href=\'{ resetPasswordLink }\'>Zmień hasło</a></p>\r\n<blockquote>\r\n<p style=\"text-align: center;\">Nie chciałeś zmieniać hasła? Nie wchodź w podany link.</p>\r\n<p style=\"text-align: center;\">&copy; { DateTime.Now.Year } FitnessClubApp</p>\r\n</blockquote>";

            var message = CreateEmailToUser(user, subject, htmlBody);

            #endregion

            #region Connection with a smtp client

            ConnectionAndSendEmail(message);

            #endregion
        }

        public void SendConfirmationPurchaseSubscription(User user, PurchasedSubscription purchasedSubscription)
        {
            #region Creating a new message

            var subject = "Potwiedzenie złożenia zamówienia";
            var htmlBody = $"<p style=\"text-align: center;\"><strong>Witaj, {user.FirstName}!</strong></p>\r\n<p style=\"text-align: center;\">Przeczytaj informacje zawarte poniżej.</p><br/><p style=\"text-align: center;\"><strong>Zakupiona subskrypcja:</strong> {purchasedSubscription.SubscriptionName}</p><p style=\"text-align: center;\"><strong>Cena:</strong> {purchasedSubscription.Price:0.# zł}</p><p style=\"text-align: center;\"><strong>Data rozpoczęcia:</strong> {purchasedSubscription.StartDate.ToString("dd.MM.yyyy")}</p><p style=\"text-align: center;\"><strong>Data zakończenia:</strong> {purchasedSubscription.EndDate.ToString("dd.MM.yyyy")}</p><br/>\r\n<p style=\"text-align: center;\">Możesz osobiście dokonać płatności za karnet w siedzibie klubu lub wpłacić pieniądze na konto firmy.</p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><strong>Dane do konta:</strong></p>\r\n<p style=\"text-align: center;\">&nbsp;<strong>Adres:&nbsp;</strong>FitnessClubApp Inc.</p>\r\n<p style=\"text-align: center;\">Warszawska 23/12&nbsp;</p>\r\n<p style=\"text-align: center;\">15-500 Białystok</p>\r\n<p style=\"text-align: center;\"><strong>Kwota:</strong> {purchasedSubscription.Price:0.# zł}</p>\r\n<p style=\"text-align: center;\"><strong>Numer konta:</strong> PL 11 2222 3333 4444 5555 6666 7777</p>\r\n<p style=\"text-align: center;\"><strong>Tytuł przelewu:</strong> SUBSKRYPCJA ID {purchasedSubscription.PurchasedSubscriptionID}</p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: center;\">Do momentu zaksięgowania wpłaty subskrypcja będzie nieaktywna i możesz nie mieć dostępu do klubu.</p>\r\n<p style=\"text-align: center;\">&nbsp;</p><p style=\"text-align: center;\">&copy; {DateTime.Now.Year} FitnessClubApp</p>\r\n</blockquote>";

            var message = CreateEmailToUser(user, subject, htmlBody);

            #endregion

            #region Connection with a smtp client

            ConnectionAndSendEmail(message);

            #endregion
        }

        private MimeMessage CreateEmailToUser(User user, string subject, string htmlBody)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("FitnessClubApp", emailAccount));
            message.To.Add(new MailboxAddress($"{user.FirstName} {user.LastName}", user.Email));
            message.Subject = subject;

            var bodyBuilder = new BodyBuilder
            {
                HtmlBody = htmlBody
            };

            message.Body = bodyBuilder.ToMessageBody();
            return message;
        }

        private void ConnectionAndSendEmail(MimeMessage message)
        {
            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 465, SecureSocketOptions.SslOnConnect);

                client.Authenticate(emailAccount, emailPassword);

                client.Send(message);
                client.Disconnect(true);
            }
        }

        #endregion
    }
}
