﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitnessClubApp.Data.Infrastructure;
using FitnessClubApp.Data.Repositories;
using FitnessClubApp.Model;

namespace FitnessClubApp.Service
{
    public interface IEntryRegisterService
    {
        IEnumerable<Entry> GetEntriesRegister();
        Entry GetEntry(int entryID);
        void CreateEntry(Entry entry);
        void EditEntry(Entry entry);
        void RemoveEntry(Entry entry);
        void SaveEntry();
        bool EntryExists(Entry entry);
        IEnumerable<Entry> GetUserEntries(int userID, DateTime limitDate);
    }

    public class EntryRegisterService : IEntryRegisterService
    {
        private readonly IEntryRepository entryRepository;
        private readonly IUnitOfWork unitOfWork;

        public EntryRegisterService(IEntryRepository entryRepository, IUnitOfWork unitOfWork)
        {
            this.entryRepository = entryRepository;
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<Entry> GetEntriesRegister()
        {
            var entriesRegister = entryRepository.GetAllWithEntities();
            return entriesRegister;
        }

        public IEnumerable<Entry> GetUserEntries(int userID, DateTime limitDate)
        {
            var entriesRegister = entryRepository.GetMany(e => e.UserID == userID && e.StartDate > limitDate && e.EndDate != null);
            return entriesRegister;
        }

        public Entry GetEntry(int entryID)
        {
            var entry = entryRepository.GetWithEntities(e => e.EntryID == entryID);
            return entry;
        }

        public void CreateEntry(Entry entry)
        {
            entryRepository.Add(entry);
        }

        public void EditEntry(Entry entry)
        {
            entryRepository.Update(entry);
        }

        public void RemoveEntry(Entry entry)
        {
            entryRepository.Delete(entry);
        }

        public void SaveEntry()
        {
            unitOfWork.Commit();
        }

        public bool EntryExists(Entry entry)
        {
            var findEntry = entryRepository.GetWithEntities(e => e.EntryID != entry.EntryID && e.UserID == entry.UserID && e.EndDate == null);
            return findEntry != null;
        }
    }
}
